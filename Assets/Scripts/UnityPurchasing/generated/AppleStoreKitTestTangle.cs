// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleStoreKitTestTangle
    {
        private static byte[] data = System.Convert.FromBase64String("L0LgWCSJ5BBfVxwwb7XDAKzmGgbh5+O4n4SZjqCCn9r62+Tt6L7v4IhAfHHAKMEzO1kKPrHm0vCEjMNCKRANtPkq6ejq6+pI0NvS2+Tt6L6Lndy5BxRH8rbn/sRh5B5iIzzA9tvh7ePA7eru7uzo6Nvm7eLBbaNtHObq6uDu6+hp6urrWesL1xoDAon5aD1sk4ZyVrVY4QtzJR0BKGWGgmcKmjChrhjG8n3ovWpNrwWenLC469tp6uHpaerq6zCUe9fHmOQwfM2f2vrb5O3ovu/h5+O4n4SZjqCCn8Fto20c5urq4O7r27Ta+tvk7ei+PA+TQ0RDhkwqMYPGjSckwuC7xwjbaeif22npt0vo6erp6erq2+bt4qpOleJmUdbWJpu8MwYEPjGcI3jvJJNHtlHVtLvAt8JnNik8wfw0yV/mUNN7gO0vF+YOfrsmeiNRzh+zIt7Z2N6x/Obf29vY2dza3N7Z2N6x6hTv7+jp6W/b/e3ovvbO6uoU7+fbtNr62+Tt6L7v6OfjuJ+EmY6gggyEC/AN5oMmzoUoneMeoeBFkZ/XYEcedFvi00iFkCDNu+HxCmI6mZ3Hng2nWvk4Fgj0teaWU7M8S5cPlNrxemtSVA7nINdQ5a+JzRHvwl2i2vrb5O3ovu/g5+O4n4SZjqCCn9rn47ifhJmOoIKf2uDb4u3ovu/t+FYrPfardbxS03ucnbk5Q6CbhVOMJI/Xhmuy2Gch3e3B+vBmoiXjFI2jbRzm6uLq/eO4n4SZjqCCn9tp6gBcq2rWbNabujovtmZON5SBs/E14NKnIKSFK1i0XH5RUkO2oZ9ABISMzlJAh9iORjMpcZOj5AInKszmLeDb4u3ovu/t+Om+uNr82/7t4sFt7+jn47ifhJmOoIKf2vrb5O3ovu/3BOJjk44lahlmmQzs8YXN+RzZWiusw6AAsSrA4r3HTkHi6JDZ2jcD9vjq6hTv7tvo6uoU2+Xt6L725OqgeFDhE0rK1SOKMs8jtfSe1TF4MG6ToyUrLvn6geTnRcTuIYSRj5TLydvm7eLBbaNtHObq6uru6+hp6uRyEFhhvnQE9sNTtME6mHTerNxQrTid+xkbggcmiXKaEothb6PTL1Zg6b642vzb/u3iwW2jbRzm6uLq/eO4n4SZjqCCn9v1/ObZ29vf29ra3PUHwHruTNak");
        private static int[] order = new int[] { 18,3,40,27,43,30,31,41,24,14,14,27,26,18,21,25,23,41,29,24,41,24,39,23,24,43,32,36,32,42,41,35,36,40,39,37,36,43,38,42,40,42,42,43,44 };
        private static int key = 235;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
