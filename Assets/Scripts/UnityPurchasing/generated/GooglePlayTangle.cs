// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("LnXCZBGPQz/1e5JKTb3KPfZ+tiVSpvfejv/jh/5Wk3IXlcLTlPr/l9jQPc/mwhwOt+z1ypvmI96UVMOrkZBiGmL/Ibr0Qm6JafG9JjvIfOjTI0rAEm+7uJ4FG529GUVxTvDfrkDDzcLyQMPIwEDDw8JQ0S8C4fxM0tM84R/+Rp+NtxyfBiT33Pr6GSx6s8NyEkWw5HEJ/LQcz0bChXSAVzI1a7II9LEKq8ewntRUd/YEg+4rjKWAzme4m8IS8aTyyq9ZNOnvqXc2l3tJ4EGafxh6AYQPC2x/CiySWX3JVcQrOID4HTLA35EpVJRjaa2aRdQ4cDB2gcR42000VSWg92T9RiryQMPg8s/Ey+hEikQ1z8PDw8fCwXzi7apd+4+v38DBw8LD");
        private static int[] order = new int[] { 11,8,3,7,4,8,7,12,13,10,13,13,12,13,14 };
        private static int key = 194;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
