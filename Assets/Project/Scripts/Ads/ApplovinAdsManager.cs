using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppodealStack.Monetization.Api;
using AppodealStack.Monetization.Common;



namespace Project.Ads
{
    public class ApplovinAdsManager : MonoBehaviour
    {
        public static ApplovinAdsManager Instance;

        public bool isSDKInit { get; private set; }

        private const float INTERSTITIAL_MAX_TIME = 60f;
        private float interstitialTimer;
        private int showInterstitialCount = 0;
        private bool isReward = false;

#if UNITY_IOS
string adUnitId = "";
#else // UNITY_ANDROID
        string adUnitIdRewarded = "01676dfbe73db82f";
        string adUnitIdInterstitial = "ac2d1fb3ee2dc9b4";
#endif
        int retryAttempt;
        Action<bool> callbackAction;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            interstitialTimer = INTERSTITIAL_MAX_TIME;
        }
        private void Update()
        {
            if (Tutor.TutorManager.Instance.isEndTutor)
            {
                if (interstitialTimer > 0) interstitialTimer -= Time.deltaTime;
                else
                {
                    ShowInterstitial();
                }
            }
              

            if (isReward)
            {
                isReward = false;
                callbackAction?.Invoke(true);
                callbackAction = null;
            }
        }



        public void InitAds()
        {
            int adTypes = AppodealAdType.Interstitial | AppodealAdType.RewardedVideo;
            string appKey = "b004a32109199ccba3a8f1f32c833be99cff95ed75bfd1e4";
            AppodealCallbacks.Sdk.OnInitialized += OnInitializationFinished;
            Appodeal.Initialize(appKey, adTypes);
        }

        private void OnInitializationFinished(object sender, SdkInitializedEventArgs e)
        {
            interstitialTimer = INTERSTITIAL_MAX_TIME;
            AppodealCallbacks.RewardedVideo.OnFinished += OnRewardedVideoFinished;
        }

        private void OnRewardedVideoFinished(object sender, RewardedVideoFinishedEventArgs e)
        {
            isReward = true;
        }

        public bool IsLoadedRewardAds()
        {
            return Appodeal.IsLoaded(AppodealAdType.RewardedVideo);
        }
        public bool IsLoadedInterstitialAds()
        {
            return Appodeal.IsLoaded(AppodealAdType.Interstitial);
        }

        public void ShowRewardedAd(Action<bool> callback)
        {
            if (IsLoadedRewardAds())
            {
                Appodeal.Show(AppodealShowStyle.RewardedVideo);
                callbackAction += callback;
                LoadRewardedAd();
            }
        }
        private void ShowInterstitial()
        {
            if (IsLoadedInterstitialAds())
            {
                showInterstitialCount++;
                float additionalTime = 0;
                if (showInterstitialCount < 10) additionalTime += showInterstitialCount * 10f;
                else additionalTime = 100f;
                 interstitialTimer = INTERSTITIAL_MAX_TIME + additionalTime;
                Appodeal.Show(AppodealShowStyle.Interstitial);
            }
        }

        private void InitializeRewardedAds()
        {
            LoadRewardedAd();

        }

        private void LoadRewardedAd()
        {
           // MaxSdk.LoadRewardedAd(adUnitIdRewarded);
        }
    }
}

