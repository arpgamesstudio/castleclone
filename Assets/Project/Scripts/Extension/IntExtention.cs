using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class IntExtention
{
    public static string ToLitter(this int num)
    {
        string str = "";
        double numD = (double)num;
        if (num < 1000)
        {
            str = "" + Math.Round(numD);
        }
        else if (num < 1000000)
        {
            str = "" + Math.Round(numD / 1000, 2) + "K";
        }
        else if (num < 1000000000)
        {
            str = "" + Math.Round(numD / 1000000, 2) + "M";
        }
        else if (num < 2000000000)
        {
            str = "" + Math.Round(numD / 1000000000, 2) + "B";
        }
        else
        {
            str = "" + num;
        }

        return str;
    }

    public static string ToLitter(this int num, int countNum)
    {
        string str = "";
        double numD = (double)num;
        if (num < 1000)
        {
            str = "" + Math.Round(numD);
        }
        else if (num < 1000000)
        {
            str = "" + Math.Round(numD / 1000, countNum) + "K";
        }
        else if (num < 1000000000)
        {
            str = "" + Math.Round(numD / 1000000, countNum) + "M";
        }
        else if (num < 2000000000)
        {
            str = "" + Math.Round(numD / 1000000000, countNum) + "B";
        }
        else
        {
            str = "" + num;
        }

        return str;
    }
}
