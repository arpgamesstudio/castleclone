using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class DoubleExtention
{
    public static string ToLitter(this double num)
    {
        string str = "";

        if(num < 1000)
        {
            str = "" + Math.Round(num);
        }
        else if (num < 1000000)
        {
            str = "" + Math.Round(num / 1000, 1) + "K";
        }
        else if (num < 1000000000)
        {
            str = "" + Math.Round(num / 1000000, 1) + "M";
        }
        else if (num < 1000000000000)
        {
            str = "" + Math.Round(num / 1000000000, 1) + "B";
        }
        else if (num < 1000000000000000)
        {
            str = "" + Math.Round(num / 1000000000000, 1) + "T";
        }
        else if (num < 1000000000000000000)
        {
            str = "" + Math.Round(num / 1000000000000000, 1) + "Q";
        }
        else
        {
            str = "" + num;
        }

        return str;
    }
}
