using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project
{
    public enum CurrencyType
    {
        Coins,
        Diamond,
        Purchase
    }

    public enum UnitType
    {
        Mellee,
        Archer,
        Mage,
        Catapult
    }
    public enum UnitRarity
    {
        Normal,
        Improved,
        Rare,
        Unique,
        Legendary,
        Mythical,
        Immortal
    }
    public class Enums { }
}

