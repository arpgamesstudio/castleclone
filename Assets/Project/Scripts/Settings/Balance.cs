using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Settings
{
    [CreateAssetMenu(fileName = "Balance", menuName = "GameSettings/Create Balance")]
    public class Balance : ScriptableObject
    {
        [SerializeField] private int startCoinsCount;
        [SerializeField] private int startDiamondCount;
        [SerializeField] private bool startMelleeOpen;
        [SerializeField] private bool startArcherOpen;
        [SerializeField] private bool startMageOpen;
        [SerializeField] private bool startCatapultOpen;

        [Space(20)]
        [Tooltip("��������� ������ ��������� � �������")]
        [SerializeField] private float priceDimondInCoins;
        [Space(10)]
        [SerializeField] private float baseMergeCellPrice;
        [SerializeField] private float baseMergeCellCostMultiply;
        [Space(30)]
        [SerializeField] private float melleeUnitBasePrice;
        [SerializeField] private float archerUnitBasePrice;
        [SerializeField] private float mageUnitBasePrice;
        [SerializeField] private float catapultUnitBasePrice;
        [Space(10)]
        [SerializeField] private float unitUpgradeRankMultiply;
        [SerializeField] private float unitUpgradeRarityMultiply;
        [SerializeField] private float buyUnitMultiply;

        [Space(30)]
        [SerializeField] private float melleeUnitBaseAttack;
        [SerializeField] private float melleeUnitBaseHp;
        [SerializeField] private float archerUnitBaseAttack;
        [SerializeField] private float archerUnitBaseHp;
        [SerializeField] private float mageUnitBaseAttack;
        [SerializeField] private float mageUnitBaseHp;
        [SerializeField] private float mageUnitBaseHealing;
        [SerializeField] private float catapultUnitBaseAttack;
        [SerializeField] private float catapultUnitBaseHp;
        [Space(10)]
        [SerializeField] private float unitUpgradeAtackMultiply;
        [SerializeField] private float unitUpgradeHpMultiply;
        [SerializeField] private float unitUpgradeMageHealingMultiply;

        public int GetStartCoinsCount { get => startCoinsCount; }
        public int GetStartDiamondCount { get => startDiamondCount; }
        public bool GetStartMelleeOpen { get => startMelleeOpen; }
        public bool GetStartArcherOpen { get => startArcherOpen; }
        public bool GetStartMageOpen { get => startMageOpen; }
        public bool GetStartCatapultOpen { get => startCatapultOpen; }

        public int GetMergeCellPrice(int index) => (int)(baseMergeCellPrice * Mathf.Pow(baseMergeCellCostMultiply, index));
        public int CalculatePriceCoinsToDiamond(int coins) => (int)(coins / priceDimondInCoins);
        public int GetUnitUpgradePrice(int currentRank, UnitRarity currentRarity, UnitType unitType)
        {
            int price = 0;
            float basePrice = 0;
            float rarityMultiply = 0;

            switch (unitType)
            {
                case UnitType.Mellee:
                    basePrice = melleeUnitBasePrice;
                    break;
                case UnitType.Archer:
                    basePrice = archerUnitBasePrice;
                    break;
                case UnitType.Mage:
                    basePrice = mageUnitBasePrice;
                    break;
                case UnitType.Catapult:
                    basePrice = catapultUnitBasePrice;
                    break;
                default:
                    break;
            }
            switch (currentRarity)
            {
                case UnitRarity.Normal:
                    rarityMultiply = 0;
                    break;
                case UnitRarity.Improved:
                    rarityMultiply = 1;
                    break;
                case UnitRarity.Rare:
                    rarityMultiply = 2;
                    break;
                case UnitRarity.Unique:
                    rarityMultiply = 3;
                    break;
                case UnitRarity.Legendary:
                    rarityMultiply = 4;
                    break;
                case UnitRarity.Mythical:
                    rarityMultiply = 5;
                    break;
                case UnitRarity.Immortal:
                    rarityMultiply = 6;
                    break;
                default:
                    break;
            }

            float basePriseConst = 0; 
            if (currentRank < 5)
            {
                basePriseConst = basePrice * Mathf.Pow(unitUpgradeRankMultiply, currentRank + (rarityMultiply * 5));
            }
            else
            {
                basePriseConst = (basePrice * 2) * Mathf.Pow(unitUpgradeRarityMultiply, currentRank + (rarityMultiply * 5));
            }
            price = (int)basePriseConst;
            return price;
        }
        public int GetUnitBuyPrice(int currentRank, UnitRarity currentRarity, UnitType unitType)
        {
            int price = 0;
            float basePrice = 0;
            float rarityMultiply = 0;

            switch (unitType)
            {
                case UnitType.Mellee:
                    basePrice = melleeUnitBasePrice;
                    break;
                case UnitType.Archer:
                    basePrice = archerUnitBasePrice;
                    break;
                case UnitType.Mage:
                    basePrice = mageUnitBasePrice;
                    break;
                case UnitType.Catapult:
                    basePrice = catapultUnitBasePrice;
                    break;
                default:
                    break;
            }
            switch (currentRarity)
            {
                case UnitRarity.Normal:
                    rarityMultiply = 0;
                    break;
                case UnitRarity.Improved:
                    rarityMultiply = 1;
                    break;
                case UnitRarity.Rare:
                    rarityMultiply = 2;
                    break;
                case UnitRarity.Unique:
                    rarityMultiply = 3;
                    break;
                case UnitRarity.Legendary:
                    rarityMultiply = 4;
                    break;
                case UnitRarity.Mythical:
                    rarityMultiply = 5;
                    break;
                case UnitRarity.Immortal:
                    rarityMultiply = 6;
                    break;
                default:
                    break;
            }

            float basePriseConst = 0;
            basePriseConst = (basePrice * Mathf.Pow(buyUnitMultiply, currentRank + (rarityMultiply * 5))) / 10f;
            price = (int)basePriseConst;
            return price;
        }

        public int GetUnitAttackValue(int currentRank, UnitRarity currentRarity, UnitType unitType)
        {
            int attack = 0;
            float baseAttack = 0;
            float rarityMultiply = 0;

            switch (unitType)
            {
                case UnitType.Mellee:
                    baseAttack = melleeUnitBaseAttack;
                    break;
                case UnitType.Archer:
                    baseAttack = archerUnitBaseAttack;
                    break;
                case UnitType.Mage:
                    baseAttack = mageUnitBaseAttack;
                    break;
                case UnitType.Catapult:
                    baseAttack = catapultUnitBaseAttack;
                    break;
                default:
                    break;
            }
            switch (currentRarity)
            {
                case UnitRarity.Normal:
                    rarityMultiply = 0;
                    break;
                case UnitRarity.Improved:
                    rarityMultiply = 1;
                    break;
                case UnitRarity.Rare:
                    rarityMultiply = 2;
                    break;
                case UnitRarity.Unique:
                    rarityMultiply = 3;
                    break;
                case UnitRarity.Legendary:
                    rarityMultiply = 4;
                    break;
                case UnitRarity.Mythical:
                    rarityMultiply = 5;
                    break;
                case UnitRarity.Immortal:
                    rarityMultiply = 6;
                    break;
                default:
                    break;
            }
            float baseAttackConst = baseAttack * Mathf.Pow(unitUpgradeAtackMultiply, currentRank + rarityMultiply * 5);
            attack = (int)baseAttackConst;
            return attack;
        }
        public float GetUnitAttackDelay(float baseDelay, UnitRarity currentRarity)
        {
            float delay = baseDelay;
            float multiply = 0;
            switch (currentRarity)
            {
                case UnitRarity.Normal:
                    multiply = 0.05f;
                    break;
                case UnitRarity.Improved:
                    multiply = 0.1f;
                    break;
                case UnitRarity.Rare:
                    multiply = 0.15f;
                    break;
                case UnitRarity.Unique:
                    multiply = 0.22f;
                    break;
                case UnitRarity.Legendary:
                    multiply = 0.3f;
                    break;
                case UnitRarity.Mythical:
                    multiply = 0.4f;
                    break;
                case UnitRarity.Immortal:
                    multiply = 0.55f;
                    break;
                default:
                    break;
            }
            return delay - (delay * multiply);
        }
        public int GetUnitHpValue(int currentRank, UnitRarity currentRarity, UnitType unitType)
        {
            int hp = 0;
            float baseHp = 0;
            float rarityMultiply = 0;

            switch (unitType)
            {
                case UnitType.Mellee:
                    baseHp = melleeUnitBaseHp;
                    break;
                case UnitType.Archer:
                    baseHp = archerUnitBaseHp;
                    break;
                case UnitType.Mage:
                    baseHp = mageUnitBaseHp;
                    break;
                case UnitType.Catapult:
                    baseHp = catapultUnitBaseHp;
                    break;
                default:
                    break;
            }
            switch (currentRarity)
            {
                case UnitRarity.Normal:
                    rarityMultiply = 0;
                    break;
                case UnitRarity.Improved:
                    rarityMultiply = 1;
                    break;
                case UnitRarity.Rare:
                    rarityMultiply = 2;
                    break;
                case UnitRarity.Unique:
                    rarityMultiply = 3;
                    break;
                case UnitRarity.Legendary:
                    rarityMultiply = 4;
                    break;
                case UnitRarity.Mythical:
                    rarityMultiply = 5;
                    break;
                case UnitRarity.Immortal:
                    rarityMultiply = 6;
                    break;
                default:
                    break;
            }
            float baseAttackConst = baseHp * Mathf.Pow(unitUpgradeHpMultiply, currentRank + rarityMultiply * 5);
            hp = (int)baseAttackConst;
            return hp;
        }
        public int GetMageHealingValue(int currentRank, UnitRarity currentRarity)
        {
            int Healing = 0;
            float rarityMultiply = 0;

            switch (currentRarity)
            {
                case UnitRarity.Normal:
                    rarityMultiply = 0;
                    break;
                case UnitRarity.Improved:
                    rarityMultiply = 1;
                    break;
                case UnitRarity.Rare:
                    rarityMultiply = 2;
                    break;
                case UnitRarity.Unique:
                    rarityMultiply = 3;
                    break;
                case UnitRarity.Legendary:
                    rarityMultiply = 4;
                    break;
                case UnitRarity.Mythical:
                    rarityMultiply = 5;
                    break;
                case UnitRarity.Immortal:
                    rarityMultiply = 6;
                    break;
                default:
                    break;
            }
            float baseAttackConst = mageUnitBaseHealing * Mathf.Pow(unitUpgradeMageHealingMultiply, currentRank + rarityMultiply * 5);
            Healing = (int)baseAttackConst;
            return Healing;
        }
    }
}

