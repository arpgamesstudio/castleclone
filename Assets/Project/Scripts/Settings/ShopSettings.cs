using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using I2.Loc;


namespace Project.Settings
{
    [CreateAssetMenu(menuName = "GameSettings/Create ShopSettings", fileName = "ShopSettings")]
    public class ShopSettings : ScriptableObject
    {
        public enum ProductRarity
        {
            Normal,
            Rare,
            Legendary
        }
        public enum ResurceType
        {
            Coin,
            Diamond
        }

        [SerializeField] private List<ShopContent> shopContents;

        public List<ShopContent> GetAllShopContent { get => shopContents; }
        



        [System.Serializable]
        public class ShopContent
        {
            [SerializeField] private string productId;
            [SerializeField] private ShopProductNames productName;
            [SerializeField] private ProductRarity productRarity;
            [SerializeField] private CurrencyType resurceType;
            [SerializeField] private int value;
            [SerializeField] private Sprite icon;
            [SerializeField] private bool isFree;
            [SerializeField] private bool isAdRewardShow;
            [SerializeField] private bool isHitProduct;
            [SerializeField] private int timerSec;
            [SerializeField] private LocalizedString description;

            public ProductRarity GetProductRarity { get => productRarity; }
            public CurrencyType GetResurceType { get => resurceType; }
            public int GetValue { get => value; }
            public bool GetIsFree { get => isFree; }
            public bool GetIsAdRewardShow { get => isAdRewardShow; }
            public bool GetIsHitProduct { get => isHitProduct; }
            public LocalizedString GetDescription { get => description; }
            public Sprite GetIcon { get => icon; }
            public ShopProductNames ProductName { get => productName; }
            public string ProductId { get => productId; }
            public int TimerSec { get => timerSec; }
        }
    }
}

