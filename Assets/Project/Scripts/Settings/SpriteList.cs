using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.Settings
{
    [CreateAssetMenu(fileName = "SpriteList", menuName = "GameSettings/Create SpriteList")]
    public class SpriteList : ScriptableObject
    {
        [SerializeField] private List<BackRaritySpriteSettings> backRaritySpriteSettings;
        [SerializeField] private List<AvatarSpriteSettings> avatarSpriteSettings;



        public Sprite GetAvatarBackSprite(UnitRarity rarity)
        {
            return backRaritySpriteSettings.Find(p => p.GetUnitRarity == rarity).GetSprite;
        }

        public Sprite GetAvatarSpriteMini(UnitType unitType, UnitRarity rarity)
        {
            return avatarSpriteSettings.Find(p => p.GetUnitType == unitType && p.GetUnitRarity == rarity).GetSpriteMini;
        }
        public Sprite GetAvatarSpriteFull(UnitType unitType, UnitRarity rarity)
        {
            return avatarSpriteSettings.Find(p => p.GetUnitType == unitType && p.GetUnitRarity == rarity).GetSpriteFull;
        }


        [System.Serializable]
        public class BackRaritySpriteSettings
        {
            [SerializeField] private string name;
            [SerializeField] private UnitRarity unitRarity;
            [SerializeField] private Sprite sprite;

            public UnitRarity GetUnitRarity { get => unitRarity; }
            public Sprite GetSprite { get => sprite; }
        }

        [System.Serializable]
        public class AvatarSpriteSettings
        {
            [SerializeField] private string name;
            [SerializeField] private UnitType unitType;
            [SerializeField] private UnitRarity unitRarity;
            [SerializeField] private Sprite spriteMini;
            [SerializeField] private Sprite spriteFull;

            public UnitType GetUnitType { get => unitType; }
            public UnitRarity GetUnitRarity { get => unitRarity; }
            public Sprite GetSpriteMini { get => spriteMini; }
            public Sprite GetSpriteFull { get => spriteFull; }
        }
    }
}

