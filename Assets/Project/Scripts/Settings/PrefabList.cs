using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Core;
using I2.Loc;


namespace Project.Settings
{
    [CreateAssetMenu(fileName = "PrefabList", menuName = "GameSettings/Create PrefabList")]
    public class PrefabList : ScriptableObject
    {
        [SerializeField] private List<UnitWarriorSettings> unitWarriorList;

        public UnitWarriorSettings GetUnit(UnitType unitType, UnitRarity unitRarity) => unitWarriorList.Find(p => p.UnitType == unitType && p.UnitRarity == unitRarity);

        [System.Serializable]
        public class UnitWarriorSettings
        {
            [SerializeField] private LocalizedString name;
            [SerializeField] private UnitType unitType;
            [SerializeField] private UnitRarity unitRarity;
            [SerializeField] private UnitWarrior prefab;
            [SerializeField] private GameObject previwPrefab;

            public LocalizedString GetNameLoc { get => name; }
            public UnitType UnitType { get => unitType;}
            public UnitRarity UnitRarity { get => unitRarity;}
            public UnitWarrior GetPrefab { get => prefab;}
            public GameObject GetPreviewPrefab { get => previwPrefab; }
        }
    }
}

