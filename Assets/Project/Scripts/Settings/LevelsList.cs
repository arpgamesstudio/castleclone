using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Core;


namespace Project.Settings
{
    [CreateAssetMenu(fileName = "LevelsList", menuName = "GameSettings/Create LevelsList")]
    public class LevelsList : ScriptableObject
    {
        [SerializeField] private List<LevelSettings> levels;




        public List<LevelSettings> GetAllLevels { get => levels; }
        public LevelSettings GetLevel(int index) { return levels.Find(p => p.GetIndex == index); }



        [System.Serializable]
        public class LevelSettings
        {
            [SerializeField] private int index;
            [SerializeField] private LevelManager levelPrefab;
            [SerializeField] private Sprite mapCastleSprite;
            [SerializeField] private Sprite mapBossSprite;
            [SerializeField] private UnitType openUnit;

            public LevelManager GetLevelPrefab { get { return levelPrefab; } }
            public int GetIndex { get { return index; } }
            public Sprite GetMapCastleSprite { get => mapCastleSprite; }
            public Sprite GetMapBossSprite { get => mapBossSprite; }
            public UnitType OpenUnit { get => openUnit; }
        }
    }
}

