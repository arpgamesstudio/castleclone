﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisiblePoints : MonoBehaviour
{
    [SerializeField]
    private bool isDraw = true;
    [SerializeField]
    private bool isShowName = true;

    public float radius = 0.2f;
    [SerializeField]
    private Color color = new Color(1f, 1f, 1f, 1f);
    [SerializeField]
    private string namePoint;

    public enum Types
    {
        DrawSphere,
        DrawWireSphere
    }
    public Types typeView;


    private SphereCollider sphereCollider;

    private void OnDrawGizmos()
    {
        sphereCollider = GetComponent<SphereCollider>();
        if (sphereCollider != null) radius = sphereCollider.radius;

        if (isDraw)
        {
            Gizmos.color = color;
            switch (typeView)
            {
                case Types.DrawSphere:
                    Gizmos.DrawSphere(transform.position, radius);
                    break;
                case Types.DrawWireSphere:
                    Gizmos.DrawWireSphere(transform.position, radius);
                    break;
            }
        }


        if (isShowName) GizmosUtils.DrawText(GUI.skin, namePoint, transform.position, color, 20, 10f);
    }



    public static class GizmosUtils
    {
        public static void DrawText(GUISkin guiSkin, string text, Vector3 position, Color? color = null, int fontSize = 0, float yOffset = 0)
        {
#if UNITY_EDITOR
            var prevSkin = GUI.skin;
            if (guiSkin == null)
                Debug.LogWarning("editor warning: guiSkin parameter is null");
            else
                GUI.skin = guiSkin;

            GUIContent textContent = new GUIContent(text);

            GUIStyle style = (guiSkin != null) ? new GUIStyle(guiSkin.GetStyle("Label")) : new GUIStyle();
            if (color != null)
                style.normal.textColor = (Color)color;
            if (fontSize > 0)
                style.fontSize = fontSize;

            Vector2 textSize = style.CalcSize(textContent);
            Vector3 screenPoint = Camera.current.WorldToScreenPoint(position);

            if (screenPoint.z > 0)
            {
                var worldPosition = Camera.current.ScreenToWorldPoint(new Vector3(screenPoint.x - textSize.x * 0.5f, screenPoint.y + textSize.y * 0.5f + yOffset, screenPoint.z));
                UnityEditor.Handles.Label(worldPosition, textContent, style);
            }
            GUI.skin = prevSkin;
#endif
        }
    }
}
