using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookToCam : MonoBehaviour
{
    private void Update()
    {
        Vector3 dir = (Camera.main.transform.position - transform.position).normalized;
        transform.rotation = new Quaternion(dir.x, dir.y, dir.z, 0);
    }
}
