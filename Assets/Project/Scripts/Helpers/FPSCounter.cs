﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FPSCounter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI fpsTxt;
    [SerializeField]
    private float timeUpdate = 0.5f;
    private float timer = 0;
    void Update()
    {
        if (Time.time - timer > timeUpdate)
        {
            timer = Time.time;
            fpsTxt.text = "" + (1 / Time.deltaTime);
        }
        
    }
}
