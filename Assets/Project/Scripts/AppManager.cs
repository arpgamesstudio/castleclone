using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Core;
using Project.Settings;
using Project.Saves;
using Project.UI;
using Project.Ads;
using TG.Audio;

namespace Project
{
    public class AppManager : MonoBehaviour
    {
        public static AppManager Instance;

        [SerializeField] private LevelsList levelsSettings;
        [SerializeField] private Balance balance;
        [SerializeField] private SpriteList spriteList;
        [SerializeField] private PrefabList prefabList;
        [SerializeField] private ShopSettings shopSettings;
        [SerializeField] private AudioLibrary audioLibrary;


        public LevelsList GetLevelsSettings { get => levelsSettings; }
        public Balance GetBalance { get => balance; }
        public SpriteList GetSpriteList { get => spriteList; }
        public LevelManager CurrentLevelManager { get; private set; }
        public bool IsBlockedScreenScroll { get; set; }
        public PrefabList GetPrefabList { get => prefabList;}
        public int GetMaxLevelsCount { get => levelsSettings.GetAllLevels.Count; }
        public ShopSettings GetShopSettings { get => shopSettings;}
        public List<StoreProduct> GetShopProducts { get; private set; }
        public Firebase.FirebaseApp GetFirebaseApp { get; private set; }
        public AudioLibrary GetAudioLibrary { get => audioLibrary; }

        public StoreProduct GetProduct(string id) => GetShopProducts.Find(p => p.productName == id);

        public Action AppInitAction;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                gameObject.AddComponent<SaveManager>();
                gameObject.AddComponent<UIManager>();
                gameObject.AddComponent<ApplovinAdsManager>();
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        internal void SetPauseGame(bool isPauseBattle)
        {
            throw new NotImplementedException();
        }

        private void Start()
        {
            StartCoroutine(InitApp());
        }

        private IEnumerator InitApp()
        {
            bool statusSuccess = false;
            IAPManager.Instance.InitializeIAPManager((status, message, products) =>
            {
                if (status == IAPOperationStatus.Success)
                {
                    GetShopProducts = products;
                    statusSuccess = true;
                }
            });
            ApplovinAdsManager.Instance.InitAds();
            yield return new WaitUntil(() => statusSuccess);
            yield return new WaitUntil(() => SaveManager.Instance.GetCurrentTime.Year != 1);
            yield return StartCoroutine(InitServices());
            yield return LoadSaveData();
            yield return StartApp();
        }
        private IEnumerator StartApp()
        {
            yield return LoadLevel();
            UIManager.Instance.Init();

            AppInitAction?.Invoke();
            Tutor.TutorManager.Instance.Init();
            SoundManager.Instance.PlayMusicClip(audioLibrary.GetMusic("MusicBack_1"));
        }

        private IEnumerator LoadLevel()
        {
            CurrentLevelManager = Instantiate(levelsSettings.GetLevel(SaveManager.Instance.GetMainParameters.currentLevelIndex).GetLevelPrefab);
            yield return new WaitForSeconds(0.1f);
            CurrentLevelManager.Init();
        }

        private IEnumerator LoadSaveData()
        {
            bool isSuccess = false;
            SaveManager.Instance.LoadAllDatas((success, msg) =>
            {
                if (!success) Debug.LogError(msg);
                else Debug.Log(msg);
                isSuccess = success;
            });
            yield return new WaitUntil(predicate: () => isSuccess);
        }

        private IEnumerator InitServices()
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    GetFirebaseApp = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here to indicate whether Firebase is ready to use by your app.
                }
                else
                {
                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
            yield return null;
        }



        public void NextLevel()
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("End Level " + SaveManager.Instance.GetMainParameters.currentLevelIndex);
            CurrentLevelManager.ClearLevel();
            Destroy(CurrentLevelManager.gameObject);
            SaveManager.Instance.NextLevel();
            StartCoroutine(InvokeCor());
            IEnumerator InvokeCor()
            {
                yield return StartCoroutine(LoadLevel());
                UIManager.Instance.Reinit();
            }
        }


        public void AddAppInitListener(Action callback)
        {
            AppInitAction += callback;
        }
    }
}

