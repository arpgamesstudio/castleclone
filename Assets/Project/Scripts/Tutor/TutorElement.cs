﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Project.UI;
using Project.Core;
using System.Collections;
using TG.Audio;

namespace Project.Tutor
{
    public class TutorElement : MonoBehaviour
    {
        private Action<int, bool> action;

        public Transform root;
        public int step;
        public bool isSave;
        public bool isCenterCamOnMergeZone = false;
        public bool isHideLeftBtnsPanel = false;
        [Space(10)]
        public Button button;
        public List<Button> buttons;
        public Button copyButton;
        public Button selectButton;
        public GameObject copyObjectWithDeactivateButton;
        public GameObject copyObjectWithActivateAllChildren;
        public Button disableButton;
        public Button enableButton;
        public bool isWaitMergeAction;
        [Space(10)]
        public List<GameObject> viewElements;
        [SerializeField] private CurrencyType reward;
        [SerializeField] private int rewardCount;
       
        private void Awake()
        {
            if (button != null) button.gameObject.SetActive(false);
        }
        public void Activation() 
        {

            if (disableButton != null) disableButton.interactable = false;
            if (enableButton != null) enableButton.interactable = true;

            Invoke("CopyElements", 0.5f);
        }

        private void CopyElements()
        {
            bool hasButtons = true;
            if (buttons == null)
            {
                hasButtons = false;
            }
            else
            {
                if (buttons.Count < 1) hasButtons = false;
            }

            if (isCenterCamOnMergeZone) AppManager.Instance.CurrentLevelManager.CenterCamOnMergeZone();
            if (isHideLeftBtnsPanel) UIManager.Instance.GetGameScreen.HideLeftBtnsPanel();
            else UIManager.Instance.GetGameScreen.ShowLeftBtnsPanel();
            
            if (copyButton != null)
            {
                button.gameObject.SetActive(false);
                var newButton = Instantiate(copyButton, root);
                newButton.transform.position = copyButton.transform.position;
                newButton.onClick.AddListener(delegate { NextClick(isSave); });
                newButton.interactable = true;
            }
            else if (selectButton != null)
            {
                button.gameObject.SetActive(false);
                selectButton.onClick.AddListener(delegate { NextClick(isSave); });
                StartCoroutine(SizeEffect(selectButton.gameObject));
            }
            else if (hasButtons)
            {
                foreach (var item in buttons)
                {
                    item.onClick.AddListener(delegate { NextClick(isSave); });
                }
            }
            else if (isWaitMergeAction)
            {
                AppManager.Instance.CurrentLevelManager.AddMergeUnitListener(()=>
                {
                    NextClick(isSave);
                });
            }
            else
            {
                button.onClick.AddListener(delegate { NextClick(isSave); });
                if (button != null) Invoke("ShowButtonInvoke", 1f);
            }

            if (copyObjectWithDeactivateButton)
            {
                var copyObject = Instantiate(copyObjectWithDeactivateButton, root);
                copyObject.transform.position = copyObjectWithDeactivateButton.transform.position;
                copyObject.gameObject.SetActive(true);

                var btns = copyObject.GetComponentsInChildren<Button>();
                if (btns != null)
                {
                    foreach (var item in btns)
                    {
                        item.interactable = false;
                    }
                }
            }

            if (copyObjectWithActivateAllChildren)
            {
                var copyObject = Instantiate(copyObjectWithActivateAllChildren, root);
                copyObject.transform.position = copyObjectWithActivateAllChildren.transform.position;

                copyObject.gameObject.SetActive(true);
                for (int i = 0; i < copyObject.transform.childCount; i++)
                {
                    copyObject.transform.GetChild(i).gameObject.SetActive(true);
                }
            }
        }

        bool isSizeEffect = false;
        private IEnumerator SizeEffect(GameObject obj)
        {
            isSizeEffect = true;
            
            float duration = 0.7f;
            Vector3 startScale = obj.transform.localScale;
            Vector3 targetScale = startScale + new Vector3(0.1f, 0.1f, 0.1f);

            while (isSizeEffect)
            {
                float elapsedTime = 0f;
                while (elapsedTime < duration)
                {
                    elapsedTime += Time.deltaTime;
                    float t = Mathf.Clamp01(elapsedTime / duration);
                    obj.transform.localScale = Vector3.Lerp(startScale, targetScale, t);

                    yield return null;
                }

                elapsedTime = 0f;
                while (elapsedTime < duration)
                {
                    elapsedTime += Time.deltaTime;
                    float t = Mathf.Clamp01(elapsedTime / duration);
                    obj.transform.localScale = Vector3.Lerp(targetScale, startScale, t);

                    yield return null;
                }
            }
            obj.transform.localScale = startScale;
        }
        private void ShowButtonInvoke()
        {
            button.gameObject.SetActive(true);
        }

        public void NextClick(bool isSave) 
        {
            isSizeEffect = false;
            if (rewardCount > 0)
            {
                if (viewElements != null)
                {
                    foreach (var item in viewElements)
                    {
                        item.gameObject.SetActive(false);
                    }
                }
            }
            action?.Invoke(step, isSave);
        }
        public void AddListener(Action<int, bool> action)
        {
            this.action += action;
        }
        public void RemoveListener(Action<int, bool> action)
        {
            this.action -= action;
        }
    }
}

