using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Saves;
using TG.Audio;

namespace Project.Tutor
{
    public class TutorManager : MonoBehaviour
    {
        public static TutorManager Instance;

        [SerializeField] private List<TutorElement> tutorElements;
        [SerializeField] private GameObject skipBtn;

        private bool init = false;
        private bool endTutor;

        [HideInInspector]public bool startBattle;
        [HideInInspector] public bool endBattle;
        [HideInInspector] public bool returnCharacterToStartPos;
        public TutorElement CurrentTutor { get; private set; }
        public List<TutorElement> GetTutorElements { get { return tutorElements; } }
        public bool isEndTutor { get { return CheckTutor(); } }


        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            HideAllElements();
        }
        public void Init()
        {
            endTutor = CheckTutor();
            if (!endTutor)
            {
                skipBtn.SetActive(false);
                ShowTutorElements(GetTutor(SaveManager.Instance.CurrentTutorStep));
            }
            else { skipBtn.SetActive(false); }
            init = true;
        }


        private void HideAllElements()
        {
            if (tutorElements == null) return;
            foreach (var tutor in tutorElements)
            {
                foreach (var item in tutor.viewElements)
                {
                    item.SetActive(false);
                }
            }
        }
        public void HideTutorElements(TutorElement tutor)
        {
            foreach (var item in tutor.viewElements)
            {
                item.SetActive(false);
            }
            tutor.RemoveListener(NextStep);
        }
        public void ShowTutorElements(TutorElement tutor)
        {
            if (tutor == null) return;

            CurrentTutor = tutor;
            startBattle = false;
            endBattle = false;
            returnCharacterToStartPos = false;

            foreach (var item in tutor.viewElements)
            {
                item.SetActive(true);
            }
            tutor.AddListener(NextStep);
            tutor.Activation();
        }

        
        public TutorElement GetTutor(int step)
        {
            return tutorElements.Find(p => p.step == step);
        }

        public bool CheckTutor()
        {
            return SaveManager.Instance.CurrentTutorStep > tutorElements.Count;
        }

        private void NextStep(int step, bool isSave)
        {
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
            if (SaveManager.Instance.CurrentTutorStep == step)
            {
                HideTutorElements(GetTutor(step));
                SaveManager.Instance.AddTutorStep(isSave);
                if (!CheckTutor())
                {
                    Firebase.Analytics.FirebaseAnalytics.LogEvent("End tutor step " + step);
                    ShowTutorElements(GetTutor(SaveManager.Instance.CurrentTutorStep));
                }
                else
                {
                    Firebase.Analytics.FirebaseAnalytics.LogEvent("End tutor");
                    endTutor = true;
                }
            }
        }
    }
}

