using Project.Saves;
using Project.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.Core
{
    public class CoinObject : MonoBehaviour
    {
        [SerializeField] private Rigidbody rigidbody;
        [SerializeField] private Collider collider;
        [SerializeField] private CurrencyType currencyType;
        private float speedMin = 3f;
        private float speedMax = 5f;

        public void StartAction(int coinsValue)
        {
            Vector3 direction = new Vector3(Random.Range(0, 0.1f), 1f, Random.Range(0, 0.1f));
            rigidbody.AddForce(direction * Random.Range(speedMin, speedMax), ForceMode.Impulse);
            StartCoroutine(Mover(coinsValue));
        }

        private IEnumerator Mover(int coinsValue)
        {
            yield return new WaitForSeconds(1f);
            rigidbody.isKinematic = true;
            collider.enabled = false;

            Vector3 startPos = transform.position;
            Vector3 targetPos = UIManager.Instance.GetGameScreen.GetMoneyPanel.GetCoinsTextTranform.position;
            for (float i = 0; i < 1f; i += 0.01f)
            {
                yield return null;
                if (Vector3.Distance(transform.position, targetPos) > 0.1f)
                {
                    transform.position = Vector3.Lerp(startPos, targetPos, i);
                    transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0.2f, 0.2f, 0.2f), i);
                }
                else break;
            }

            SaveManager.Instance.AddCurrency(currencyType, coinsValue);
            UIManager.Instance.GetGameScreen.GetMoneyPanel.AddCoinsEffect();
            gameObject.SetActive(false);
        }
    }
}

