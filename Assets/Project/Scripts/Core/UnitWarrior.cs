using Project.Saves;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Core.UI;
using Project.Core.Path;
using Project.Controllers;
using DamageNumbersPro;
using Project.Core.Interfaces;
using I2.Loc;
using Project.UI;
using TG.Audio;

namespace Project.Core
{
    public class UnitWarrior : MonoBehaviour, IDamageble
    {
        [SerializeField] private UnitType unitType;
        [SerializeField] private UnitRarity unitRarity;
        [SerializeField] private float moveSpeed = 1.5f;
        [SerializeField] private float attackDelay = 3.5f;
        [SerializeField] private UnitWarriorUI unitWarriorUI;
        [SerializeField] private Projectile projectile;
        [SerializeField] private Transform firePoint;

        [SerializeField] private WarriorUnitEffectsContainer warriorUnitEffectsContainer;
        [SerializeField] private UnitWarriorAnimationController animationController;
        [SerializeField] private DamageNumber damageNumbersPrefab;

        


        private bool isDrag;
        private bool isSelect;

        private bool isDead;
        private bool isDamage;
        private bool isAttack;

        private float attackDelayTimer;

        private int mergeRank;
        private int attackDamage;
        private int hp;
        private int healing;
        private int mergeCount;

        private EnemiesUnit targetEnemy;
        private IDamageble damageTarget;

        private Vector2Int targetCellPoint;
        private List<Vector2Int> path;
        private PathFinder pathFinder;
        private Map map;
        private int cellIndex;
        private int pathStepIndex;

        private Quaternion startUIRotation;


        public bool IsMergeZone { get; private set; }
        public UnitType UnitType { get => unitType; }
        public UnitRarity UnitRarity { get => unitRarity; }
        public int GetMergeRank { get => mergeRank; }
        public int GetMergeCount { get => mergeCount; }
        public bool IsSelected { get => isSelect; }
        public bool IsDead { get => isDead;}
        public Transform GetTransform { get => transform; }

        private void Awake()
        {
            warriorUnitEffectsContainer.dragEffect.Stop();
            warriorUnitEffectsContainer.selectEffect.Stop();
            warriorUnitEffectsContainer.preMergeEffect.Stop();
            warriorUnitEffectsContainer.mergeEffect.Stop();

            startUIRotation = unitWarriorUI.transform.rotation;
        }


        public void Init(int index)
        {
            cellIndex = index;
            IsMergeZone = true;
            isSelect = false;
            isDrag = false;
            targetEnemy = null;
            path = null;
            isDead = false;
            isDamage = false;
            isAttack = false;
            attackDelayTimer = 0;


            animationController.AddListenerEndAttack(OnEndAttack);
            animationController.AddListenerEndDamage(OnEndDamage);
            animationController.AddListenerOnAttack(OnAttack);


            var data = SaveManager.Instance.GetLevelData.GetCell(cellIndex);
            if (data != null)
            {
                Vector2Int pos = new Vector2Int(data.unitXPos, data.unitYPos);
                if (pos != AppManager.Instance.CurrentLevelManager.GetMergeCell(index).GetMapPosition)
                {
                    transform.position = new Vector3(pos.x, transform.position.y, pos.y);
                    hp = data.unitHp;
                    AppManager.Instance.CurrentLevelManager.GetMap.CloseCell(pos);
                    IsMergeZone = false;
                }
            }
        }

        private void OnAttack()
        {
            if (damageTarget == null) return;

            if (targetEnemy != null)
            {
                targetEnemy.AddTargetUnit(this);
            }
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("Attack"));
            if (unitType == UnitType.Mellee)
                damageTarget.Damage(attackDamage);
            else
            {
                var prj = Instantiate(projectile, firePoint);
                prj.transform.position = firePoint.position;
                prj.transform.parent = null;
                prj.Init(damageTarget, attackDamage, firePoint.transform);
            }
        }


        private void OnEndDamage()
        {
            isDamage = false;
        }

        private void OnEndAttack()
        {
            isAttack = false;
        }

        internal void SetParameters(int rank, int attack, int hp, int healing)
        {
            this.mergeRank = rank;
            this.attackDamage = attack;
            this.hp = hp;
            this.healing = healing;
            mergeCount = rank - SaveManager.Instance.GetMainParameters.GetUnitRank(unitType);
            unitWarriorUI.SetMergeStars(mergeCount);
        }
        internal void NextRank(int cellIndex)
        {
            mergeRank++;
            mergeCount = mergeRank - SaveManager.Instance.GetMainParameters.GetUnitRank(unitType);
            attackDamage = AppManager.Instance.GetBalance.GetUnitAttackValue(mergeRank, unitRarity, unitType);
            hp = AppManager.Instance.GetBalance.GetUnitHpValue(mergeRank, unitRarity, unitType);
            healing = AppManager.Instance.GetBalance.GetMageHealingValue(mergeRank, unitRarity);
            SaveManager.Instance.LevelSetCell(cellIndex, true, unitType, hp, mergeCount);
            warriorUnitEffectsContainer.mergeEffect.Play();
            unitWarriorUI.SetMergeStars(mergeCount);
        }
        internal void UpdateAction()
        {
            unitWarriorUI.transform.rotation = startUIRotation;
            if (isDrag || isDead) return;

            if (!IsMergeZone && !isSelect)
            {
                if (damageTarget == null) unitWarriorUI.StatesView(false);
            }
            
            if (damageTarget != null)
            {
                if (path == null)
                {
                    GetPath();
                }
                else
                {
                    if (!Move())
                    {
                        animationController.Move(0);
                        if (!isAttack)
                        {
                            if (!AttackAction())
                            {
                                isAttack = false;
                                damageTarget = null;
                                targetEnemy = null;
                                path = null;
                                isSelect = false;
                            }
                        }
                    }
                }
            }
        }

        private bool AttackAction()
        {
            if (path == null) return false;
            if (targetCellPoint == Vector2Int.zero) return false;
            if (damageTarget.IsDead) return false;

            Vector2 currentPoint = new Vector2(transform.position.x, transform.position.z);
            Vector2 direction = new Vector2(damageTarget.GetTransform.position.x, damageTarget.GetTransform.position.z) - currentPoint;
            transform.forward = new Vector3(direction.x, 0, direction.y).normalized;

            if (attackDelayTimer > 0) attackDelayTimer -= Time.deltaTime;
            else
            {
                //Attack
                attackDelayTimer = AppManager.Instance.GetBalance.GetUnitAttackDelay(attackDelay, unitRarity);
                unitWarriorUI.StatesView(true, hp, attackDamage);
                isAttack = true;
                animationController.Attack();
            }

            return true;
        }

        private bool Move()
        {
            if (path == null) return false;
            if (targetCellPoint == Vector2Int.zero) return false;

            Vector2 currentPoint = new Vector2(transform.position.x, transform.position.z);

            if ((float)Vector2.Distance(targetCellPoint, currentPoint) > 0.05f)
            {
                //Move
                if ((float)Vector2.Distance(currentPoint, path[pathStepIndex]) < 0.05f) pathStepIndex++;
                else
                {
                    Vector2 direction = path[pathStepIndex] - currentPoint;
                    transform.position += new Vector3(direction.normalized.x, 0, direction.normalized.y) * moveSpeed * Time.deltaTime;
                    transform.forward = new Vector3(direction.x, 0, direction.y).normalized;
                    animationController.Move((direction.normalized * moveSpeed).magnitude);
                }
            }
            else
            {
                transform.position = new Vector3(targetCellPoint.x, transform.position.y, targetCellPoint.y);
                return false;
            }
            return true;
        }

        private void GetPath()
        {
            Vector2Int currentPoint = new Vector2Int((int)transform.position.x, (int)transform.position.z);
            targetCellPoint = CalculateTargetPoint(currentPoint);
            if (targetCellPoint == Vector2Int.zero)
            {
                Debug.LogError("Not path!");
                UIManager.Instance.GetMessagePopup.ShowMessage(MessagePopup.TextName.NoPath);
                if (AppManager.Instance.CurrentLevelManager.GetMergeCell(cellIndex).GetMapPosition == currentPoint) IsMergeZone = true;
                damageTarget = null;
                targetEnemy = null;
                return;
            }
            if (currentPoint == targetCellPoint)
            {
                path = new List<Vector2Int>();
                path.Add(targetCellPoint);
                map.CloseCell(targetCellPoint);
                return;
            }
            path = pathFinder.GetPath(currentPoint, targetCellPoint);
            pathStepIndex = 0;
            map.CloseCell(targetCellPoint);
            SaveManager.Instance.UnitSetPos(cellIndex, targetCellPoint.x, targetCellPoint.y);
        }

        private Vector2Int CalculateTargetPoint(Vector2Int currentPoint)
        {
            Vector2Int enemyPoint = new Vector2Int((int)damageTarget.GetTransform.position.x, (int)damageTarget.GetTransform.position.z);
            List<Vector2Int> targetPoints = new List<Vector2Int>();
            Vector2Int targetPoint = Vector2Int.zero;

            switch (unitType)
            {
                case UnitType.Mellee:
                    Vector2Int downPoint = new Vector2Int(enemyPoint.x, enemyPoint.y - 1);
                    Vector2Int leftPoint = new Vector2Int(enemyPoint.x - 1, enemyPoint.y);
                    Vector2Int rightPoint = new Vector2Int(enemyPoint.x + 1, enemyPoint.y);
                    Vector2Int upPoint = new Vector2Int(enemyPoint.x, enemyPoint.y + 1);
                    targetPoints.AddRange(new List<Vector2Int> { downPoint, leftPoint, rightPoint, upPoint });
                    break;
                case UnitType.Archer:
                    Vector2Int downPoint1 = new Vector2Int(enemyPoint.x, enemyPoint.y - 1);
                    Vector2Int leftPoint1 = new Vector2Int(enemyPoint.x - 1, enemyPoint.y);
                    Vector2Int rightPoint1 = new Vector2Int(enemyPoint.x + 1, enemyPoint.y);
                    Vector2Int upPoint1 = new Vector2Int(enemyPoint.x, enemyPoint.y + 1);
                    Vector2Int downPoint2 = new Vector2Int(enemyPoint.x, enemyPoint.y - 2);
                    Vector2Int leftPoint2 = new Vector2Int(enemyPoint.x - 2, enemyPoint.y);
                    Vector2Int rightPoint2 = new Vector2Int(enemyPoint.x + 2, enemyPoint.y);
                    Vector2Int upPoint2 = new Vector2Int(enemyPoint.x, enemyPoint.y + 2);
                    targetPoints.AddRange(new List<Vector2Int> { downPoint2, leftPoint2, rightPoint2, upPoint2, downPoint1, leftPoint1, rightPoint1, upPoint1 });
                    break;
                case UnitType.Mage:
                    downPoint1 = new Vector2Int(enemyPoint.x, enemyPoint.y - 1);
                    leftPoint1 = new Vector2Int(enemyPoint.x - 1, enemyPoint.y);
                    rightPoint1 = new Vector2Int(enemyPoint.x + 1, enemyPoint.y);
                    upPoint1 = new Vector2Int(enemyPoint.x, enemyPoint.y + 1);
                    downPoint2 = new Vector2Int(enemyPoint.x, enemyPoint.y - 2);
                    leftPoint2 = new Vector2Int(enemyPoint.x - 2, enemyPoint.y);
                    rightPoint2 = new Vector2Int(enemyPoint.x + 2, enemyPoint.y);
                    upPoint2 = new Vector2Int(enemyPoint.x, enemyPoint.y + 2);
                    targetPoints.AddRange(new List<Vector2Int> { downPoint2, leftPoint2, rightPoint2, upPoint2, downPoint1, leftPoint1, rightPoint1, upPoint1 });
                    break;
                case UnitType.Catapult:
                    downPoint1 = new Vector2Int(enemyPoint.x, enemyPoint.y - 1);
                    leftPoint1 = new Vector2Int(enemyPoint.x - 1, enemyPoint.y);
                    rightPoint1 = new Vector2Int(enemyPoint.x + 1, enemyPoint.y);
                    upPoint1 = new Vector2Int(enemyPoint.x, enemyPoint.y + 1);
                    downPoint2 = new Vector2Int(enemyPoint.x, enemyPoint.y - 2);
                    leftPoint2 = new Vector2Int(enemyPoint.x - 2, enemyPoint.y);
                    rightPoint2 = new Vector2Int(enemyPoint.x + 2, enemyPoint.y);
                    upPoint2 = new Vector2Int(enemyPoint.x, enemyPoint.y + 2);
                    targetPoints.AddRange(new List<Vector2Int> { downPoint2, leftPoint2, rightPoint2, upPoint2, downPoint1, leftPoint1, rightPoint1, upPoint1 });
                    break;
                default:
                    break;
            }

            List<Vector2Int> lastPath = null;
            foreach (var point in targetPoints)
            {
                if (map.GetCell(point) == null) continue;
                if (AppManager.Instance.CurrentLevelManager.GetMergeCell(point) != null) continue;
                var pt = pathFinder.GetPath(currentPoint, point);
                if (currentPoint == point)
                {
                    lastPath = new List<Vector2Int>();
                    lastPath.Add(point);
                    break;
                }
                if (pt != null)
                {
                    if (lastPath == null) lastPath = pt;
                    else
                    {
                        if (pt.Count < lastPath.Count) lastPath = pt;
                    }
                }
            }
            if (lastPath != null)
            {
                targetPoint = lastPath[lastPath.Count - 1];
            }

            return targetPoint;
        }

        internal void OnDrag(bool isDrag)
        {
            this.isDrag = isDrag;
            if (isDrag) warriorUnitEffectsContainer.dragEffect.Play();
            else warriorUnitEffectsContainer.dragEffect.Stop();
        }
        internal void OnPreMerge(bool active)
        {
            if (active)
            {
                if (!warriorUnitEffectsContainer.preMergeEffect.isPlaying) warriorUnitEffectsContainer.preMergeEffect.Play();
            }
            else warriorUnitEffectsContainer.preMergeEffect.Stop();
        }
        internal void OnSelect(bool isSelect)
        {
            if (damageTarget != null && isSelect == true) return;
            this.isSelect = isSelect;
            unitWarriorUI.StatesView(isSelect, hp, attackDamage);
            if (isSelect == true && !warriorUnitEffectsContainer.selectEffect.isPlaying) warriorUnitEffectsContainer.selectEffect.Play();
            else warriorUnitEffectsContainer.selectEffect.Stop();
        }

        internal void Attack(EnemiesUnit target, IDamageble damageTarget, Path.PathFinder pathFinder, Map map)
        {
            if (this.damageTarget != null) return;
            if (damageTarget.IsDead) return;
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("MoveToAttack"));
            attackDelayTimer = 0;
            isSelect = false;
            this.damageTarget = damageTarget;
            targetEnemy = target;
            IsMergeZone = false;
            OnSelect(false);
            this.pathFinder = pathFinder;
            this.map = map;
            Vector2Int currentPoint = new Vector2Int((int)transform.position.x, (int)transform.position.z);
            if (CalculateTargetPoint(currentPoint) != Vector2Int.zero)
            {
                map.OpenCell(currentPoint);
            }
        }

        public void Damage(int damage)
        {
            hp -= damage;
            
            SaveManager.Instance.RemoveWarriorUnitHp(cellIndex, damage);
            if (hp <= 0 && !IsDead)
            {
                hp = 0;
                isDead = true;
                SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("PlayerDie"));
                animationController.Die();
                targetEnemy = null;
                damageTarget = null;
                unitWarriorUI.StatesView(false);
                unitWarriorUI.HideMergeStars();
                StartCoroutine(DestroyObject());
                var pos = AppManager.Instance.CurrentLevelManager.GetMergeCell(cellIndex).GetMapPosition;
                SaveManager.Instance.UnitSetPos(cellIndex, pos.x, pos.y);
            }
            else
            {
                SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("PlayerDamage"));
                unitWarriorUI.StatesView(true, hp, attackDamage);
            }
            
            isAttack = false;
            damageNumbersPrefab.CreateNew(damage, transform.position);
        }

        private IEnumerator DestroyObject()
        {
            yield return new WaitForSeconds(3f);
            Vector2Int currentPoint = new Vector2Int((int)transform.position.x, (int)transform.position.z);
            map.OpenCell(currentPoint);
            float targetPosY = -2f;
            while (true)
            {
                yield return null;
                transform.position -= new Vector3(0, 0.01f, 0);
                if (transform.position.y < targetPosY) break;
            }

            var cell = AppManager.Instance.CurrentLevelManager.GetMergeCell(cellIndex);
            if (cell != null) cell.DestroyUnit();
        }
    }
}

