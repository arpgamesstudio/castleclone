using DamageNumbersPro;
using Project.Core.Interfaces;
using Project.Core.UI;
using Project.Saves;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.Core
{
    public class Chest : MonoBehaviour, IDamageble
    {
        [SerializeField] private Animator animator;
        [SerializeField] private GameObject viewObj;
        [SerializeField] private int coinsCount;
        [SerializeField] private int diamondsCount;
        [SerializeField] private int hp;
        [SerializeField] private float respawnTimeoutAfterDestroySec;
        [SerializeField] private DamageNumber damageNumbersPrefab;
        [SerializeField] private ChestUI chestUI;
        [SerializeField] private CoinsContainer coinsContainer;


        private int index;
        private Quaternion startUIRotation;
        private int currentHp;
        private DateTime currentTimer;
        private bool isOpen = false;
        private bool isInit = false;
        private DateTime startTime;
        private float hideUITimer = 5f;

        public Transform GetTransform => transform;
        public bool IsDead => isOpen;

        public int GetMaxHp { get => hp;}

        private void Start()
        {
            chestUI.AddRewardedAdViewSuccessListener(GetAdsReward);
        }

        private void GetAdsReward(bool success)
        {
            SaveManager.Instance.MinusChestTime(index, 1800);
            var saveData = SaveManager.Instance.GetLevelData.GetChestData(index);
            if (saveData != null)
            {
                startTime = DateTime.Parse(saveData.startTime);
            }
        }

        public void Init(int index)
        {
            startUIRotation = chestUI.transform.rotation;
            this.index = index;
            
            var saveData = SaveManager.Instance.GetLevelData.GetChestData(index);
            if (saveData != null)
            {
                isOpen = saveData.isOpen;
                currentHp = saveData.hp;
                if (isOpen)
                {
                    startTime = DateTime.Parse(saveData.startTime);
                    Open();
                }
            }
            else SetBaseParameters();
            chestUI.ViewPanels(isOpen);
            chestUI.SetHp(currentHp);
            isInit = true;
        }



        private void Update()
        {
            chestUI.transform.rotation = startUIRotation;
            if (!isInit) return;

            if (isOpen)
            {
                var timeRemainder = SaveManager.Instance.GetCurrentTime - startTime;
                var time = respawnTimeoutAfterDestroySec - timeRemainder.TotalSeconds;
                TimeSpan span = TimeSpan.FromSeconds(time);
                chestUI.SetTimer(span.ToString());
                if (time <= 0) Close();
            }
            else
            {
                if (hideUITimer < 4f) hideUITimer += Time.deltaTime;
                else chestUI.ShowStatesPanel(false);
            }
        }
        public void SetBaseParameters()
        {
            currentHp = hp;
            isOpen = false;
            chestUI.SetHp(currentHp);
            chestUI.ViewPanels(isOpen);
            animator.SetBool("IsOpen", false);
        }
        public void Damage(int damage)
        {
            currentHp -= damage;
            hideUITimer = 0;
            chestUI.ShowStatesPanel(true);
            if (currentHp <= 0)
            {
                Open();
                coinsContainer.StartAction(coinsCount, diamondsCount);
                startTime = SaveManager.Instance.GetCurrentTime;
            }
            damageNumbersPrefab.CreateNew(damage, transform.position);
            chestUI.SetHp(currentHp);
            animator.SetTrigger("Damage");
            SaveManager.Instance.RemoveChestHp(index, damage);
        }

        private void Open()
        {
            isOpen = true;
            chestUI.ViewPanels(isOpen);
            chestUI.ShowStatesPanel(false);
            animator.SetBool("IsOpen", isOpen);
        }

        private void Close()
        {
            SetBaseParameters();
            SaveManager.Instance.CloseChest(index, hp);
        }
    }
}

