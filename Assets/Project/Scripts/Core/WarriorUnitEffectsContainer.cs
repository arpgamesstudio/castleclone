using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorUnitEffectsContainer : MonoBehaviour
{
    [Space(10), Header("Effects")]
    public ParticleSystem dragEffect;
    public ParticleSystem selectEffect;
    public ParticleSystem preMergeEffect;
    public ParticleSystem mergeEffect;
}
