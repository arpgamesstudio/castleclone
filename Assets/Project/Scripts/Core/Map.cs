﻿using System.Collections.Generic;
using UnityEngine;

namespace Project.Core.Path
{
    public class Map
    {
        public readonly List<Cell> cells;

        public Map(List<Transform> allCells, List<Transform> impassableCells, List<Gate> gates)
        {
            cells = new List<Cell>();
            foreach (var cell in allCells)
            {
                Vector2Int position = new Vector2Int((int)cell.position.x, (int)cell.position.z);
                Transform impCell = impassableCells.Find(p => new Vector2Int((int)p.position.x, (int)p.position.z) == position);
                Transform gateCell = null;
                foreach (var gate in gates)
                {
                    var gateCl = gate.GetCells.Find(p => new Vector2Int((int)p.position.x, (int)p.position.z) == position);
                    if (gateCl != null) gateCell = gateCl;
                }
                CellType cellType = CellType.Passable;
                if (impCell != null || gateCell != null) cellType = CellType.Impassable;

                Cell mapCell = new Cell(position, cellType);
                cells.Add(mapCell);
            }
        }

        public Cell GetCell(Vector2Int position)
        {
            return cells.Find(p => p.position == position);
        }
        public void OpenCell(Vector2Int position)
        {
            cells.Find(p => p.position == position)?.SetCellType(CellType.Passable);
        }
        public void CloseCell(Vector2Int position)
        {
            cells.Find(p => p.position == position)?.SetCellType(CellType.Impassable);
        }

    }
}

