using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.Core
{
    public class CoinsContainer : MonoBehaviour
    {
        [SerializeField] private CoinObject coinObjectPrefab;
        [SerializeField] private CoinObject diamondObjectPrefab;
        [SerializeField] private int viewPoolObjsCountMin = 5;
        [SerializeField] private int viewPoolObjsCountMax = 10;

        private List<CoinObject> coinObjects;
        private List<CoinObject> diamondObjects;


        private void Start()
        {
            coinObjects = new List<CoinObject>();
            for (int i = 0; i < Random.Range(viewPoolObjsCountMin, viewPoolObjsCountMax); i++)
            {
                var coin = Instantiate(coinObjectPrefab, transform);
                coin.gameObject.SetActive(false);
                coin.transform.position = transform.position;
                coinObjects.Add(coin);
            }
            diamondObjects = new List<CoinObject>();
            for (int i = 0; i < Random.Range(viewPoolObjsCountMin, viewPoolObjsCountMax); i++)
            {
                var diamond = Instantiate(diamondObjectPrefab, transform);
                diamond.gameObject.SetActive(false);
                diamond.transform.position = transform.position;
                diamondObjects.Add(diamond);
            }
        }
        public void StartAction(int allCoinsValue, int allDiamondValue)
        {
            StartCoroutine(ActionCoroutine(allCoinsValue, allDiamondValue));  
        }

        private IEnumerator ActionCoroutine(int allCoinsValue, int allDiamondValue)
        {
            yield return null;
            if (allCoinsValue > 0)
            {
                int countPerOnceCoin = allCoinsValue / coinObjects.Count;
                foreach (var coin in coinObjects)
                {
                    coin.gameObject.SetActive(true);
                    coin.StartAction(countPerOnceCoin);
                    yield return null;
                }
            }

            if (allDiamondValue > 0)
            {
                int countPerOnceDiamond = allDiamondValue / diamondObjects.Count;
                foreach (var diamond in diamondObjects)
                {
                    diamond.gameObject.SetActive(true);
                    diamond.StartAction(countPerOnceDiamond);
                    yield return null;
                }
            }
        }
    }
}

