using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Core.Interfaces;


namespace Project.Core
{
    public class Projectile : MonoBehaviour
    {
        private enum ProjectileType
        {
            Arrow,
            MagicOrb,
            CatapultRock
        }
        [SerializeField] private ProjectileType projectileType;
        [SerializeField] private Collider collider;
        [SerializeField] private float speed = 15f;
        [SerializeField] private float angle = 45f;
        [SerializeField] private ParticleSystem hitEffect;


        private IDamageble targetEnemy;
        Rigidbody rb;
        private int attackDamage;

        public void Init(IDamageble enemy, int attackDamage, Transform firePoint)
        {
            this.attackDamage = attackDamage;
            hitEffect.Stop();
            targetEnemy = enemy;
            rb = GetComponent<Rigidbody>();
            var direction = (targetEnemy.GetTransform.position - transform.position);
            
            if (projectileType == ProjectileType.Arrow) direction += new Vector3(0, -0.5f, 0);
            if (projectileType == ProjectileType.CatapultRock)
            {
                float g = Physics.gravity.y;
                var directionXZ = new Vector3(direction.x, 0, direction.z);
                float x = directionXZ.magnitude;
                float y = direction.y;
                float angleInRadians = angle * Mathf.PI / 180f;
                float v2 = (g * x * x) / (2 * (y - Mathf.Tan(angleInRadians) * x) * Mathf.Pow(Mathf.Cos(angleInRadians), 2));
                float v = Mathf.Sqrt(Mathf.Abs(v2));
                firePoint.localEulerAngles = new Vector3(-angle, 0, 0);
                rb.velocity = firePoint.forward * v;
            }
            else
            {
                rb.AddForce(direction.normalized * speed, ForceMode.Impulse);
            }
        }


        private void OnCollisionEnter(Collision collision)
        {
            if (targetEnemy == null) return;
            hitEffect.Play();


            if (projectileType == ProjectileType.Arrow || projectileType == ProjectileType.MagicOrb)
            {
                targetEnemy.Damage(attackDamage);
                transform.Translate(transform.forward * 0.2f);
            }
            else if(projectileType == ProjectileType.CatapultRock)
            {
                targetEnemy.Damage(attackDamage);
                Vector2Int enemyPoint = new Vector2Int((int)targetEnemy.GetTransform.position.x, (int)targetEnemy.GetTransform.position.z);
                Vector2Int downPoint = new Vector2Int(enemyPoint.x, enemyPoint.y - 1);
                Vector2Int leftPoint = new Vector2Int(enemyPoint.x - 1, enemyPoint.y);
                Vector2Int rightPoint = new Vector2Int(enemyPoint.x + 1, enemyPoint.y);
                Vector2Int upPoint = new Vector2Int(enemyPoint.x, enemyPoint.y + 1);
                var downEnemy = AppManager.Instance.CurrentLevelManager.GetEnemy(downPoint);
                if (downEnemy != null)
                {
                    if (!downEnemy.IsDead) downEnemy.Damage(attackDamage / 2);
                }
                var leftEnemy = AppManager.Instance.CurrentLevelManager.GetEnemy(leftPoint);
                if (leftEnemy != null)
                {
                    if (!leftEnemy.IsDead) leftEnemy.Damage(attackDamage / 2);
                }
                var rightEnemy = AppManager.Instance.CurrentLevelManager.GetEnemy(rightPoint);
                if (rightEnemy != null)
                {
                    if (!rightEnemy.IsDead) rightEnemy.Damage(attackDamage / 2);
                }
                var upEnemy = AppManager.Instance.CurrentLevelManager.GetEnemy(upPoint);
                if (upEnemy != null)
                {
                    if (!upEnemy.IsDead) upEnemy.Damage(attackDamage / 2);
                }
            }
            transform.parent = collision.collider.transform;
            rb.isKinematic = true;
            collider.enabled = false;
            targetEnemy = null;
            Invoke("DestroyObj", 0.5f);
        }
        private void DestroyObj()
        {
            Destroy(gameObject);
            Destroy(this);
        }
    }
}

