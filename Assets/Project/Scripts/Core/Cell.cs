﻿using UnityEngine;

namespace Project.Core.Path
{
    public enum CellType
    {
        Passable,
        Impassable
    }
    public class Cell
    {
        public readonly Vector2Int position;

        private CellType cellType;
        public CellType GetCellType { get => cellType; }

        public Cell(Vector2Int position, CellType cellType)
        {
            this.position = position;
            this.cellType = cellType;
        }

        public void SetCellType(CellType cellType) { this.cellType = cellType; }
    }
}

