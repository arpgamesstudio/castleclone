using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Settings;
using System;
using Project.Core.Path;
using UnityEngine.EventSystems;
using Project.Controllers;
using Project.UI;
using Project.Saves;
using Project.Core.Interfaces;
using TG.Audio;


namespace Project.Core
{
    public partial class LevelManager : MonoBehaviour
    {
        [SerializeField] private List<MergeCell> mergeFieldCells;
        [SerializeField] private List<Transform> gameFieldCells;
        [SerializeField] private List<Transform> impassableCells;
        [SerializeField] private List<Gate> gates;
        [SerializeField] private List<EnemiesUnit> enemies;
        [SerializeField] private List<Chest> chests;

        [Space(20)]
        [SerializeField] private MoveCamBorderLevels moveCamBorderLevels;

        MoveCamController camController;

        private Action mergeUnitCallback;

        private UnitBuyBtn[] unitBuyBtns;
        private Map map;
        private PathFinder pathFinder;

        public List<MergeCell> GetMergeFieldCells { get => mergeFieldCells; }
        public List<Gate> GetGates { get => gates; }
        public List<EnemiesUnit> GetEnemies { get => enemies; }
        public List<Chest> GetChests { get => chests; }

        public MergeCell GetMergeCell(int index) => mergeFieldCells.Find(p => p.GetIndex == index);
        public UnitBuyBtn[] GetUnitBuyBtns { get => unitBuyBtns; }
        public Map GetMap { get => map;}

        public MergeCell GetMergeCell(Vector2Int point) => mergeFieldCells.Find(p => p.GetMapPosition == point);
        public int GetFreeMergeCellsCount
        {
            get
            {
                int count = 0;
                foreach (var cell in mergeFieldCells)
                {
                    if (!cell.GetIsLock)
                    {
                        if (!cell.GetIsOccupied) count++;
                    }
                }
                return count;
            }
        }
        public EnemiesUnit GetEnemy(Vector2Int point) => enemies.Find(p => p.GetMapPosition == point);
        public int AllEnemiesCount { get => enemies.Count; }
        public int DeadEnemiesCount
        {
            get
            {
                int count = 0;
                foreach (var enemy in enemies)
                {
                    if (enemy.IsDead) count++;
                }
                return count;
            }
        }
        public Vector2Int GetMapPosition => new Vector2Int((int)transform.position.x, (int)transform.position.y);
        public bool IsInit { get; private set; }




        public void Init()
        {
            List<Transform> cellsField = new List<Transform>();
            foreach (var cell in mergeFieldCells) { cellsField.Add(cell.transform); }
            foreach (var cell in gameFieldCells) { cellsField.Add(cell); }
            map = new Map(cellsField, impassableCells, gates);
            pathFinder = new PathFinder(map);

            foreach (var item in mergeFieldCells)
            {
                item.Init();
                item.AddListenerClick(ClickMergeCell);
            }
            camController = FindObjectOfType<MoveCamController>();
            camController?.SetBorders(moveCamBorderLevels); 

            unitBuyBtns = FindObjectsOfType<UnitBuyBtn>();
            foreach (var item in unitBuyBtns)
            {
                item.Init();
                item.AddListenerClick(BuyBtnClick);
            }

            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].SetBaseParameters(map);
                enemies[i].Init(i);
            }

            foreach (var gate in gates)
            {
                foreach (var cell in gate.GetCells)
                {
                    Vector2Int currentPoint = new Vector2Int((int)cell.transform.position.x, (int)cell.transform.position.z);
                    map.CloseCell(currentPoint);
                }
            }
            for (int i = 0; i < chests.Count; i++)
            {
                chests[i].SetBaseParameters();
                chests[i].Init(i);
                Vector2Int currentPoint = new Vector2Int((int)chests[i].transform.position.x, (int)chests[i].transform.position.z);
                map.CloseCell(currentPoint);
            }
            IsInit = true;
            //TestUnitBalance(UnitType.Mellee);
        }

        public void CenterCamOnMergeZone()
        {
            camController.CenterCamOnPosition(new Vector3(0, 0, moveCamBorderLevels.backBorder));
        }

        internal void ClearLevel()
        {
            foreach (var cell in mergeFieldCells)
            {
                cell.Clear();
            }
        }

        private void BuyBtnClick(UnitType unitType, UnitRarity unitRarity, int rank, int price, int attack, int hp, int healing)
        {
            MergeCell cell = null;

            for (int i = 0; i <= mergeFieldCells.Count; i++)
            {
                cell = mergeFieldCells.Find(p => p.GetIndex == i);
                if (cell == null) continue;
                if (!cell.GetIsLock)
                {
                    if (!cell.GetIsOccupied)
                    {
                        if (price <= SaveManager.Instance.GetMainParameters.coins)
                        {
                            SaveManager.Instance.RemoveCurrency(CurrencyType.Coins, price);
                            SaveManager.Instance.LevelSetCell(i, true, unitType, hp);
                            cell.SpawnUnit(unitType, unitRarity, rank, attack, hp, unitType == UnitType.Mage ? healing : 0);
                            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("BuySuccess"));
                            break;
                        }
                    }
                }
            }
        }
        
        private void TestUnitBalance(UnitType unitType)
        {
            for (int r = 0; r < 7; r++)
            {
                UnitRarity rarity;
                switch (r)
                {
                    case 0:
                        rarity = UnitRarity.Normal;
                        break;
                    case 1:
                        rarity = UnitRarity.Improved;
                        break;
                    case 2:
                        rarity = UnitRarity.Rare;
                        break;
                    case 3:
                        rarity = UnitRarity.Unique;
                        break;
                    case 4:
                        rarity = UnitRarity.Legendary;
                        break;
                    case 5:
                        rarity = UnitRarity.Mythical;
                        break;
                    case 6:
                        rarity = UnitRarity.Immortal;
                        break;
                    default:
                        rarity = UnitRarity.Normal;
                        break;
                }

                for (int i = 0; i < 6; i++)
                {
                    Debug.Log(AppManager.Instance.GetBalance.GetUnitHpValue(i, rarity, unitType));
                }
            }
        }



        
        private void Update()
        {
            ClickAndDragObjects();
            foreach (var enemy in enemies)
            {
                if (enemy != null)
                {
                    if (enemy.IsInit && !enemy.IsDead)
                    {
                        enemy.UpdateAction();
                    }
                }
            }
        }



        #region DragAndMergeUnits

        bool isDownTap = false;
        const float DOWN_TAP_TO_MERGE_TIME = 0.4f;
        float downTapToMergeTimer = 0;
        GameObject hitEmptyObj;
        UnitWarrior unitWariorHit = null;
        UnitWarrior unitWariorSelected = null;
        Vector3 unitWarriorStartDragPosition;
        bool isDragUnit;
        private void ClickAndDragObjects()
        {
            if (Input.GetMouseButtonDown(0) && !IsPointerOverGameObject())
            {
                isDragUnit = false;
                isDownTap = true;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                
                if (Physics.Raycast(ray, out hit))
                {
                    MergeCell cell = hit.collider.GetComponent<MergeCell>();
                    if (cell != null)
                    {
                        cell.Click();
                    }
                    unitWariorHit = hit.collider.GetComponent<UnitWarrior>();

                    EnemiesUnit enemiesUnit = hit.collider.GetComponent<EnemiesUnit>();
                    IDamageble damageble = hit.collider.GetComponent<IDamageble>();

                    if (damageble != null && unitWariorHit == null)
                    {
                        if (unitWariorSelected != null)
                        {
                            unitWariorSelected.Attack(enemiesUnit, damageble, pathFinder, map);

                            unitWariorSelected = null;
                        }
                        else
                        {
                            if (enemiesUnit != null) enemiesUnit.Click();
                        }
                    }


                    if (unitWariorHit == null && enemiesUnit == null)
                    {
                        hitEmptyObj = hit.collider.gameObject;
                    }
                    else hitEmptyObj = null;
                }
            }



            if (Input.GetMouseButtonUp(0) && !IsPointerOverGameObject())
            {
                if (isDragUnit) MergeUnit();
                else
                {
                    if (unitWariorHit != null)
                    {
                        foreach (var cell in mergeFieldCells)
                        {
                            if (cell.GetUnitWarrior != null)
                            {
                                if (cell.GetUnitWarrior.IsSelected)
                                {
                                    cell.GetUnitWarrior.OnSelect(false);
                                    unitWariorSelected = null;
                                }
                            }
                        }
                        unitWariorHit.OnSelect(true);
                        unitWariorSelected = unitWariorHit;
                        SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("SelectPlayerUnit"));
                    }
                    unitWariorHit = null;
                }
                isDragUnit = false;
                isDownTap = false;
                downTapToMergeTimer = 0;

                if (hitEmptyObj != null && !camController.IsMove)
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider.gameObject == hitEmptyObj)
                        {
                            foreach (var cell in mergeFieldCells)
                            {
                                if (cell.GetUnitWarrior != null)
                                {
                                    if (cell.GetUnitWarrior.IsSelected)
                                    {
                                        cell.GetUnitWarrior.OnSelect(false);
                                    }
                                }
                            }
                            unitWariorSelected = null;
                            foreach (var enemy in enemies)
                            {
                                enemy.Deselect();
                            }
                        }
                    }
                    else
                    {
                        hitEmptyObj = null;
                    }
                }
            }

            if (isDownTap)
            {
                downTapToMergeTimer += Time.deltaTime;
                if (downTapToMergeTimer >= DOWN_TAP_TO_MERGE_TIME)
                {
                    if (unitWariorHit != null)
                    {
                        if (unitWariorHit.IsMergeZone && !isDragUnit && unitWariorHit.GetMergeCount < 16)
                        {
                            isDragUnit = true;
                            unitWarriorStartDragPosition = unitWariorHit.transform.position;
                            unitWariorHit.transform.position += new Vector3(0, 0.5f, 0);
                            unitWariorHit.OnDrag(true);
                            foreach (var cell in mergeFieldCells)
                            {
                                if (cell.GetUnitWarrior != null)
                                {
                                    if (cell.GetUnitWarrior.IsSelected)
                                    {
                                        cell.GetUnitWarrior.OnSelect(false);
                                        unitWariorSelected = null;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (isDragUnit)
            {
                AppManager.Instance.IsBlockedScreenScroll = true;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                Vector2Int unitDragMapCurrentPos = new Vector2Int();

                RaycastHit[] hits = Physics.RaycastAll(ray, Mathf.Infinity, ~SortingLayer.GetLayerValueFromName("UnitWarrior"));
                foreach (var hit in hits)
                {
                    Vector3 newPos = new Vector3(hit.point.x, unitWariorHit.transform.position.y, hit.point.z);
                    float maxZPos = mergeFieldCells.Find(p => p.GetIndex == 1).transform.position.z;
                    if (newPos.z > maxZPos) newPos.z = maxZPos;

                    unitWariorHit.transform.position = newPos;

                    var cell = hit.collider.GetComponent<MergeCell>();
                    if (cell != null)
                    {
                        unitDragMapCurrentPos = new Vector2Int((int)cell.transform.position.x, (int)cell.transform.position.z);
                    }
                }
                
                foreach (var cell in mergeFieldCells)
                {
                    var unit = cell.GetUnitWarrior;
                    if (unit != null)
                    {
                        if (unit == unitWariorHit) continue;
                        if (unit.IsMergeZone)
                        {
                            if (unit.UnitType == unitWariorHit.UnitType && unit.UnitRarity == unitWariorHit.UnitRarity && unit.GetMergeRank == unitWariorHit.GetMergeRank)
                            {
                                Vector2Int unitMapPos = new Vector2Int((int)unit.transform.position.x, (int)unit.transform.position.z);
                                unit.OnPreMerge(true);
                                if (unitMapPos == unitDragMapCurrentPos)
                                    unit.transform.position = new Vector3(unit.transform.position.x, cell.transform.position.y + 0.2f, unit.transform.position.z);
                                else unit.transform.position = cell.transform.position;
                            }
                            else
                            {
                                unit.OnPreMerge(false);
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var cell in mergeFieldCells)
                {
                    if (cell.GetUnitWarrior != null)
                    {
                        cell.GetUnitWarrior.OnPreMerge(false);
                        cell.GetUnitWarrior.OnDrag(false);
                    }
                }
                AppManager.Instance.IsBlockedScreenScroll = false;
            }
        }

        public void RespawnAllUnitsAfterUpgrade(UnitType currentUnitType)
        {
            foreach (var cell in mergeFieldCells)
            {
                if (cell.GetUnitWarrior == null) continue;
                if(cell.GetUnitWarrior.UnitType == currentUnitType)
                {
                    if (cell.GetUnitWarrior.IsMergeZone)
                        cell.RespawnUnitAfterUpgrade();
                }
            }
        }
        private void MergeUnit()
        {
            if (unitWariorHit == null) return;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector2Int unitDragMapCurrentPos = new Vector2Int();

            RaycastHit[] hits = Physics.RaycastAll(ray, Mathf.Infinity, ~SortingLayer.GetLayerValueFromName("UnitWarrior"));
            foreach (var hit in hits)
            {
                var cell = hit.collider.GetComponent<MergeCell>();
                if (cell != null)
                {
                    unitDragMapCurrentPos = new Vector2Int((int)cell.transform.position.x, (int)cell.transform.position.z);
                }
            }

            bool isMerge = false;
            UnitWarrior unit = null;
            int cellIndex = 0;

            foreach (var cell in mergeFieldCells)
            {
                unit = cell.GetUnitWarrior;
                cellIndex = cell.GetIndex;
                if (unit != null)
                {
                    if (unit == unitWariorHit) continue;
                    if (unit.IsMergeZone)
                    {
                        if (unit.UnitType == unitWariorHit.UnitType && unit.UnitRarity == unitWariorHit.UnitRarity && unit.GetMergeRank == unitWariorHit.GetMergeRank)
                        {
                            Vector2Int unitMapPos = new Vector2Int((int)unit.transform.position.x, (int)unit.transform.position.z);
                            if (unitMapPos == unitDragMapCurrentPos)
                            {
                                unit.transform.position = cell.transform.position;
                                isMerge = true;
                                break;
                            }    
                        }
                    }
                }
            }

            if (isMerge)
            {
                var clearCell = mergeFieldCells.Find(p => p.GetUnitWarrior == unitWariorHit);
                if (clearCell != null)
                {
                    clearCell.DestroyUnit();
                    unit.NextRank(cellIndex);
                    mergeUnitCallback?.Invoke();
                    SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("Merge"));
                }
            }
            else
            {
                unitWariorHit.transform.position = unitWarriorStartDragPosition;
            }
        }

        public void AddMergeUnitListener(Action callback)
        {
            mergeUnitCallback += callback;
        }
        public void RemoveMergeUnitListener(Action callback)
        {
            mergeUnitCallback -= callback;
        }
        #endregion

        private void ClickMergeCell(MergeCell cell)
        {
            if (cell.GetIsLock && cell.GetIndex == SaveManager.Instance.GetMainParameters.spawnPointsAvailable + 1)
            {
                UIManager.Instance.GetPopup.ShowConfirmBuyPopup(
                    cell.GetPrice,
                    AppManager.Instance.GetBalance.CalculatePriceCoinsToDiamond(cell.GetPrice),
                    (priceCoins) =>
                    {
                        if (priceCoins <= SaveManager.Instance.GetMainParameters.coins)
                        {
                            SaveManager.Instance.RemoveCurrency(CurrencyType.Coins, priceCoins);
                            SaveManager.Instance.AddMergeCell();
                            cell.OpenCell();
                            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("BuySuccess"));
                        }
                        UIManager.Instance.GetPopup.HideConfirmBuyPopup();
                    },
                    (priceDiamond) =>
                    {
                        if (priceDiamond <= SaveManager.Instance.GetMainParameters.diamonds)
                        {
                            SaveManager.Instance.RemoveCurrency(CurrencyType.Diamond, priceDiamond);
                            SaveManager.Instance.AddMergeCell();
                            cell.OpenCell();
                            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("BuySuccess"));
                        }
                        UIManager.Instance.GetPopup.HideConfirmBuyPopup();
                    }
                    );
            }
        }


        public bool IsPointerOverGameObject()
        {
            // check Mouse 
            if (EventSystem.current.IsPointerOverGameObject())
                return true;

            // chek Touch 
            if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
                    return true;
            }

            return false;
        }

        private void OnDestroy()
        {
            if (unitBuyBtns != null)
            {
                foreach (var item in unitBuyBtns)
                {
                    item.Clear();
                }
            }
        }
    }
}

