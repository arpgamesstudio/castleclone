﻿using System;
using UnityEngine;
using Project.Saves;
using UnityEngine.EventSystems;
using TMPro;
using UnityEngine.UI;

namespace Project.Core
{
    public class MergeCell : MonoBehaviour
    {
        [SerializeField] private int index;
        [SerializeField] private GameObject lockObj;
        [SerializeField] private TextMeshProUGUI costText;
        [SerializeField] private GameObject costObj;
        [SerializeField] private GameObject pricePanel;
        [SerializeField] private GameObject occupiedUnitIsAttackObj;

        [Space(10), Header("Effects")]
        [SerializeField] private ParticleSystem openEffect;
        [SerializeField] private ParticleSystem spawnEffect;

        private bool isLock;
        private bool isOccupied;
        private bool isInit = false;

        private UnitWarrior unitWarrior;

        private Action<MergeCell> clickAction;

        public int GetIndex { get => index; }
        public Vector2Int GetMapPosition { get=> new Vector2Int((int)transform.position.x, (int)transform.position.z); }
        public bool GetIsLock { get => isLock; }
        public bool GetIsOccupied { get => isOccupied; }
        public int GetPrice { get; private set; }
        public UnitWarrior GetUnitWarrior { get => unitWarrior; }

        private void Awake()
        {
            openEffect.Stop();
            spawnEffect.Stop();
        }
        internal void Init()
        {
            isLock = index > SaveManager.Instance.GetMainParameters.spawnPointsAvailable;
            lockObj.SetActive(isLock);
            clickAction = null;
            GetPrice = AppManager.Instance.GetBalance.GetMergeCellPrice(index);

            costText.text = $"{GetPrice.ToLitter(1)}";
            LayoutRebuilder.ForceRebuildLayoutImmediate(pricePanel.transform as RectTransform);

            isOccupied = SaveManager.Instance.GetLevelData.GetCell(index).isOccupied;

            if (isOccupied)
            {
                var saveData = SaveManager.Instance.GetLevelData.GetCell(index);
                int rank = SaveManager.Instance.GetMainParameters.GetUnitRank(saveData.unitType) + saveData.mergeCount;
                var rarity = SaveManager.Instance.GetMainParameters.GetUnitRarity(saveData.unitType);
                SpawnUnit(
                    saveData.unitType,
                    rarity,
                    rank,
                    AppManager.Instance.GetBalance.GetUnitAttackValue(rank, rarity, saveData.unitType),
                    saveData.unitHp,
                    saveData.unitType == UnitType.Mage ? AppManager.Instance.GetBalance.GetMageHealingValue(rank, rarity) : 0,
                    false);
            }

            isInit = true;
        }
        private void Update()
        {
            if (!isInit) return;

            lockObj.SetActive(isLock);
            if (!isLock) costObj.SetActive(false);
            else
            {
                if (index == SaveManager.Instance.GetMainParameters.spawnPointsAvailable + 1) costObj.SetActive(true);
                else costObj.SetActive(false);
            }
            if (unitWarrior != null)
            {
                unitWarrior.UpdateAction();
                if (!unitWarrior.IsMergeZone) occupiedUnitIsAttackObj.SetActive(true);
                else occupiedUnitIsAttackObj.SetActive(false);
            }
            else
            {
                occupiedUnitIsAttackObj.SetActive(false);
            }
        }
        public void OpenCell()
        {
            isLock = false;
            openEffect.Play();
        }
        internal void Click()
        {
            clickAction?.Invoke(this);
        }

        public void AddListenerClick(Action<MergeCell> callback)
        {
            clickAction += callback;
        }

        internal void Clear()
        {
            if (unitWarrior != null)
            {
                Destroy(unitWarrior.gameObject);
                unitWarrior = null;
            }
        }

        internal void SpawnUnit(UnitType unitType, UnitRarity unitRarity, int rank, int attack, int hp, int healing, bool spawnEffectEnable = true)
        {
            if (hp <= 0)
            {
                DestroyUnit();
                return;
            }
            unitWarrior = Instantiate(AppManager.Instance.GetPrefabList.GetUnit(unitType, unitRarity).GetPrefab);
            unitWarrior.transform.position = transform.position;
            unitWarrior.SetParameters(rank, attack, hp, healing);
            unitWarrior.Init(index);
            isOccupied = true;
            if (spawnEffectEnable) spawnEffect.Play();
        }
        internal void RespawnUnitAfterUpgrade()
        {
            var saveData = SaveManager.Instance.GetLevelData.GetCell(index);
            int mergeRank = unitWarrior.GetMergeRank + 1;
            var unitType = saveData.unitType;
            var rarity = SaveManager.Instance.GetMainParameters.GetUnitRarity(unitType);
            int attack = AppManager.Instance.GetBalance.GetUnitAttackValue(mergeRank + 1, rarity, unitWarrior.UnitType);
            int hp = AppManager.Instance.GetBalance.GetUnitHpValue(mergeRank + 1, rarity, unitWarrior.UnitType);
            int healing = saveData.unitType == UnitType.Mage ? AppManager.Instance.GetBalance.GetMageHealingValue(mergeRank + 1, rarity) : 0;

            if (unitWarrior.UnitRarity != rarity)
            {
                Destroy(unitWarrior.gameObject);
                SpawnUnit(unitType, rarity, mergeRank - 6, attack, hp, healing);
            }
            else
            {
                unitWarrior.SetParameters(mergeRank, attack, hp, healing);
            }
        }
        internal void DestroyUnit()
        {
            isOccupied = false;
            SaveManager.Instance.LevelSetCell(index, isOccupied);
            if (unitWarrior != null) Destroy(unitWarrior.gameObject);
            unitWarrior = null;
        }
    }
}

