using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace Project.Core.UI
{
    public class UnitWarriorUI : MonoBehaviour
    {
        [SerializeField] private List<GameObject> mergeStartObjs;
        [SerializeField] private GameObject statesPanel;
        [SerializeField] private TextMeshProUGUI hpText;
        [SerializeField] private TextMeshProUGUI attackText;


        private void Awake()
        {
            statesPanel.SetActive(false);
        }


        public void SetMergeStars(int mergeCount)
        {
            for (int i = 0; i < mergeStartObjs.Count; i++)
            {
                if (mergeCount < 16)
                {
                    if (i < mergeCount && mergeCount > 0)
                    {
                        if (mergeCount > 10)
                        {
                            if (i < 10) mergeStartObjs[i].SetActive(false);
                        }
                        else mergeStartObjs[i].SetActive(true);
                    }
                    else mergeStartObjs[i].SetActive(false);
                }
                else
                {
                    if (i >= 15) mergeStartObjs[i].SetActive(true);
                    else mergeStartObjs[i].SetActive(false);
                }
            }
        }

        public void HideMergeStars()
        {
            foreach (var item in mergeStartObjs)
            {
                item.SetActive(false);
            }
        }

        public void StatesView(bool isShow, int hp = 0, int attack = 0)
        {
            statesPanel.SetActive(isShow);
            hpText.text = $"{hp}";
            attackText.text = $"{attack}";
        }
    }
}

