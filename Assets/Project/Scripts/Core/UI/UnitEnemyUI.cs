using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace Project.Core.UI
{
    public class UnitEnemyUI : MonoBehaviour
    {
        [SerializeField] private GameObject statesPanel;
        [SerializeField] private TextMeshProUGUI hpText;


        public void ShowStatesPanel(bool isShow)
        {
            statesPanel.SetActive(isShow);
        }
        public void SetHp(int hp)
        {
            hpText.text = $"{hp}";
        }
    }
}

