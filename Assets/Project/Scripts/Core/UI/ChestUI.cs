using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Project.Ads;


namespace Project.Core.UI
{
    public class ChestUI : MonoBehaviour
    {
        [SerializeField] private GameObject statesPanel;
        [SerializeField] private GameObject adsPanel;
        [SerializeField] private TextMeshProUGUI hpText;
        [SerializeField] private TextMeshProUGUI timerText;
        [SerializeField] private Button adButton;
        [SerializeField] private GameObject adOpenObj;
        [SerializeField] private GameObject adCloseObj;



        private void Update()
        {
            if (ApplovinAdsManager.Instance == null) return;
            if (ApplovinAdsManager.Instance.IsLoadedRewardAds())
            {
                adOpenObj.SetActive(true);
                adCloseObj.SetActive(false);
                adButton.interactable = true;
            }
            else
            {
                adOpenObj.SetActive(false);
                adCloseObj.SetActive(true);
                adButton.interactable = false;
            }
        }


        public void ShowStatesPanel(bool isShow)
        {
            statesPanel.SetActive(isShow);
        }
        public void ShowAdsPanel(bool isShow)
        {
            adsPanel.SetActive(isShow);
        }
        public void SetHp(int hp)
        {
            hpText.text = $"{hp}";
        }
        public void SetTimer(string time)
        {
            timerText.text = $"{time}";
        }

        internal void ViewPanels(bool isOpen)
        {
            if (isOpen)
            {
                ShowAdsPanel(true);
                ShowStatesPanel(false);
            }
            else
            {
                ShowAdsPanel(false);
                ShowStatesPanel(true);
            }
        }


        private Action<bool> rewardedViewAction;
        public void AddRewardedAdViewSuccessListener(Action<bool> action)
        {
            rewardedViewAction += action;
        }
        public void AdsButtonClick()
        {
            ApplovinAdsManager.Instance.ShowRewardedAd((success) =>
            {
                rewardedViewAction?.Invoke(success);
            });
        }
    }
}

