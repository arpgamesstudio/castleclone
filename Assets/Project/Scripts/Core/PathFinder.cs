﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Core.Path
{
    public class PathFinder
    {
        private const int MAX_LENGHT_WAVE = 500;
        private Map map;

        public PathFinder(Map map)
        {
            this.map = map;
        }

        public List<Vector2Int> GetPath(Vector2Int startPos, Vector2Int targetPos)
        {
            if (startPos == null || targetPos == null) return null;
            if (map.GetCell(startPos) == null || map.GetCell(targetPos) == null) return null;
            if (startPos == targetPos) return null;

            return CalculatePath(startPos, targetPos);
        }

        private List<Vector2Int> CalculatePath(Vector2Int startPos, Vector2Int targetPos)
        {
            List<Vector2Int> path = null;
            List<CellPassable> mapPassable = new List<CellPassable>();

            bool isFinished = false;
            int maxIndexWave = 0;

            #region CalculateMapPassable
            for (int i = 0; i < MAX_LENGHT_WAVE; i++)
            {
                if (i == 0)
                {
                    CellPassable cell = new CellPassable(startPos);
                    cell.isCheck = true;
                    cell.wave = i;
                    mapPassable.Add(cell);
                    continue;
                }

                List<CellPassable> lastCells = mapPassable.FindAll(p => p.wave == i - 1);
                foreach (var lastCell in lastCells)
                {
                    //up
                    Vector2Int upPos = new Vector2Int(lastCell.position.x, lastCell.position.y + 1);
                    var upCell = map.GetCell(upPos);
                    var upPassableMapCell = mapPassable.Find(p => p.position == upPos);
                    if (upCell != null && upPassableMapCell == null)
                    {
                        if (upCell.GetCellType == CellType.Passable)
                        {
                            CellPassable cell = new CellPassable(upPos);
                            cell.isCheck = true;
                            cell.wave = i;
                            mapPassable.Add(cell);
                        }
                    }
                    if (upPos == targetPos)
                    {
                        isFinished = true;
                        maxIndexWave = i;
                        break;
                    }
                    //down
                    Vector2Int downPos = new Vector2Int(lastCell.position.x, lastCell.position.y - 1);
                    var downCell = map.GetCell(downPos);
                    var downPassableMapCell = mapPassable.Find(p => p.position == downPos);
                    if (downCell != null && downPassableMapCell == null)
                    {
                        if (downCell.GetCellType == CellType.Passable)
                        {
                            CellPassable cell = new CellPassable(downPos);
                            cell.isCheck = true;
                            cell.wave = i;
                            mapPassable.Add(cell);
                        }
                    }
                    if (downPos == targetPos)
                    {
                        isFinished = true;
                        maxIndexWave = i;
                        break;
                    }
                    //left
                    Vector2Int leftPos = new Vector2Int(lastCell.position.x - 1, lastCell.position.y);
                    var leftCell = map.GetCell(leftPos);
                    var leftPassableMapCell = mapPassable.Find(p => p.position == leftPos);
                    if (leftCell != null && leftPassableMapCell ==null)
                    {
                        if (leftCell.GetCellType == CellType.Passable)
                        {
                            CellPassable cell = new CellPassable(leftPos);
                            cell.isCheck = true;
                            cell.wave = i;
                            mapPassable.Add(cell);
                        }
                    }
                    if (leftPos == targetPos)
                    {
                        isFinished = true;
                        maxIndexWave = i;
                        break;
                    }
                    //right
                    Vector2Int rightPos = new Vector2Int(lastCell.position.x + 1, lastCell.position.y);
                    var rightCell = map.GetCell(rightPos);
                    var righPassableMapCell = mapPassable.Find(p => p.position == rightPos);
                    if (rightCell != null && righPassableMapCell == null)
                    {
                        if (rightCell.GetCellType == CellType.Passable)
                        {
                            CellPassable cell = new CellPassable(rightPos);
                            cell.isCheck = true;
                            cell.wave = i;
                            mapPassable.Add(cell);
                        }
                    }
                    if (rightPos == targetPos)
                    {
                        isFinished = true;
                        maxIndexWave = i;
                        break;
                    }
                }
                if (isFinished) break;
            }
            #endregion

            #region WriteReverseWavePath
            if (mapPassable.Count > 0 && isFinished)
            {
                path = new List<Vector2Int>();
                var cell = mapPassable.Find(p => p.position == targetPos);
                if (cell != null) path.Add(cell.position);
                var lastCell = cell;

                for (int i = maxIndexWave - 1; i >= 0; i--)
                {
                    if (lastCell == null)
                    {
                        path = null;
                        break;
                    }
                    //up
                    Vector2Int upPos = new Vector2Int(lastCell.position.x, lastCell.position.y + 1);
                    var upCell = mapPassable.Find(p => p.position == upPos);
                    if (upCell != null)
                    {
                        if (upCell.wave == i) cell = upCell;
                    }
                    //down
                    Vector2Int downPos = new Vector2Int(lastCell.position.x, lastCell.position.y - 1);
                    var downCell = mapPassable.Find(p => p.position == downPos);
                    if (downCell != null)
                    {
                        if (downCell.wave == i) cell = downCell;
                    }
                    //Left
                    Vector2Int leftPos = new Vector2Int(lastCell.position.x - 1, lastCell.position.y);
                    var leftCell = mapPassable.Find(p => p.position == leftPos);
                    if (leftCell != null)
                    {
                        if (leftCell.wave == i) cell = leftCell;
                    }
                    //right
                    Vector2Int rightPos = new Vector2Int(lastCell.position.x + 1, lastCell.position.y);
                    var rightCell = mapPassable.Find(p => p.position == rightPos);
                    if (rightCell != null)
                    {
                        if (rightCell.wave == i) cell = rightCell;
                    }

                    lastCell = cell;
                    path.Add(cell.position);

                }
            }
            #endregion
            if (path != null) path.Reverse();
            return path;
        }

        private class CellPassable
        {
            public readonly Vector2Int position;
            public int wave = 0;
            public bool isCheck = false;
            public CellPassable(Vector2Int position) 
            {
                this.position = position;
            }
        }
    }
}

