﻿using System;
using System.Collections.Generic;
using TG.Audio;
using UnityEngine;

namespace Project.Core
{
    public class Gate : MonoBehaviour
    {
        [SerializeField] private List<Transform> cells;
        [SerializeField] private List<EnemiesUnit> enemies;
        [SerializeField] private Animator animator;
        [SerializeField] private ParticleSystem openEffect;
        public List<Transform> GetCells { get => cells; }

        private bool isOpen = false;

        private void Awake()
        {
            openEffect.Stop();
        }
        private void Start()
        {
            openEffect.Stop();
        }
        public void OpenGate()
        {
            if (isOpen) return;
            isOpen = true;
            animator.SetBool("IsClose", false);
            Vector2Int pos = new Vector2Int((int)transform.position.x, (int)transform.position.z);
            AppManager.Instance.CurrentLevelManager.GetMap.OpenCell(pos);
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("LevelComplete"));
            openEffect.Play();
        }

        private void Update()
        {
            if (enemies == null || !AppManager.Instance.CurrentLevelManager.IsInit) return;

            bool isAllEnemiesDead = true;
            foreach (var enemy in enemies)
            {
                if (!enemy.IsDead)
                {
                    isAllEnemiesDead = false;
                    break;
                }
            }

            if (isAllEnemiesDead)
            {
                OpenGate();
            }
        }
    }
}

