using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.Controllers;
using System;
using Project.Core.UI;
using Project.Core.Path;
using DamageNumbersPro;
using Project.Core.Interfaces;
using Project.Saves;
using TG.Audio;

namespace Project.Core
{
    public class EnemiesUnit : MonoBehaviour, IDamageble
    {
        [SerializeField] private EnemyAnimationController animationController;
        [SerializeField] private UnitEnemyUI unitEnemyUI;
        [SerializeField] private Collider hitCollider;
        [SerializeField] private DamageNumber damageNumbersPrefab;
        [SerializeField] private CoinsContainer coinsContainer;

        [SerializeField] private int rewardCoins;
        [SerializeField] private int rewardDiamonds;
        [SerializeField] private int baseHpValue;
        [SerializeField] private int baseAttackValue;
        [SerializeField] private float attackDelay;

        private int currentHp;
        private int currentAttack;
        private IDamageble targetUnit;
        private bool isDead = false;
        private Map map;
        private List<IDamageble> targetUnits;
        private bool isInit;
        private float attackDelayTimer;
        private bool isAttack;
        private bool isDamage; 
        private Quaternion startUIRotation;
        private float hideUITimer = 5f;

        public int GetIndex { get; private set; }
        public Vector2Int GetMapPosition { get => new Vector2Int((int)transform.position.x, (int)transform.position.z); }
        public int GetMaxHp { get => baseHpValue; }
        public int CurrentHp { get => currentHp; }
        public int CurrentAttack { get => currentAttack; }
        public bool IsDead { get => isDead; }
        public bool IsInit { get => isInit; }

        public Transform GetTransform => transform;

        public void SetBaseParameters(Map map)
        {
            this.map = map;
            Vector2Int currentPoint = new Vector2Int((int)transform.position.x, (int)transform.position.z);
            map.CloseCell(currentPoint);
            currentHp = baseHpValue;
            currentAttack = baseAttackValue;
            unitEnemyUI.SetHp(currentHp);
        }

        public void SetParameters(int hp, bool isDead)
        {
            this.isDead = isDead;
            currentHp = hp;
            unitEnemyUI.SetHp(currentHp);
            if (IsDead)
            {
                Die();
                transform.position = new Vector3(transform.position.x, transform.position.y - 2f, transform.position.z);
            }
        }

        public void Damage(int value)
        {
            isDamage = true;
            hideUITimer = 0;
            unitEnemyUI.ShowStatesPanel(true);
            animationController.Damage();
            currentHp -= value;
            SaveManager.Instance.RemoveEnemyHp(GetIndex, value);
            if (currentHp <= 0)
            {
                Die();
                coinsContainer.StartAction(rewardCoins, rewardDiamonds);
            }
            unitEnemyUI.SetHp(currentHp);
            damageNumbersPrefab.CreateNew(value, transform.position);
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("EnemyDamage"));
        }

        private void Die()
        {
            Vector2Int currentPoint = new Vector2Int((int)transform.position.x, (int)transform.position.z);
            map.OpenCell(currentPoint);
            isDead = true;
            animationController.Die();
            unitEnemyUI.ShowStatesPanel(false);
            hitCollider.enabled = false;
            StartCoroutine(DestroyObject());
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("EnemyDie"));
        }
        private IEnumerator DestroyObject()
        {
            yield return new WaitForSeconds(2f);
            float targetPosY = -2f;
            while (true)
            {
                yield return null;
                transform.position -= new Vector3(0, 0.01f, 0);
                if (transform.position.y < targetPosY) break;
            }
        }
        internal void Init(int index)
        {
            GetIndex = index;
            animationController.AddListenerEndAttack(OnEndAttack);
            animationController.AddListenerEndDamage(OnEndDamage);
            animationController.AddListenerOnAttack(OnAttack);
            targetUnits = new List<IDamageble>();
            startUIRotation = unitEnemyUI.transform.rotation;
            isInit = true;
            var data = SaveManager.Instance.GetLevelData.GetEnemyData(index);
            if (data != null) SetParameters(data.hp, data.isDead);
        }

        internal void Click()
        {
            if (IsDead) return;

            unitEnemyUI.ShowStatesPanel(true);
            hideUITimer = 0;
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("SelectEnemyUnit"));
        }

        internal void Deselect()
        {

        }

        internal void AddTargetUnit(IDamageble unitWarrior)
        {
            if (targetUnits.Find(p => p == unitWarrior) != null) return;
            targetUnits.Add(unitWarrior);
        }

        internal void UpdateAction()
        {
            unitEnemyUI.transform.rotation = startUIRotation;
            if (targetUnit == null)
            {
                foreach (var unit in targetUnits)
                {
                    if (unit != null)
                    {
                        if (!unit.IsDead)
                        {
                            targetUnit = unit;
                            break;
                        }

                    }
                }
            }
            else
            {
                Attack();
            }

            if (hideUITimer < 4f) hideUITimer += Time.deltaTime;
            else unitEnemyUI.ShowStatesPanel(false);
        }

        private void Attack()
        {
            transform.forward = -(targetUnit.GetTransform.position - transform.position).normalized;
            if (attackDelayTimer > 0) attackDelayTimer -= Time.deltaTime;
            else
            {
                //Attack
                attackDelayTimer = attackDelay;

                isAttack = true;
                animationController.Attack();
            }
        }

        private void OnAttack()
        {
            if (targetUnit == null) return;
            if (!targetUnit.IsDead) targetUnit.Damage(currentAttack);
            if (targetUnit.IsDead)
            {
                targetUnits.Remove(targetUnit);
                targetUnit = null;
            }
        }


        private void OnEndDamage()
        {
            isDamage = false;
        }

        private void OnEndAttack()
        {
            isAttack = false;
        }
    }
}

