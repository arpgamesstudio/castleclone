using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Core.Interfaces
{
    public interface IDamageble
    {
        Transform GetTransform { get;}
        bool IsDead { get; }

        void Damage(int value);
    }
}

