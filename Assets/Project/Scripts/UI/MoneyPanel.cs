using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Project.Saves;
using System;

public class MoneyPanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI coinsText;
    [SerializeField] private GameObject coinsObj;
    [SerializeField] private ParticleSystem addCoinsEffect;
    [SerializeField] private TextMeshProUGUI diamondsText;


    public Transform GetCoinsTextTranform { get => coinsText.transform; }

    private void Awake()
    {
        addCoinsEffect.Stop();
    }

    void Update()
    {
        if (SaveManager.Instance == null) return;
        if (SaveManager.Instance.GetMainParameters == null) return;

        coinsText.text = $"{SaveManager.Instance.GetMainParameters.coins.ToLitter()}";
        diamondsText.text = $"{SaveManager.Instance.GetMainParameters.diamonds.ToLitter()}";
    }

    internal void AddCoinsEffect()
    {
        StartCoroutine(SizeEffect(coinsObj));
        addCoinsEffect.Play();
    }

    private IEnumerator SizeEffect(GameObject obj)
    {
        obj.transform.localScale = Vector3.one;
        float scale = 1;
        for (float i = 0; i < 1f; i += 0.03f)
        {
            yield return null;
            scale = Mathf.Lerp(scale, 1.5f, i);
            obj.transform.localScale = new Vector3(scale, scale, scale);
        }
        for (float i = 0; i < 1f; i += 0.03f)
        {
            yield return null;
            scale = Mathf.Lerp(scale, 1f, i);
            obj.transform.localScale = new Vector3(scale, scale, scale);
        }
        obj.transform.localScale = Vector3.one;
    }
}
