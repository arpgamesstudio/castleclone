using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Project.Saves;
using TG.Audio;

namespace Project.UI
{
    public class ShopElement : MonoBehaviour
    {
        [SerializeField] private Image back;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI descriptionText;
        [SerializeField] private TextMeshProUGUI priceText;
        [SerializeField] private TextMeshProUGUI timerText;
        [SerializeField] private Image btnIcon;
        [SerializeField] private GameObject hitObj;
        [SerializeField]private GameObject freeObj;
        [SerializeField] private Button buyBtn;

        [Space(20)]
        [SerializeField] private Sprite normalBack;
        [SerializeField] private Sprite rareBack;
        [SerializeField] private Sprite legendaryBack;

        Settings.ShopSettings.ShopContent content;

        bool isInit = false;


        internal void Init(Settings.ShopSettings.ShopContent content)
        {
            this.content = content;
            isInit = true;

            icon.sprite = content.GetIcon;
            descriptionText.text = string.Format(content.GetDescription, content.GetValue.ToLitter());
            if (content.GetIsHitProduct) hitObj.SetActive(true);
            else hitObj.SetActive(false);
            if (content.GetIsFree)
            {
                freeObj.SetActive(true);
                priceText.gameObject.SetActive(false);
            }
            else
            {
                freeObj.SetActive(false);
                priceText.gameObject.SetActive(true);
            }
            if (content.GetIsAdRewardShow) btnIcon.gameObject.SetActive(true);
            else btnIcon.gameObject.SetActive(false);

            switch (content.GetProductRarity)
            {
                case Settings.ShopSettings.ProductRarity.Normal:
                    back.sprite = normalBack;
                    break;
                case Settings.ShopSettings.ProductRarity.Rare:
                    back.sprite = rareBack;
                    break;
                case Settings.ShopSettings.ProductRarity.Legendary:
                    back.sprite = legendaryBack;
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(content.ProductId))
            {
                var storeProduct = AppManager.Instance.GetProduct(content.ProductId);
                if (storeProduct != null)
                {
                    priceText.text = $"{storeProduct.localizedPriceString}";
                }
            }
        }

        private void Update()
        {
            if (!isInit) return;
            bool isTimeActivated = false;
            if (content.TimerSec > 0)
            {
                if (SaveManager.Instance.GetShopTimer != DateTime.MinValue)
                {
                    var timeRemainder = SaveManager.Instance.GetCurrentTime - SaveManager.Instance.GetShopTimer;
                    var time = content.TimerSec - timeRemainder.TotalSeconds;
                    TimeSpan span = TimeSpan.FromSeconds(time);
                    timerText.text = $"{span}";
                    if (span.TotalSeconds < 0)
                    {
                        timerText.gameObject.SetActive(false);
                    }
                    else
                    {
                        timerText.gameObject.SetActive(true);
                        isTimeActivated = true;
                    }
                }
            }
            if (content.GetIsAdRewardShow )
            {
                if (Ads.ApplovinAdsManager.Instance.IsLoadedRewardAds())
                {
                    if (!isTimeActivated) buyBtn.interactable = true;
                }
                else
                {
                    buyBtn.interactable = false;
                }
            }
            if (isTimeActivated) buyBtn.interactable = false;
        }
        public void BuyClick()
        {
            if (!string.IsNullOrEmpty(content.ProductId))
            {
                var storeProduct = AppManager.Instance.GetProduct(content.ProductId);
                if (storeProduct != null)
                {
                    IAPManager.Instance.BuyProduct(content.ProductName, OnGetItem);
                }
            }
            if (content.GetIsAdRewardShow)
            {
                Ads.ApplovinAdsManager.Instance.ShowRewardedAd((success) =>
                {
                    if (success)
                    {
                        SaveManager.Instance.AddCurrency(content.GetResurceType, content.GetValue);
                        if (content.TimerSec > 0) SaveManager.Instance.ResetShopTimer();
                        SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("BuySuccess"));
                    }
                });
            }
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }

        private void OnGetItem(IAPOperationStatus status, string message, StoreProduct product)
        {
            if (status == IAPOperationStatus.Success)
            {
                SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("BuySuccess"));
                SaveManager.Instance.AddCurrency(content.GetResurceType, content.GetValue);
            } 
        }
    }
}

