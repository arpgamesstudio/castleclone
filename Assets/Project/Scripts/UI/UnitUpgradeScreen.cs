using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Project.Saves;
using System;
using TG.Audio;

namespace Project.UI
{
    public class UnitUpgradeScreen : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI unitNameText;
        [SerializeField] private TextMeshProUGUI upgradePriceText;
        [SerializeField] private Button upgradeBtn;
        [SerializeField] private GameObject healingPanelObj;

        [Space(10)]
        [SerializeField] private Button melleeSelectBtn;
        [SerializeField] private Button archerSelectBtn;
        [SerializeField] private Button mageSelectBtn;
        [SerializeField] private Button catapultSelectBtn;
        [SerializeField] private GameObject melleeLockBtnObj;
        [SerializeField] private GameObject archerLockBtnObj;
        [SerializeField] private GameObject mageLockBtnObj;
        [SerializeField] private GameObject catapultLockBtnObj;
        [SerializeField] private GameObject melleeSelectBtnObj;
        [SerializeField] private GameObject archerSelectBtnObj;
        [SerializeField] private GameObject mageSelectBtnObj;
        [SerializeField] private GameObject catapultSelectBtnObj;
        [SerializeField] private Image melleeAvatar;
        [SerializeField] private Image archerAvatar;
        [SerializeField] private Image mageAvatar;
        [SerializeField] private Image catapultAvatar;
        [SerializeField] private Image melleeAvatarBack;
        [SerializeField] private Image archerAvatarBack;
        [SerializeField] private Image mageAvatarBack;
        [SerializeField] private Image catapultAvatarBack;

        [Space(10)]
        [SerializeField] private List<GameObject> rankStarObjs;


        [Space(10)]
        [SerializeField] private TextMeshProUGUI hpCurrentText;
        [SerializeField] private TextMeshProUGUI hpNextText;
        [SerializeField] private TextMeshProUGUI damageCurrentText;
        [SerializeField] private TextMeshProUGUI damageNextText;
        [SerializeField] private TextMeshProUGUI healingCurrentText;
        [SerializeField] private TextMeshProUGUI healingNextText;

        [Space(10)]
        [SerializeField] private Transform previewSpawnPoint;

        [Space(10)]
        [SerializeField] private ParticleSystem upgradeEffect;



        private PreviewUnit previewUnit;
        private UnitType currentUnitType;


        private void Start()
        {
            currentUnitType = UnitType.Mellee;
            upgradeEffect.Stop();
        }
        private void UpdateView()
        {
            RankView();
            StatesView();
            SelectBtnsView();
            SelectUnitBtnViewObj();
            SetBtnsAvatarAndBack();
            UpdatePreview();

            unitNameText.text = $"{AppManager.Instance.GetPrefabList.GetUnit(currentUnitType, SaveManager.Instance.GetMainParameters.GetUnitRarity(currentUnitType))?.GetNameLoc}";
            var price = AppManager.Instance.GetBalance.GetUnitUpgradePrice(
                SaveManager.Instance.GetMainParameters.GetUnitRank(currentUnitType),
                SaveManager.Instance.GetMainParameters.GetUnitRarity(currentUnitType),
                currentUnitType);
            upgradePriceText.text = $"{price.ToLitter(1)}";
            bool isLockBtn = false;
            if (SaveManager.Instance.GetMainParameters.GetUnitRarity(currentUnitType) == UnitRarity.Immortal)
            {
                if (SaveManager.Instance.GetMainParameters.GetUnitRank(currentUnitType) >= 5)
                    isLockBtn = true;
            }
            if (price <= SaveManager.Instance.GetMainParameters.coins && !isLockBtn)
                upgradeBtn.interactable = true;
            else upgradeBtn.interactable = false;
        }

        private void UpdatePreview()
        {
            if (previewUnit == null)
            {
                CreatePreviewUnit();
            }
            else
            {
                if (previewUnit.unitType != currentUnitType || previewUnit.unitRarity != SaveManager.Instance.GetMainParameters.GetUnitRarity(currentUnitType))
                {
                    ClearPreviewUnit();
                    CreatePreviewUnit();
                }
            }

            void CreatePreviewUnit()
            {
                previewUnit = new PreviewUnit();
                previewUnit.unit = Instantiate(
                    AppManager.Instance.GetPrefabList.GetUnit(currentUnitType, SaveManager.Instance.GetMainParameters.GetUnitRarity(currentUnitType)).GetPreviewPrefab,
                    previewSpawnPoint);
                previewUnit.unit.transform.localPosition = Vector3.zero;
                previewUnit.unitType = currentUnitType;
                previewUnit.unitRarity = SaveManager.Instance.GetMainParameters.GetUnitRarity(currentUnitType);
            }
            void ClearPreviewUnit()
            {
                Destroy(previewUnit.unit);
                previewUnit = null;
            }
        }

        private void SetBtnsAvatarAndBack()
        {
            melleeAvatar.sprite = AppManager.Instance.GetSpriteList.GetAvatarSpriteMini(UnitType.Mellee, SaveManager.Instance.GetMainParameters.GetUnitRarity(UnitType.Mellee));
            archerAvatar.sprite = AppManager.Instance.GetSpriteList.GetAvatarSpriteMini(UnitType.Archer, SaveManager.Instance.GetMainParameters.GetUnitRarity(UnitType.Archer));
            mageAvatar.sprite = AppManager.Instance.GetSpriteList.GetAvatarSpriteMini(UnitType.Mage, SaveManager.Instance.GetMainParameters.GetUnitRarity(UnitType.Mage));
            catapultAvatar.sprite = AppManager.Instance.GetSpriteList.GetAvatarSpriteMini(UnitType.Catapult, SaveManager.Instance.GetMainParameters.GetUnitRarity(UnitType.Catapult));

            melleeAvatarBack.sprite = AppManager.Instance.GetSpriteList.GetAvatarBackSprite(SaveManager.Instance.GetMainParameters.GetUnitRarity(UnitType.Mellee));
            archerAvatarBack.sprite = AppManager.Instance.GetSpriteList.GetAvatarBackSprite(SaveManager.Instance.GetMainParameters.GetUnitRarity(UnitType.Archer));
            mageAvatarBack.sprite = AppManager.Instance.GetSpriteList.GetAvatarBackSprite(SaveManager.Instance.GetMainParameters.GetUnitRarity(UnitType.Mage));
            catapultAvatarBack.sprite = AppManager.Instance.GetSpriteList.GetAvatarBackSprite(SaveManager.Instance.GetMainParameters.GetUnitRarity(UnitType.Catapult));
        }

        private void SelectBtnsView()
        {
            if (SaveManager.Instance.GetMainParameters.melleeUnitIsOpen)
            {
                melleeSelectBtn.interactable = true;
                melleeLockBtnObj.SetActive(false);
            }
            else
            {
                melleeSelectBtn.interactable = false;
                melleeLockBtnObj.SetActive(true);
            }

            if (SaveManager.Instance.GetMainParameters.archerUnitIsOpen)
            {
                archerSelectBtn.interactable = true;
                archerLockBtnObj.SetActive(false);
            }
            else
            {
                archerSelectBtn.interactable = false;
                archerLockBtnObj.SetActive(true);
            }

            if (SaveManager.Instance.GetMainParameters.mageUnitIsOpen)
            {
                mageSelectBtn.interactable = true;
                mageLockBtnObj.SetActive(false);
            }
            else
            {
                mageSelectBtn.interactable = false;
                mageLockBtnObj.SetActive(true);
            }

            if (SaveManager.Instance.GetMainParameters.archerUnitIsOpen)
            {
                catapultSelectBtn.interactable = true;
                catapultLockBtnObj.SetActive(false);
            }
            else
            {
                catapultSelectBtn.interactable = false;
                catapultLockBtnObj.SetActive(true);
            }
        }
        private void SelectUnitBtnViewObj()
        {
            if (currentUnitType == UnitType.Mellee)
                melleeSelectBtnObj.SetActive(true);
            else melleeSelectBtnObj.SetActive(false);

            if (currentUnitType == UnitType.Archer)
                archerSelectBtnObj.SetActive(true);
            else archerSelectBtnObj.SetActive(false);

            if (currentUnitType == UnitType.Mage)
                mageSelectBtnObj.SetActive(true);
            else mageSelectBtnObj.SetActive(false);

            if (currentUnitType == UnitType.Catapult)
                catapultSelectBtnObj.SetActive(true);
            else catapultSelectBtnObj.SetActive(false);
        }
        private void StatesView()
        {
            int currentRank = SaveManager.Instance.GetMainParameters.GetUnitRank(currentUnitType);
            UnitRarity currentRarity = SaveManager.Instance.GetMainParameters.GetUnitRarity(currentUnitType);
            UnitRarity nextRarity = currentRarity;
            int nextRank = currentRank + 1;
            bool isMax = false;
            if (currentUnitType == UnitType.Mage) healingPanelObj.SetActive(true);
            else healingPanelObj.SetActive(false);

            if (nextRank > 5)
            {
                nextRank = 0;
                switch (currentRarity)
                {
                    case UnitRarity.Normal:
                        nextRarity = UnitRarity.Improved;
                        break;
                    case UnitRarity.Improved:
                        nextRarity = UnitRarity.Rare;
                        break;
                    case UnitRarity.Rare:
                        nextRarity = UnitRarity.Unique;
                        break;
                    case UnitRarity.Unique:
                        nextRarity = UnitRarity.Legendary;
                        break;
                    case UnitRarity.Legendary:
                        nextRarity = UnitRarity.Mythical;
                        break;
                    case UnitRarity.Mythical:
                        nextRarity = UnitRarity.Immortal;
                        break;
                    case UnitRarity.Immortal:
                        nextRarity = UnitRarity.Immortal;
                        nextRank = 5;
                        isMax = true;
                        break;
                    default:
                        break;
                }
            }

            hpCurrentText.text = $"{AppManager.Instance.GetBalance.GetUnitHpValue(currentRank, currentRarity, currentUnitType).ToLitter(1)}";
            damageCurrentText.text = $"{AppManager.Instance.GetBalance.GetUnitAttackValue(currentRank, currentRarity, currentUnitType).ToLitter(1)}";
            healingCurrentText.text = $"{AppManager.Instance.GetBalance.GetMageHealingValue(currentRank, currentRarity).ToLitter(1)}";
            if (!isMax)
            {
                hpNextText.text = $"{AppManager.Instance.GetBalance.GetUnitHpValue(nextRank, nextRarity, currentUnitType).ToLitter(1)}";
                damageNextText.text = $"{AppManager.Instance.GetBalance.GetUnitAttackValue(nextRank, nextRarity, currentUnitType).ToLitter(1)}";
                healingNextText.text = $"{AppManager.Instance.GetBalance.GetMageHealingValue(nextRank, nextRarity).ToLitter(1)}";
                upgradeBtn.interactable = true;
            }
            else
            {
                hpNextText.text = "max";
                damageNextText.text = "max";
                healingNextText.text = "max";
                upgradeBtn.interactable = false;
            }
        }

        private void RankView()
        {
            for (int i = 0; i < 5; i++)
            {
                if (SaveManager.Instance.GetMainParameters.GetUnitRank(currentUnitType) > i) rankStarObjs[i].SetActive(true);
                else rankStarObjs[i].SetActive(false);
            }
        }
        


       

        public void Init()
        {
            UpdateView();
        }

        public void UpgradeBtnClick()
        {
            var price = AppManager.Instance.GetBalance.GetUnitUpgradePrice(
                   SaveManager.Instance.GetMainParameters.GetUnitRank(currentUnitType),
                   SaveManager.Instance.GetMainParameters.GetUnitRarity(currentUnitType),
                   currentUnitType);
            if (price <= SaveManager.Instance.GetMainParameters.coins)
            {
                SaveManager.Instance.RemoveCurrency(CurrencyType.Coins, price);
                SaveManager.Instance.UpgradeUnit(currentUnitType);
                UpdateView();
                foreach (var item in AppManager.Instance.CurrentLevelManager.GetUnitBuyBtns)
                {
                    item.Init();
                }
                AppManager.Instance.CurrentLevelManager.RespawnAllUnitsAfterUpgrade(currentUnitType);
                upgradeEffect.Play();
            }
            else Debug.Log("Not enought coins!");
        }
        public void SelectMelleeUnitClick()
        {
            currentUnitType = UnitType.Mellee;
            UpdateView();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void SelectArcherUnitClick()
        {
            currentUnitType = UnitType.Archer;
            UpdateView();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void SelectMageUnitClick()
        {
            currentUnitType = UnitType.Mage;
            UpdateView();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void SelectCatapultUnitClick()
        {
            currentUnitType = UnitType.Catapult;
            UpdateView();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void CloseClick()
        {
            UIManager.Instance.HideUnitUpgradeScreen();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }


        private class PreviewUnit
        {
            public GameObject unit;
            public UnitRarity unitRarity;
            public UnitType unitType;
        }
    }
}

