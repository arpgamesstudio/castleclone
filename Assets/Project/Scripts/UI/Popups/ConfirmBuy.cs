using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Project.Saves;
using TG.Audio;
using Project.Ads;

namespace Project.UI.Popup
{
    public class ConfirmBuy : MonoBehaviour
    {
        [SerializeField] private Button coinsBuyBtn;
        [SerializeField] private Button diamondBuyBtn;
        [SerializeField] private Button coinsAdRewardBtn;
        [SerializeField] private Button diamondAdRewardBtn;
        [SerializeField] private TextMeshProUGUI coinsCountText;
        [SerializeField] private TextMeshProUGUI diamondCountText;
        [SerializeField] private Image coinsAdRewardBtnImage;
        [SerializeField] private Image diamondAdRewardBtnImage;

        [Space(10)]
        [SerializeField] private Sprite adRewardBtnOpenSprite;
        [SerializeField] private Sprite adRewardBtnCloseSprite;


        Action<int> coinsClick;
        Action<int> diamondClick;
        Action closeClick;

        bool isAddRewardLoaded = true;
        bool isAdRewardCoinsReceived;
        bool isAdRewardDiamondReceived;

        int coinsCountBaseValue;
        int diamondCountBaseValue;

        int coinsCountCurrentValue;
        int diamondCountCurrentValue;

        internal void Init(int coinsCount, int diamondCount, Action<int> coinsClick, Action<int> diamondClick, Action closeClick)
        {
            this.coinsClick = null;
            this.coinsClick += coinsClick;
            this.diamondClick = null;
            this.diamondClick += diamondClick;
            this.closeClick = null;
            this.closeClick += closeClick;

            isAdRewardCoinsReceived = false;
            isAdRewardDiamondReceived = false;
            this.coinsCountBaseValue = coinsCount;
            this.diamondCountBaseValue = diamondCount;
            coinsCountCurrentValue = coinsCount;
            diamondCountCurrentValue = diamondCount;

            coinsCountText.text = $"{coinsCount.ToLitter()}";
            diamondCountText.text = $"{diamondCount.ToLitter()}";

            if (coinsCount < 0)
            {
                coinsBuyBtn.gameObject.SetActive(false);
                coinsAdRewardBtn.gameObject.SetActive(false);
            }
            else
            {
                coinsBuyBtn.gameObject.SetActive(true);
                coinsAdRewardBtn.gameObject.SetActive(true);
            }
            if (diamondCount < 0)
            {
                diamondBuyBtn.gameObject.SetActive(false);
                diamondAdRewardBtn.gameObject.SetActive(false);
            }
            else
            {
                diamondBuyBtn.gameObject.SetActive(true);
                diamondAdRewardBtn.gameObject.SetActive(true);
            }
        }

        private void Update()
        {
            if (isAddRewardLoaded)
            {
                coinsAdRewardBtnImage.sprite = adRewardBtnOpenSprite;
                diamondAdRewardBtnImage.sprite = adRewardBtnOpenSprite;
            }
            else
            {
                coinsAdRewardBtnImage.sprite = adRewardBtnCloseSprite;
                diamondAdRewardBtnImage.sprite = adRewardBtnCloseSprite;
            }

            if (isAdRewardCoinsReceived)
            {
                coinsAdRewardBtn.interactable = false;
                coinsCountCurrentValue = coinsCountBaseValue / 2;
                if (coinsCountCurrentValue <= 0) coinsCountCurrentValue = 1;
                coinsCountText.text = $"{coinsCountCurrentValue.ToLitter()}";
            }
            else
            {
                coinsAdRewardBtn.interactable = true;
                coinsCountCurrentValue = coinsCountBaseValue;
            }
            if (isAdRewardDiamondReceived)
            {
                diamondAdRewardBtn.interactable = false;
                diamondCountCurrentValue = (int)(diamondCountBaseValue * 0.3f) - 1;
                if (diamondCountCurrentValue <= 0) diamondCountCurrentValue = 1;
                diamondCountText.text = $"{diamondCountCurrentValue.ToLitter()}";
            }
            else
            {
                diamondAdRewardBtn.interactable = true;
                diamondCountCurrentValue = diamondCountBaseValue;
            }


            if (coinsCountCurrentValue > SaveManager.Instance.GetMainParameters.coins) coinsBuyBtn.interactable = false;
            else coinsBuyBtn.interactable = true;

            if (diamondCountCurrentValue > SaveManager.Instance.GetMainParameters.diamonds) diamondBuyBtn.interactable = false;
            else diamondBuyBtn.interactable = true;
        }


        public void CoinsBuyBtnClick()
        {
            coinsClick?.Invoke(coinsCountCurrentValue);
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void DiamondBuyBtnClick()
        {
            diamondClick?.Invoke(diamondCountCurrentValue);
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void CoinsAdRewardBtnClick()
        {
            if (!isAddRewardLoaded) return;
            ApplovinAdsManager.Instance.ShowRewardedAd((success) =>
            {
                if(success) isAdRewardCoinsReceived = true;
            });
            
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void DiamondAdRewardBtnClick()
        {
            if (!isAddRewardLoaded) return;
            ApplovinAdsManager.Instance.ShowRewardedAd((success) =>
            {
                if (success) isAdRewardDiamondReceived = true;
            });
            
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void CloseClick()
        {
            closeClick?.Invoke();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
    }
}

