using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.UI.Popup
{
    public class Popups : MonoBehaviour
    {
        [SerializeField] private ConfirmBuy confirmBuy;

        public ConfirmBuy GetConfirmBuy { get => confirmBuy; }
        private void Awake()
        {
            HideAllPopup();
        }

        private void HideAllPopup()
        {
            HideConfirmBuyPopup();
        }

        public void ShowConfirmBuyPopup(int coinsCount = -1, int diamondCount = -1, Action<int> coinsClick = null, Action<int> diamondClick = null)
        {
            confirmBuy.gameObject.SetActive(true);
            confirmBuy.Init(coinsCount, diamondCount, coinsClick, diamondClick, ()=> { HideConfirmBuyPopup(); });
        }
        public void HideConfirmBuyPopup()
        {
            confirmBuy.gameObject.SetActive(false);
        }
    }
}

