using System;
using System.Collections;
using System.Collections.Generic;
using TG.Audio;
using UnityEngine;
using UnityEngine.UI;


namespace Project.UI
{
    public class ShopScreen : MonoBehaviour
    {
        [SerializeField] private ShopElement shopElementPrefab;
        [SerializeField] private ScrollRect scrollRect;



        private List<ShopElement> shopElements;





        internal void Init()
        {
            ClearElements();
            CreateShopElements();
        }

        private void CreateShopElements()
        {
            if (shopElements == null) shopElements = new List<ShopElement>();

            var settings = AppManager.Instance.GetShopSettings;

            foreach (var item in settings.GetAllShopContent)
            {
                var element = Instantiate(shopElementPrefab, scrollRect.content);
                element.Init(item);
                shopElements.Add(element);
            }
        }

        private void ClearElements()
        {
            if (shopElements == null) return;
            foreach (var item in shopElements)
            {
                Destroy(item.gameObject);
            }
            shopElements = null;
        }

        public void CloseClick()
        {
            UIManager.Instance.HideShopScreen();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
    }
}

