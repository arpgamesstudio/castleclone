using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Project.Saves;
using System;
using Project.Ads;

namespace Project.UI
{
    public class UnitBuyBtn : MonoBehaviour
    {
        [SerializeField] private Button button;
        [SerializeField] private Button adRewardButton;
        [SerializeField] private UnitType unitType;
        [SerializeField] private List<GameObject> rankObjs;
        [SerializeField] private TextMeshProUGUI priceText;
        [SerializeField] private GameObject lockObj;
        [SerializeField] private Image lockTimerIcon;
        [SerializeField] private GameObject pricePanel;
        [SerializeField] private GameObject rankPanel;
        [SerializeField] private GameObject statePanel;
        [SerializeField] private GameObject healingObj;
        [SerializeField] private Image backImg;
        [SerializeField] private Image avatarImg;
        [SerializeField] private TextMeshProUGUI attackStateText;
        [SerializeField] private TextMeshProUGUI hpStateText;
        [SerializeField] private TextMeshProUGUI healingStateText;


        private const float LOCK_MAX_TIME = 1.5f;
        private Action<UnitType, UnitRarity, int, int, int, int, int> callbackClick;

        private bool isInit = false;

        private int rank;
        private bool isOpen;
        private UnitRarity rarity;

        private int price;
        private int attack;
        private int hp;
        private int healing;

        private float lockTimer = 0f;
        

        public void Init()
        {
            SetVariables();

            SetSprites();
            ViewParameters();
            RankView();
            

            if (unitType != UnitType.Mage) healingObj.SetActive(false);
            else healingStateText.text = $"{healing}";

            isInit = true;
        }
        private void Update()
        {
            if (!isInit) return;
            SetAdButtonView();
            if (lockTimer > 0)
            {
                lockTimer -= Time.deltaTime;
                lockTimerIcon.fillAmount = lockTimer / LOCK_MAX_TIME;
            }
            else lockTimerIcon.fillAmount = 0;
        }

        private void RankView()
        {
            for (int i = 0; i < 5; i++)
            {
                if (rank > i) rankObjs[i].SetActive(true);
                else rankObjs[i].SetActive(false);
            }
        }

        private void ViewParameters()
        {
            if (isOpen)
            {
                lockObj.SetActive(false);
                pricePanel.SetActive(true);
                rankPanel.SetActive(true);
                statePanel.SetActive(true);
            }
            else
            {
                lockObj.SetActive(true);
                pricePanel.SetActive(false);
                rankPanel.SetActive(false);
                statePanel.SetActive(false);
            }
            priceText.text = $"{price.ToLitter()}";
            attackStateText.text = $"{attack}";
            hpStateText.text = $"{hp}";
        }

        private void SetVariables()
        {
            switch (unitType)
            {
                case UnitType.Mellee:
                    rank = SaveManager.Instance.GetMainParameters.melleeUnitRank;
                    isOpen = SaveManager.Instance.GetMainParameters.melleeUnitIsOpen;
                    rarity = SaveManager.Instance.GetMainParameters.melleeUnitRarity;
                    break;
                case UnitType.Archer:
                    rank = SaveManager.Instance.GetMainParameters.archerUnitRank;
                    isOpen = SaveManager.Instance.GetMainParameters.archerUnitIsOpen;
                    rarity = SaveManager.Instance.GetMainParameters.archerUnitRarity;
                    break;
                case UnitType.Mage:
                    rank = SaveManager.Instance.GetMainParameters.mageUnitRank;
                    isOpen = SaveManager.Instance.GetMainParameters.mageUnitIsOpen;
                    rarity = SaveManager.Instance.GetMainParameters.mageUnitRarity;
                    healing = AppManager.Instance.GetBalance.GetMageHealingValue(rank, rarity);
                    break;
                case UnitType.Catapult:
                    rank = SaveManager.Instance.GetMainParameters.catapultUnitRank;
                    isOpen = SaveManager.Instance.GetMainParameters.catapultUnitIsOpen;
                    rarity = SaveManager.Instance.GetMainParameters.catapultUnitRarity;
                    break;
                default:
                    break;
            }
            price = AppManager.Instance.GetBalance.GetUnitBuyPrice(rank, rarity, unitType);
            attack = AppManager.Instance.GetBalance.GetUnitAttackValue(rank, rarity, unitType);
            hp = AppManager.Instance.GetBalance.GetUnitHpValue(rank, rarity, unitType);
        }

        private void SetSprites()
        {
            backImg.sprite = AppManager.Instance.GetSpriteList.GetAvatarBackSprite(rarity);
            avatarImg.sprite = AppManager.Instance.GetSpriteList.GetAvatarSpriteMini(unitType, rarity);
        }


        private void SetAdButtonView()
        {
            if (SaveManager.Instance == null) return;
            if (SaveManager.Instance.GetMainParameters == null) return;
            if (AppManager.Instance.CurrentLevelManager == null) return;

            if (!isOpen)
            {
                adRewardButton.gameObject.SetActive(false);
                return;
            }

            if (price > SaveManager.Instance.GetMainParameters.coins)
            {
                pricePanel.SetActive(false);
                adRewardButton.gameObject.SetActive(true);
                button.interactable = false;
            }
            else
            {
                if (isOpen) pricePanel.SetActive(true);
                adRewardButton.gameObject.SetActive(false);
                if (lockTimer <= 0f) button.interactable = true;
                else button.interactable = false;
            }

            if (ApplovinAdsManager.Instance.IsLoadedRewardAds() && AppManager.Instance.CurrentLevelManager.GetFreeMergeCellsCount > 0) adRewardButton.interactable = true;
            else adRewardButton.interactable = false;
        }

        public void UpdateParameters()
        {
            if (price > SaveManager.Instance.GetMainParameters.coins) button.interactable = false;
            else button.interactable = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="callback">return (unitType, unitRarity, rank, price, attack, hp, healing)</param>
        public void AddListenerClick(Action<UnitType, UnitRarity, int, int, int, int, int> callback)
        {
            callbackClick += callback;
        }
        public void Click()
        {
            if (!isOpen) return;
            callbackClick?.Invoke(unitType, rarity, rank, price, attack, hp, healing);
            lockTimer = LOCK_MAX_TIME;
        }
        public void AdRewardClick()
        {
            ApplovinAdsManager.Instance.ShowRewardedAd((success) =>
            {
                if (success)
                {
                    callbackClick?.Invoke(unitType, rarity, rank, 0, attack, hp, healing);
                    lockTimer = LOCK_MAX_TIME;
                }
            });
        }
        public void Clear()
        {
            callbackClick = null;
        }

        
    }
}

