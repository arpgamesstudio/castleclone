using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using I2.Loc;

namespace Project.UI
{
    public class MessagePopup : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI messageText;
        [SerializeField] Image image;
        [Header("I2Loc"), Space(20)]
        [SerializeField] private LocalizedString noPathLocString;

        Color textStartColor;
        Color imageStartColor;

        public enum TextName
        {
            NoPath
        }

        private void Start()
        {
            textStartColor = messageText.color;
            imageStartColor = image.color;
            messageText.color = new Color(0, 0, 0, 0);
            image.color = new Color(0, 0, 0, 0);
        }
        public void ShowMessage(string text)
        {
            messageText.color = textStartColor;
            image.color = imageStartColor;

            messageText.text = $"{text}";
            StartCoroutine(CloseCoroutine());
        }

        public void ShowMessage(TextName textName)
        {
            messageText.color = textStartColor;
            image.color = imageStartColor;

            switch (textName)
            {
                case TextName.NoPath:
                    messageText.text = $"{noPathLocString}";
                    break;
                default:
                    break;
            }
            
            StartCoroutine(CloseCoroutine());
        }

        private IEnumerator CloseCoroutine()
        {
            yield return new WaitForSeconds(1.5f);
            while (image.color.a > 0)
            {
                image.color -= new Color(0, 0, 0, 0.01f);
                messageText.color -= new Color(0, 0, 0, 0.01f);
                yield return null;
            }
        }
    }
}

