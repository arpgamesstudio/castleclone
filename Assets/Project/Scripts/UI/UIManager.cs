using Project.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Project.UI.Popup;

namespace Project.UI
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager Instance;



        private SplashScreen splashScreen;

        public GameScreen GetGameScreen { get; private set; }
        public UnitUpgradeScreen GetUnitUpgradeScreen { get; private set; }
        public Popups GetPopup { get; private set; }
        public MessagePopup GetMessagePopup { get; private set; }
        public MapScreen GetMapScreen { get; private set; }
        public ShopScreen GetShopScreen { get; private set; }


        Action initAction;




        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        internal void Init()
        {
            ClearAll();

            GetGameScreen = FindObjectOfType<GameScreen>();
            GetPopup = FindObjectOfType<Popups>();
            GetUnitUpgradeScreen = FindObjectOfType<UnitUpgradeScreen>();
            GetMessagePopup = FindObjectOfType<MessagePopup>();
            GetMapScreen = FindObjectOfType<MapScreen>();
            GetShopScreen = FindObjectOfType<ShopScreen>();
            splashScreen = FindObjectOfType<SplashScreen>();

            HideScreensOnStartGame();
            initAction?.Invoke();
            GetGameScreen.Init();
            splashScreen.gameObject.SetActive(false);
        }

        private void HideScreensOnStartGame()
        {
            GetUnitUpgradeScreen.gameObject.SetActive(false);
            GetShopScreen.gameObject.SetActive(false);
            HideMapScreen();
        }

        public void ShowUnitUpgradeScreen()
        {
            GetUnitUpgradeScreen.gameObject.SetActive(true);
            GetUnitUpgradeScreen.Init();
        }
        public void HideUnitUpgradeScreen()
        {
            GetUnitUpgradeScreen.gameObject.SetActive(false);
        }
        public void ShowMapScreen()
        {
            GetMapScreen.gameObject.SetActive(true);
            GetMapScreen.Init();
        }
        public void HideMapScreen()
        {
            GetMapScreen.gameObject.SetActive(false);
        }

        public void ShowShopScreen()
        {
            GetShopScreen.gameObject.SetActive(true);
            GetShopScreen.Init();
        }
        public void HideShopScreen()
        {
            GetShopScreen.gameObject.SetActive(false);
        }

        public void AddListenerInit(Action callback) { initAction += callback; }

        private void ClearAll()
        {
            initAction = null;
        }

        internal void Reinit()
        {
            HideScreensOnStartGame();
            GetGameScreen.Init();
        }
    }
}

