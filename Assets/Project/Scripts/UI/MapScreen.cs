using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Project;
using Project.Saves;
using TG.Audio;

namespace Project.UI
{
    public class MapScreen : MonoBehaviour
    {
        [SerializeField] private MapElement prefab;
        [SerializeField] private ScrollRect scrollRect;
        [SerializeField] private GameObject lastMapElement;

        private List<MapElement> mapElements;




        public void Init()
        {
            ClearElements();
            SpawnMapElements();

            if (SaveManager.Instance.GetMainParameters.currentLevelIndex == AppManager.Instance.GetMaxLevelsCount)
            {
                lastMapElement.SetActive(true);
                lastMapElement.transform.SetAsLastSibling();
            }
            else lastMapElement.SetActive(false);
        }

        private void SpawnMapElements()
        {
            if (mapElements == null) mapElements = new List<MapElement>();
             var settings = AppManager.Instance.GetLevelsSettings;

            foreach (var item in settings.GetAllLevels)
            {
                MapElement element = Instantiate(prefab, scrollRect.content);
                element.Init(item);
                mapElements.Add(element);
            }
        }

        private void ClearElements()
        {
            if (mapElements == null) return;

            foreach (var item in mapElements)
            {
                Destroy(item.gameObject);
            }
            mapElements = null;
        }

        public void CloseClick()
        {
            UIManager.Instance.HideMapScreen();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }

        public void CenterViewOnLevel(int level)
        {
            RectTransform target = mapElements.Find(p => p.GetLevel == level)?.GetComponent<RectTransform>();
            if (target != null) scrollRect.GetComponent<ScrollRectEnsureVisible>().CenterOnItem(target);
        }
    }
}

