using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using Project.Saves;
using UnityEngine.UI;
using TG.Audio;

namespace Project.UI
{
    public class GameScreen : MonoBehaviour
    {
        [SerializeField] private MoneyPanel moneyPanel;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private Image levelProgressFill;
        [SerializeField] private TextMeshProUGUI levelProgressText;
        [SerializeField] private Button nextLevelBtn;
        [SerializeField] private GameObject leftBtnsPanel;



        public MoneyPanel GetMoneyPanel { get => moneyPanel; }


        private string startLevelText;
        private string startLevelProgressText;
        private bool isInit = false;



        private void Start()
        {
            startLevelProgressText = levelProgressText.text;
            startLevelText = levelText.text;
        }
        private void Update()
        {
            if (!isInit) return;
            UpdateLevelProgress();
        }

        private void UpdateLevelProgress()
        {
            var levelManager = AppManager.Instance.CurrentLevelManager;
            float progressFill = levelManager.DeadEnemiesCount / (float)levelManager.AllEnemiesCount;
            levelProgressFill.fillAmount = progressFill;
            if (progressFill >= 1f) nextLevelBtn.gameObject.SetActive(true);
            else nextLevelBtn.gameObject.SetActive(false);
            levelProgressText.text = $"{string.Format(startLevelProgressText, levelManager.DeadEnemiesCount, levelManager.AllEnemiesCount)}";
        }

        public void HideLeftBtnsPanel() { leftBtnsPanel.gameObject.SetActive(false); }
        public void ShowLeftBtnsPanel() { leftBtnsPanel.gameObject.SetActive(true); }


        public void Init()
        {
            isInit = true;
            levelText.text = $"{string.Format(startLevelText, SaveManager.Instance.GetMainParameters.currentLevelIndex)}";
        }

        public void OpenUpgradeScreenClick()
        {
            UIManager.Instance.ShowUnitUpgradeScreen();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void OpenMapClick()
        {
            UIManager.Instance.ShowMapScreen();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void OpenShopScreen()
        {
            UIManager.Instance.ShowShopScreen();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void NextLevelClick()
        {
            UIManager.Instance.ShowMapScreen();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
            StartCoroutine(InvokeCor());

            IEnumerator InvokeCor()
            {
                yield return null;
                UIManager.Instance.GetMapScreen.CenterViewOnLevel(SaveManager.Instance.GetMainParameters.currentLevelIndex);
            }
        }
    }
}

