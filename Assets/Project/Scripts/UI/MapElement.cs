using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Project.Settings;
using Project.Saves;
using TG.Audio;

namespace Project.UI
{
    public class MapElement : MonoBehaviour
    {
        [SerializeField] private Image castleImg;
        [SerializeField] private Image bossImg;
        [SerializeField] private Image hideImg;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private Button rewardBtn;
        [SerializeField] private GameObject progressBackObj;
        [SerializeField] private Image progressFillImg;
        [SerializeField] private TextMeshProUGUI progressText;
        [SerializeField] private GameObject completeObj;
        [SerializeField] private Button startLevelBtn;



        private string startLevelTextValue;
        private string startProgressTextValue;
        SaveManager.MainParameters saveMainData;
        SaveManager.LevelData saveLevelData;
        LevelsList.LevelSettings levelSettings;


        public int GetLevel { get; private set; }






        public void Init(LevelsList.LevelSettings levelSettings)
        {
            rewardBtn.gameObject.SetActive(false);//remove it after implementation

            GetLevel = levelSettings.GetIndex;
            if (string.IsNullOrEmpty(startLevelTextValue)) startLevelTextValue = levelText.text;
            if (string.IsNullOrEmpty(startProgressTextValue)) startProgressTextValue = progressText.text;

            this.levelSettings = levelSettings;
            saveMainData = SaveManager.Instance.GetMainParameters;
            saveLevelData = SaveManager.Instance.GetLevelData;

            if (levelSettings.GetIndex > saveMainData.currentLevelIndex)
            {
                //HiddenLevel
                castleImg.gameObject.SetActive(false);
                bossImg.gameObject.SetActive(false);
                hideImg.gameObject.SetActive(true);
                levelText.text = $"{string.Format(startLevelTextValue, levelSettings.GetIndex)}";
                //rewardBtn.gameObject.SetActive(false);
                progressBackObj.SetActive(false);
                completeObj.SetActive(false);
                if (HasLastLevelComplete()) startLevelBtn.gameObject.SetActive(true);
                else startLevelBtn.gameObject.SetActive(false);
            }
            else if (levelSettings.GetIndex == saveMainData.currentLevelIndex)
            {
                //CurrentProgress
                castleImg.gameObject.SetActive(true);
                bossImg.gameObject.SetActive(true);
                castleImg.sprite = levelSettings.GetMapCastleSprite;
                bossImg.sprite = levelSettings.GetMapBossSprite;
                hideImg.gameObject.SetActive(false);
                startLevelBtn.gameObject.SetActive(false);
                levelText.text = $"{string.Format(startLevelTextValue, levelSettings.GetIndex)}";

                int allEnemiesCount = levelSettings.GetLevelPrefab.AllEnemiesCount;
                int deadEnemiesCount = 0;

                if (HasLevelComplete(out deadEnemiesCount))
                {
                    //rewardBtn.gameObject.SetActive(true);
                    progressBackObj.SetActive(false);
                }
                else
                {
                   // rewardBtn.gameObject.SetActive(false);
                    progressBackObj.SetActive(true);

                    progressFillImg.fillAmount = deadEnemiesCount / (float)allEnemiesCount;
                    progressText.text = $"{string.Format(startProgressTextValue, deadEnemiesCount, allEnemiesCount)}";
                }

                completeObj.SetActive(false);
            }
            else
            {
                //Copmleted
                completeObj.SetActive(true);
                rewardBtn.gameObject.SetActive(false);
                progressBackObj.SetActive(false);
                hideImg.gameObject.SetActive(false);
                startLevelBtn.gameObject.SetActive(false);
                levelText.text = $"{string.Format(startLevelTextValue, levelSettings.GetIndex)}";
                castleImg.gameObject.SetActive(true);
                bossImg.gameObject.SetActive(true);
                castleImg.sprite = levelSettings.GetMapCastleSprite;
                bossImg.sprite = levelSettings.GetMapBossSprite;
            }
        }



        private bool HasLastLevelComplete()
        {
            if (GetLevel - 1 != SaveManager.Instance.GetMainParameters.currentLevelIndex) return false;
            int allEnemiesCount = AppManager.Instance.GetLevelsSettings.GetLevel(GetLevel - 1).GetLevelPrefab.AllEnemiesCount;
            int deadEnemiesCount = 0;
            foreach (var enemy in saveLevelData.enemyDatas)
            {
                if (enemy.isDead) deadEnemiesCount++;
            }

            bool isLevelComplete = deadEnemiesCount == allEnemiesCount;
            return isLevelComplete;
        }
        private bool HasLevelComplete(out int deadEnemiesCount)
        {
            int allEnemiesCount = levelSettings.GetLevelPrefab.AllEnemiesCount;
            deadEnemiesCount = 0;
            foreach (var enemy in saveLevelData.enemyDatas)
            {
                if (enemy.isDead) deadEnemiesCount++;
            }

            bool isLevelComplete = deadEnemiesCount == allEnemiesCount;
            return isLevelComplete;
        }

        public void GetRewardsClick()
        {
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
        public void StartLevelClick()
        {
            AppManager.Instance.NextLevel();
            UIManager.Instance.HideMapScreen();
            SoundManager.Instance.PlayAudioClip(AppManager.Instance.GetAudioLibrary.GetSound("ClickUI"));
        }
    }
}

