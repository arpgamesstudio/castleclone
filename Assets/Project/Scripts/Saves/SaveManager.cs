using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Project.Saves
{
    public partial class SaveManager : MonoBehaviour
    {
        public static SaveManager Instance;

        private const string MAIN_PARAMETERS = "MainParameters";
        private const string LEVEL_DATA = "LevelData";
        private const string SHOP_TIMER = "ShopTimer";
        private const string TUTOR_STEP = "TutorStep";


        [System.Serializable]
        private class ArraySerialize<T>
        {
            public T[] array;
        }





        private bool isInitServer = false;


        private MainParameters mainParameters;
        private LevelData levelData;
        private DateTime serverTime;

        public bool IsInitServer { get; private set; }
        public MainParameters GetMainParameters { get => mainParameters;}
        public LevelData GetLevelData { get => levelData; }
        public DateTime GetCurrentTime { get => serverTime; }
        public DateTime GetShopTimer { get; private set; }
        public int CurrentTutorStep { get; private set; }



        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                StartCoroutine(GetInternetTime());
            }
            else
            {
                Destroy(gameObject);
            }
        }


        public IEnumerator GetInternetTime()
        {
            UnityWebRequest myHttpWebRequest = UnityWebRequest.Get("https://www.microsoft.com");
            yield return myHttpWebRequest.SendWebRequest();

            string netTime = myHttpWebRequest.GetResponseHeader("date");
            serverTime = DateTime.Parse(netTime);

            StartCoroutine(UpdateInternetTime());
        }
        public IEnumerator UpdateInternetTime()
        {
            while (true)
            {
                yield return new WaitForSeconds(1f);
                serverTime = serverTime.AddSeconds(1);
            }
        }



        /// <summary>
        /// ��������� ��� ������ ���������� ������
        /// </summary>
        /// /// <param name="levelId">�� ������</param>
        /// <param name="callback">������� ��������/�� �������� ��������, return void(success, msg)</param>
        public void LoadAllDatas(Action<bool, string> callback) { StartCoroutine(LoadAllDatasCoroutine(callback)); }

        private IEnumerator LoadAllDatasCoroutine(Action<bool, string> callback)
        {
            bool isLoadPartData = false;
            StartCoroutine(LoadMainParameters((success) =>
           {
               if (!success) callback?.Invoke(false, "Error: Loading MainParameters data is fail.");
               isLoadPartData = success;
           }));
            yield return new WaitUntil(predicate: () => isLoadPartData);



            isLoadPartData = false;
            StartCoroutine(LoadLevelData((success) =>
           {
               if (!success) callback?.Invoke(false, "Error: Loading LevelData data is fail.");
               isLoadPartData = success;
           }));
            yield return new WaitUntil(predicate: () => isLoadPartData);



            isLoadPartData = false;
            StartCoroutine(LoadShopTimer((success) =>
            {
                if (!success) callback?.Invoke(false, "Error: Loading ShopTime data is fail.");
                isLoadPartData = success;
            }));
            yield return new WaitUntil(predicate: () => isLoadPartData);


            isLoadPartData = false;
            StartCoroutine(LoadTutor((success) =>
            {
                if (!success) callback?.Invoke(false, "Error: Loading Tutor data is fail.");
                isLoadPartData = success;
            }));
            yield return new WaitUntil(predicate: () => isLoadPartData);



            callback?.Invoke(true, "Load all datas success!");
        }



        private bool HasSaveKey(string key)
        {
            if (IsInitServer)
            {
                //TODO: check server key
                return false;
            }
            else
            {
                return PlayerPrefs.HasKey(key);
            }
        }

        private void SaveLocalData(string key, string data) { PlayerPrefs.SetString(key, data); }
        private void SaveServerData(string key, string data)
        {
            //TODO: save data to server
        }

        private string LoadLocalData(string key) => PlayerPrefs.GetString(key);
        private IEnumerator LoadServerData(string key, Action<string> result)
        {
            bool isError = false;
            string json = "";

            //TODO: load data from server
            Thread.Sleep(5000);

            if (isError) result?.Invoke("");
            else result?.Invoke(json);
            yield return null;
        }






        #region Tutor
        private IEnumerator LoadTutor(Action<bool> callback)
        {
            if (HasSaveKey(TUTOR_STEP))
            {
                string valStr = "";
                if (IsInitServer)
                {
                    yield return StartCoroutine(LoadServerData(TUTOR_STEP, (str) =>
                    {
                        valStr = str;
                    }));
                }
                else valStr = LoadLocalData(TUTOR_STEP);

                if (!string.IsNullOrEmpty(valStr))
                {
                    CurrentTutorStep = int.Parse(valStr);
                }
                else CurrentTutorStep = 1;
            }
            else CurrentTutorStep = 1;

            callback?.Invoke(true);
        }

        public void SaveTutor()
        {
            string valStr = "";
            valStr = CurrentTutorStep.ToString();
            if (IsInitServer) SaveServerData(TUTOR_STEP, valStr);
            else SaveLocalData(TUTOR_STEP, valStr);
        }

        internal void AddTutorStep(bool isSave)
        {
            CurrentTutorStep++;
            if (isSave) SaveTutor();
        }
        #endregion
        #region ShopTimer
        private IEnumerator LoadShopTimer(Action<bool> callback)
        {
            if (HasSaveKey(SHOP_TIMER))
            {
                string valStr = "";
                if (IsInitServer)
                {
                    yield return StartCoroutine(LoadServerData(SHOP_TIMER, (str) =>
                    {
                        valStr = str;
                    }));
                }
                else valStr = LoadLocalData(SHOP_TIMER);

                if (!string.IsNullOrEmpty(valStr))
                {
                    GetShopTimer = DateTime.Parse(valStr);
                }
                else GetShopTimer = DateTime.MinValue;
            }

            callback?.Invoke(true);
        }
        public void ResetShopTimer()
        {
            GetShopTimer = new DateTime(serverTime.Ticks);
            if (IsInitServer) SaveServerData(SHOP_TIMER, GetShopTimer.ToString());
            else SaveLocalData(SHOP_TIMER, GetShopTimer.ToString());
        }
        #endregion
        #region MainParameters
        private IEnumerator LoadMainParameters(Action<bool> callback)
        {
            if (HasSaveKey(MAIN_PARAMETERS))
            {
                string json = "";
                if (IsInitServer)
                {
                    yield return StartCoroutine(LoadServerData(MAIN_PARAMETERS, (str) =>
                    {
                        json = str;
                    }));
                }
                else json = LoadLocalData(MAIN_PARAMETERS);

                if (!string.IsNullOrEmpty(json))
                {
                    MainParameters serialize = JsonUtility.FromJson<MainParameters>(json);

                    mainParameters = new MainParameters();

                    mainParameters.currentLevelIndex = serialize.currentLevelIndex;
                    mainParameters.spawnPointsAvailable = serialize.spawnPointsAvailable;
                    mainParameters.coins = serialize.coins;
                    mainParameters.diamonds = serialize.diamonds;

                    mainParameters.melleeUnitRank = serialize.melleeUnitRank;
                    mainParameters.melleeUnitIsOpen = serialize.melleeUnitIsOpen;
                    mainParameters.melleeUnitRarity = serialize.melleeUnitRarity;
                    mainParameters.archerUnitRank = serialize.archerUnitRank;
                    mainParameters.archerUnitIsOpen = serialize.archerUnitIsOpen;
                    mainParameters.archerUnitRarity = serialize.archerUnitRarity;
                    mainParameters.mageUnitRank = serialize.mageUnitRank;
                    mainParameters.mageUnitIsOpen = serialize.mageUnitIsOpen;
                    mainParameters.mageUnitRarity = serialize.mageUnitRarity;
                    mainParameters.catapultUnitRank = serialize.catapultUnitRank;
                    mainParameters.catapultUnitIsOpen = serialize.catapultUnitIsOpen;
                    mainParameters.catapultUnitRarity = serialize.catapultUnitRarity;

                    callback?.Invoke(true);
                }
                else callback?.Invoke(false);
            }
            else CreateFirstMainParameters(callback);
            yield return null;
        }



        private void SaveMainParameters()
        {
            string json = "";

            MainParameters serialize = new MainParameters(mainParameters);
            json = JsonUtility.ToJson(serialize);

            if (IsInitServer) SaveServerData(MAIN_PARAMETERS, json);
            else SaveLocalData(MAIN_PARAMETERS, json);
        }
        private void CreateFirstMainParameters(Action<bool> callback)
        {
            mainParameters = new MainParameters(GetFirstParamsPortalRoom());

            SaveMainParameters();
            callback?.Invoke(true);
        }
        private MainParameters GetFirstParamsPortalRoom()
        {
            MainParameters data = new MainParameters();
            data.currentLevelIndex = 1;
            data.spawnPointsAvailable = 2;
            data.coins = AppManager.Instance.GetBalance.GetStartCoinsCount;
            data.diamonds = AppManager.Instance.GetBalance.GetStartDiamondCount;

            data.melleeUnitRank = 0;
            data.melleeUnitIsOpen = AppManager.Instance.GetBalance.GetStartMelleeOpen;
            data.melleeUnitRarity = UnitRarity.Normal;
            data.archerUnitRank = 0;
            data.archerUnitIsOpen = AppManager.Instance.GetBalance.GetStartArcherOpen;
            data.archerUnitRarity = UnitRarity.Normal;
            data.mageUnitRank = 0;
            data.mageUnitIsOpen = AppManager.Instance.GetBalance.GetStartMageOpen;
            data.mageUnitRarity = UnitRarity.Normal;
            data.catapultUnitRank = 0;
            data.catapultUnitIsOpen = AppManager.Instance.GetBalance.GetStartCatapultOpen;
            data.catapultUnitRarity = UnitRarity.Normal;
            return data;
        }

        public void RemoveCurrency(CurrencyType currencyType, int count)
        {
            switch (currencyType)
            {
                case CurrencyType.Coins:
                    mainParameters.coins -= count;
                    if (mainParameters.coins < 0) mainParameters.coins = 0;
                    SaveMainParameters();
                    break;
                case CurrencyType.Diamond:
                    mainParameters.diamonds -= count;
                    if (mainParameters.diamonds < 0) mainParameters.diamonds = 0;
                    SaveMainParameters();
                    break;
                case CurrencyType.Purchase:
                    break;
                default:
                    break;
            }
        }
        public void AddCurrency(CurrencyType currencyType, int count)
        {
            switch (currencyType)
            {
                case CurrencyType.Coins:
                    mainParameters.coins += count;
                    SaveMainParameters();
                    break;
                case CurrencyType.Diamond:
                    mainParameters.diamonds += count;
                    SaveMainParameters();
                    break;
                case CurrencyType.Purchase:
                    break;
                default:
                    break;
            }
        }
        public void AddMergeCell()
        {
            mainParameters.spawnPointsAvailable++;
            SaveMainParameters();
        }

        internal void UpgradeUnit(UnitType currentUnitType)
        {
            switch (currentUnitType)
            {
                case UnitType.Mellee:
                    if (mainParameters.melleeUnitRank < 5)
                    {
                        mainParameters.melleeUnitRank++;
                    }
                    else
                    {
                        switch (mainParameters.melleeUnitRarity)
                        {
                            case UnitRarity.Normal:
                                mainParameters.melleeUnitRarity = UnitRarity.Improved;
                                mainParameters.melleeUnitRank = 0;
                                break;
                            case UnitRarity.Improved:
                                mainParameters.melleeUnitRarity = UnitRarity.Rare;
                                mainParameters.melleeUnitRank = 0;
                                break;
                            case UnitRarity.Rare:
                                mainParameters.melleeUnitRarity = UnitRarity.Unique;
                                mainParameters.melleeUnitRank = 0;
                                break;
                            case UnitRarity.Unique:
                                mainParameters.melleeUnitRarity = UnitRarity.Legendary;
                                mainParameters.melleeUnitRank = 0;
                                break;
                            case UnitRarity.Legendary:
                                mainParameters.melleeUnitRarity = UnitRarity.Mythical;
                                mainParameters.melleeUnitRank = 0;
                                break;
                            case UnitRarity.Mythical:
                                mainParameters.melleeUnitRarity = UnitRarity.Immortal;
                                mainParameters.melleeUnitRank = 0;
                                break;
                            case UnitRarity.Immortal:
                                mainParameters.melleeUnitRarity = UnitRarity.Immortal;
                                mainParameters.melleeUnitRank = 5;
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case UnitType.Archer:
                    if (mainParameters.archerUnitRank < 5)
                    {
                        mainParameters.archerUnitRank++;
                    }
                    else
                    {
                        switch (mainParameters.archerUnitRarity)
                        {
                            case UnitRarity.Normal:
                                mainParameters.archerUnitRarity = UnitRarity.Improved;
                                mainParameters.archerUnitRank = 0;
                                break;
                            case UnitRarity.Improved:
                                mainParameters.archerUnitRarity = UnitRarity.Rare;
                                mainParameters.archerUnitRank = 0;
                                break;
                            case UnitRarity.Rare:
                                mainParameters.archerUnitRarity = UnitRarity.Unique;
                                mainParameters.archerUnitRank = 0;
                                break;
                            case UnitRarity.Unique:
                                mainParameters.archerUnitRarity = UnitRarity.Legendary;
                                mainParameters.archerUnitRank = 0;
                                break;
                            case UnitRarity.Legendary:
                                mainParameters.archerUnitRarity = UnitRarity.Mythical;
                                mainParameters.archerUnitRank = 0;
                                break;
                            case UnitRarity.Mythical:
                                mainParameters.archerUnitRarity = UnitRarity.Immortal;
                                mainParameters.archerUnitRank = 0;
                                break;
                            case UnitRarity.Immortal:
                                mainParameters.archerUnitRarity = UnitRarity.Immortal;
                                mainParameters.archerUnitRank = 5;
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case UnitType.Mage:
                    if (mainParameters.mageUnitRank < 5)
                    {
                        mainParameters.mageUnitRank++;
                    }
                    else
                    {
                        switch (mainParameters.mageUnitRarity)
                        {
                            case UnitRarity.Normal:
                                mainParameters.mageUnitRarity = UnitRarity.Improved;
                                mainParameters.mageUnitRank = 0;
                                break;
                            case UnitRarity.Improved:
                                mainParameters.mageUnitRarity = UnitRarity.Rare;
                                mainParameters.mageUnitRank = 0;
                                break;
                            case UnitRarity.Rare:
                                mainParameters.mageUnitRarity = UnitRarity.Unique;
                                mainParameters.mageUnitRank = 0;
                                break;
                            case UnitRarity.Unique:
                                mainParameters.mageUnitRarity = UnitRarity.Legendary;
                                mainParameters.mageUnitRank = 0;
                                break;
                            case UnitRarity.Legendary:
                                mainParameters.mageUnitRarity = UnitRarity.Mythical;
                                mainParameters.mageUnitRank = 0;
                                break;
                            case UnitRarity.Mythical:
                                mainParameters.mageUnitRarity = UnitRarity.Immortal;
                                mainParameters.mageUnitRank = 0;
                                break;
                            case UnitRarity.Immortal:
                                mainParameters.mageUnitRarity = UnitRarity.Immortal;
                                mainParameters.mageUnitRank = 5;
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case UnitType.Catapult:
                    if (mainParameters.catapultUnitRank < 5)
                    {
                        mainParameters.catapultUnitRank++;
                    }
                    else
                    {
                        switch (mainParameters.catapultUnitRarity)
                        {
                            case UnitRarity.Normal:
                                mainParameters.catapultUnitRarity = UnitRarity.Improved;
                                mainParameters.catapultUnitRank = 0;
                                break;
                            case UnitRarity.Improved:
                                mainParameters.catapultUnitRarity = UnitRarity.Rare;
                                mainParameters.catapultUnitRank = 0;
                                break;
                            case UnitRarity.Rare:
                                mainParameters.catapultUnitRarity = UnitRarity.Unique;
                                mainParameters.catapultUnitRank = 0;
                                break;
                            case UnitRarity.Unique:
                                mainParameters.catapultUnitRarity = UnitRarity.Legendary;
                                mainParameters.catapultUnitRank = 0;
                                break;
                            case UnitRarity.Legendary:
                                mainParameters.catapultUnitRarity = UnitRarity.Mythical;
                                mainParameters.catapultUnitRank = 0;
                                break;
                            case UnitRarity.Mythical:
                                mainParameters.catapultUnitRarity = UnitRarity.Immortal;
                                mainParameters.catapultUnitRank = 0;
                                break;
                            case UnitRarity.Immortal:
                                mainParameters.catapultUnitRarity = UnitRarity.Immortal;
                                mainParameters.catapultUnitRank = 5;
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                default:
                    break;
            }
            SaveMainParameters();
        }
        #endregion
        #region LevelData
        private IEnumerator LoadLevelData(Action<bool> callback)
        {
            if (HasSaveKey(LEVEL_DATA))
            {
                string json = "";
                if (IsInitServer)
                {
                    yield return StartCoroutine(LoadServerData(LEVEL_DATA, (str) =>
                    {
                        json = str;
                    }));
                }
                else json = LoadLocalData(LEVEL_DATA);

                if (!string.IsNullOrEmpty(json))
                {
                    LevelData serialize = JsonUtility.FromJson<LevelData>(json);

                    levelData = new LevelData();

                    levelData.isGetReward = serialize.isGetReward;

                    levelData.mergeCellDatas = new List<LevelData.MergeCellData>();

                    foreach (var item in serialize.mergeCellDatas)
                    {
                        LevelData.MergeCellData data = new LevelData.MergeCellData(item.index, item.isOccupied, item.unitType, item.mergeCount, item.unitHp, item.unitXPos, item.unitYPos);
                        levelData.mergeCellDatas.Add(data);
                    }

                    levelData.chestDatas = new List<LevelData.ChestData>();
                    foreach (var item in serialize.chestDatas)
                    {
                        LevelData.ChestData data = new LevelData.ChestData(item.index, item.hp, item.isOpen, item.startTime);
                        levelData.chestDatas.Add(data);
                    }

                    levelData.enemyDatas = new List<LevelData.EnemyData>();
                    foreach (var item in serialize.enemyDatas)
                    {
                        LevelData.EnemyData data = new LevelData.EnemyData(item.index, item.hp, item.isDead);
                        levelData.enemyDatas.Add(data);
                    }

                    callback?.Invoke(true);
                }
                else CreateFirstLevelData(callback);
            }
            else CreateFirstLevelData(callback);
            yield return null;
        }



        private void SaveLevelData()
        {
            string json = "";

            LevelData serialize = new LevelData(levelData);
            json = JsonUtility.ToJson(serialize);
            Debug.Log(json);
            if (IsInitServer) SaveServerData(LEVEL_DATA, json);
            else SaveLocalData(LEVEL_DATA, json);
        }
        internal void NextLevel()
        {
            OpenUnit();
            mainParameters.currentLevelIndex++;
            if (mainParameters.currentLevelIndex > AppManager.Instance.GetMaxLevelsCount) mainParameters.currentLevelIndex = AppManager.Instance.GetMaxLevelsCount;
            SaveMainParameters();
            CreateFirstLevelData((success) => { });

            void OpenUnit()
            {
                var levelSettings = AppManager.Instance.GetLevelsSettings.GetLevel(mainParameters.currentLevelIndex);
                if (levelSettings == null) return;

                switch (levelSettings.OpenUnit)
                {
                    case UnitType.Mellee:
                        break;
                    case UnitType.Archer:
                        mainParameters.archerUnitIsOpen = true;
                        break;
                    case UnitType.Mage:
                        mainParameters.mageUnitIsOpen = true;
                        break;
                    case UnitType.Catapult:
                        mainParameters.catapultUnitIsOpen = true;
                        break;
                    default:
                        break;
                }
            }
        }

        private void CreateFirstLevelData(Action<bool> callback)
        {
            levelData = new LevelData();
            levelData.isGetReward = false;

            var settings = AppManager.Instance.GetLevelsSettings.GetLevel(mainParameters.currentLevelIndex);
            foreach (var item in settings.GetLevelPrefab.GetMergeFieldCells)
            {
                int index = item.GetIndex;
                bool isOccupied = false;
                UnitType unitType = UnitType.Mellee;
                int mergeCount = 0;
                int unitHp = 0;
                int posX = item.GetMapPosition.x;
                int posY = item.GetMapPosition.y;
                LevelData.MergeCellData data = new LevelData.MergeCellData(index, isOccupied, unitType, mergeCount, unitHp, posX, posY);
                if (levelData.mergeCellDatas == null) levelData.mergeCellDatas = new List<LevelData.MergeCellData>();
                levelData.mergeCellDatas.Add(data);
            }

            for (int i = 0; i < settings.GetLevelPrefab.GetChests.Count; i++)
            {
                var chest = settings.GetLevelPrefab.GetChests[i];
                int index = i;
                int hp = chest.GetMaxHp;
                bool isOpen = false;
                string startTime = "";
                LevelData.ChestData data = new LevelData.ChestData(index, hp, isOpen, startTime);
                if (levelData.chestDatas == null) levelData.chestDatas = new List<LevelData.ChestData>();
                levelData.chestDatas.Add(data);
            }

            for (int i = 0; i < settings.GetLevelPrefab.GetEnemies.Count; i++)
            {
                var enemy = settings.GetLevelPrefab.GetEnemies[i];
                int index = i;
                int hp = enemy.GetMaxHp;
                bool isDead = false;
                LevelData.EnemyData data = new LevelData.EnemyData(index, hp, isDead);
                if (levelData.enemyDatas == null) levelData.enemyDatas = new List<LevelData.EnemyData>();
                levelData.enemyDatas.Add(data);
            }

            SaveLevelData();
            callback?.Invoke(true);
        }

        

        /// <summary>
        /// Set merge cell settings
        /// </summary>
        /// <param name="index">Cell index</param>
        /// <param name="unitType"></param>
        /// <param name="isOccepied"></param>
        /// <param name="unitHp"></param>
        /// <param name="mergeCount"></param>
        public void LevelSetCell(int index, bool isOccepied, UnitType unitType = UnitType.Mellee, int unitHp = 0, int mergeCount = 0)
        {
            var cell = levelData.GetCell(index);
            if (cell == null) return;

            cell.unitType = unitType;
            cell.isOccupied = isOccepied;
            cell.unitHp = unitHp;
            cell.mergeCount = mergeCount;
            SaveLevelData();
        }
        public void UnitSetPos(int index, int posX, int posY)
        {
            var cell = levelData.GetCell(index);
            if (cell == null) return;

            cell.unitXPos = posX;
            cell.unitYPos = posY;
            SaveLevelData();
        }
        public void RemoveWarriorUnitHp(int index, int value)
        {
            var cell = levelData.GetCell(index);
            if (cell == null) return;
            cell.unitHp -= value;
            if (cell.unitHp <= 0)
            {
                cell.unitHp = 0;
            }
            SaveLevelData();
        }
        public void RemoveEnemyHp(int index, int value)
        {
            var enemy = levelData.GetEnemyData(index);
            if (enemy == null) return;
            enemy.hp -= value;
            if (enemy.hp <= 0)
            {
                enemy.hp = 0;
                enemy.isDead = true;
            }
            SaveLevelData();
        }
        public void RemoveChestHp(int index, int value)
        {
            var chest = levelData.GetChestData(index);
            if (chest == null) return;
            chest.hp -= value;
            if (chest.hp <= 0)
            {
                chest.hp = 0;
                chest.isOpen = true;
                chest.startTime = GetCurrentTime.ToString();
            }
            SaveLevelData();
        }
        public void CloseChest(int index, int hp)
        {
            var chest = levelData.GetChestData(index);
            if (chest == null) return;
            chest.hp = hp;
            chest.isOpen = false;
            SaveLevelData();
        }
        public void MinusChestTime(int index, int sec)
        {
            var chest = levelData.GetChestData(index);
            if (chest == null) return;

            var startTime = DateTime.Parse(chest.startTime);
            var result = startTime.AddSeconds(-sec);
            chest.startTime = result.ToString();
            SaveLevelData();
        }

#endregion
    }
}

