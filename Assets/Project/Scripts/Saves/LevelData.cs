﻿using System.Collections.Generic;


namespace Project.Saves
{
    public partial class SaveManager
    {
        [System.Serializable]
        public class LevelData
        {
            public List<MergeCellData> mergeCellDatas;
            public List<ChestData> chestDatas;
            public List<EnemyData> enemyDatas;
            public bool isGetReward;

            public LevelData() { }

            public LevelData(LevelData levelData) 
            {
                isGetReward = levelData.isGetReward;
                mergeCellDatas = new List<MergeCellData>();
                foreach (var item in levelData.mergeCellDatas)
                {
                    MergeCellData data = new MergeCellData(item.index, item.isOccupied, item.unitType, item.mergeCount, item.unitHp, item.unitXPos, item.unitYPos);
                    mergeCellDatas.Add(data);
                }

                chestDatas = new List<ChestData>();
                foreach (var item in levelData.chestDatas)
                {
                    ChestData data = new ChestData(item.index, item.hp, item.isOpen, item.startTime);
                    chestDatas.Add(data);
                }

                enemyDatas = new List<EnemyData>();
                foreach (var item in levelData.enemyDatas)
                {
                    EnemyData data = new EnemyData(item.index, item.hp, item.isDead);
                    enemyDatas.Add(data);
                }
            }


            public MergeCellData GetCell(int index) => mergeCellDatas.Find(p => p.index == index);
            public ChestData GetChestData(int index) => chestDatas.Find(p => p.index == index);
            public EnemyData GetEnemyData(int index) => enemyDatas.Find(p => p.index == index);



            [System.Serializable]
            public class MergeCellData
            {
                public int index;
                public bool isOccupied;
                public UnitType unitType;
                public int mergeCount;
                public int unitHp;
                public int unitXPos;
                public int unitYPos;

                public MergeCellData(int index, bool isOccupied, UnitType unitType, int mergeCount, int unitHp, int unitXPos, int unitYPos)
                {
                    this.index = index;
                    this.isOccupied = isOccupied;
                    this.unitType = unitType;
                    this.mergeCount = mergeCount;
                    this.unitHp = unitHp;
                    this.unitXPos = unitXPos;
                    this.unitYPos = unitYPos;
                }
            }

            [System.Serializable]
            public class ChestData
            {
                public int index;
                public int hp;
                public bool isOpen;
                public string startTime;

                public ChestData(int index, int hp, bool isOpen, string startTime)
                {
                    this.index = index;
                    this.hp = hp;
                    this.isOpen = isOpen;
                    this.startTime = startTime;
                }
            }

            [System.Serializable]
            public class EnemyData
            {
                public int index;
                public int hp;
                public bool isDead;

                public EnemyData(int index, int hp, bool isDead)
                {
                    this.index = index;
                    this.hp = hp;
                    this.isDead = isDead;
                }
            }
        }
    }
}

