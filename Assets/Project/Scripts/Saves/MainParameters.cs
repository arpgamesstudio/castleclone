﻿using System;

namespace Project.Saves
{
    public partial class SaveManager
    {
        [System.Serializable]
        public class MainParameters
        {
            public int currentLevelIndex;
            public int spawnPointsAvailable;
            public int coins;
            public int diamonds;

            public int melleeUnitRank;
            public bool melleeUnitIsOpen;
            public UnitRarity melleeUnitRarity;
            public int archerUnitRank;
            public bool archerUnitIsOpen;
            public UnitRarity archerUnitRarity;
            public int mageUnitRank;
            public bool mageUnitIsOpen;
            public UnitRarity mageUnitRarity;
            public int catapultUnitRank;
            public bool catapultUnitIsOpen;
            public UnitRarity catapultUnitRarity;

            public UnitRarity GetUnitRarity(UnitType unitType)
            {
                UnitRarity rarity = UnitRarity.Normal;
                switch (unitType)
                {
                    case UnitType.Mellee:
                        rarity = melleeUnitRarity;
                        break;
                    case UnitType.Archer:
                        rarity = archerUnitRarity;
                        break;
                    case UnitType.Mage:
                        rarity = mageUnitRarity;
                        break;
                    case UnitType.Catapult:
                        rarity = catapultUnitRarity;
                        break;
                    default:
                        break;
                }
                return rarity;
            }
            public int GetUnitRank(UnitType unitType)
            {
                int rank = 0;
                switch (unitType)
                {
                    case UnitType.Mellee:
                        rank = melleeUnitRank;
                        break;
                    case UnitType.Archer:
                        rank = archerUnitRank;
                        break;
                    case UnitType.Mage:
                        rank = mageUnitRank;
                        break;
                    case UnitType.Catapult:
                        rank = catapultUnitRank;
                        break;
                    default:
                        break;
                }
                return rank;
            }
            public bool GetUnitIsOpen(UnitType unitType)
            {
                bool isOpen = false;
                switch (unitType)
                {
                    case UnitType.Mellee:
                        isOpen = melleeUnitIsOpen;
                        break;
                    case UnitType.Archer:
                        isOpen = archerUnitIsOpen;
                        break;
                    case UnitType.Mage:
                        isOpen = mageUnitIsOpen;
                        break;
                    case UnitType.Catapult:
                        isOpen = catapultUnitIsOpen;
                        break;
                    default:
                        break;
                }
                return isOpen;
            }
            public MainParameters() { }

            public MainParameters(MainParameters mainParameters)
            {
                this.currentLevelIndex = mainParameters.currentLevelIndex;
                this.spawnPointsAvailable = mainParameters.spawnPointsAvailable;
                this.coins = mainParameters.coins;
                this.diamonds = mainParameters.diamonds;
                this.melleeUnitRank = mainParameters.melleeUnitRank;
                this.melleeUnitIsOpen = mainParameters.melleeUnitIsOpen;
                this.melleeUnitRarity = mainParameters.melleeUnitRarity;
                this.archerUnitRank = mainParameters.archerUnitRank;
                this.archerUnitIsOpen = mainParameters.archerUnitIsOpen;
                this.archerUnitRarity = mainParameters.archerUnitRarity;
                this.mageUnitRank = mainParameters.mageUnitRank;
                this.mageUnitIsOpen = mainParameters.mageUnitIsOpen;
                this.mageUnitRarity = mainParameters.mageUnitRarity;
                this.catapultUnitRank = mainParameters.catapultUnitRank;
                this.catapultUnitIsOpen = mainParameters.catapultUnitIsOpen;
                this.catapultUnitRarity = mainParameters.catapultUnitRarity;
            }
        }

        
    }
}

