using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Project.Controllers
{
    public class EnemyAnimationController : MonoBehaviour
    {
        private Action onAttackAction;
        private Action endAttackAction;
        private Action endDamageAction;


        private Animator animator;


        private void Start()
        {
            animator = GetComponent<Animator>();    
        }

        public void EndDamageEvent()
        {
            endDamageAction?.Invoke();
        }
        public void EndAttackEvent()
        {
            endAttackAction?.Invoke();
        }
        public void OnAttackEvent()
        {
            onAttackAction?.Invoke();
        }

        public void AddListenerEndDamage(Action callback)
        {
            endDamageAction += callback;
        }
        public void AddListenerEndAttack(Action callback)
        {
            endAttackAction += callback;
        }
        public void AddListenerOnAttack(Action callback)
        {
            onAttackAction += callback;
        }

        public void Attack()
        {
            animator.SetTrigger("Attack");
        }

        public void Damage()
        {
            animator.SetTrigger("Damage");
        }
        public void Die()
        {
            animator.SetBool("Dead", true);
        }
    }
}

