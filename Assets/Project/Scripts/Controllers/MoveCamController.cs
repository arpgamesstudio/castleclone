﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Project.Controllers
{
    public class MoveCamController : MonoBehaviour
    {
        [SerializeField] private float speed = 0.6f;
        [SerializeField] private float touchSpeed = 0.5f;
        [SerializeField] private bool lockHorizontalMove;

        private MoveCamBorderLevels moveCamBorderLevels;


        private float targetZDepth = 0;
        private float t = 0;
        private bool isSetStarPosition;

        private float startHorPosition;


        public bool IsMove { get; private set; }


        public void SetBorders(MoveCamBorderLevels moveCamBorderLevels)
        {
            this.moveCamBorderLevels = moveCamBorderLevels;
        }

        private void Start()
        {
            startHorPosition = transform.position.x;
            isSetStarPosition = false;
        }


        void Update()
        {
            if (moveCamBorderLevels == null) return;

            GameObject cam = gameObject;

            if (cam.transform.position.z > moveCamBorderLevels.frontBorder)
            {
                cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, moveCamBorderLevels.frontBorder);
                if (targetPosition != Vector3.zero) targetPosition = new Vector3(targetPosition.x, cam.transform.position.y, moveCamBorderLevels.frontBorder);
            }
            if (cam.transform.position.z < moveCamBorderLevels.backBorder)
            {
                cam.transform.position = new Vector3(cam.transform.position.x, cam.transform.position.y, moveCamBorderLevels.backBorder);
                if (targetPosition != Vector3.zero) targetPosition = new Vector3(targetPosition.x, cam.transform.position.y, moveCamBorderLevels.backBorder);
            }

            if (lockHorizontalMove) cam.transform.position = new Vector3(startHorPosition, cam.transform.position.y, cam.transform.position.z);

           
        }




        private bool isMouseDown0;
        private Vector2 mouseStartPos;
        private Vector2 mouseCurrentPos;
        private Vector2 mouseLastPos;
        private Vector2 mouseDeltaPos;
        private float lastDeltaY;
        [HideInInspector]
        public Vector3 targetPosition;
        private bool isCameraMove = false;
        private void LateUpdate()
        {
            if (AppManager.Instance == null) return;
            if (Tutor.TutorManager.Instance == null) return;
            if (!Tutor.TutorManager.Instance.isEndTutor) return;
            if (AppManager.Instance.IsBlockedScreenScroll || IsPointerOverGameObject())
            {
                targetPosition = transform.position;
                isMouseDown0 = false;
                return;
            }
            isCameraMove = false;
            if (isMouseDown0)
            {
                if (Vector2.Distance(mouseStartPos, Input.mousePosition) > 10)
                {
                    isCameraMove = true;
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                targetPosition = Vector3.zero;
                mouseStartPos = Input.mousePosition;
                isMouseDown0 = true;
            }
            if (Input.GetMouseButtonUp(0))
            {
                isMouseDown0 = false;
            }
            mouseCurrentPos = Input.mousePosition;
            var tempDelta = mouseCurrentPos - mouseLastPos;
            if (tempDelta.y == 0 || lastDeltaY == 0)
            {
                mouseDeltaPos = tempDelta;
            }
            else if ((lastDeltaY > 0 && tempDelta.y > 0) || (lastDeltaY < 0 && tempDelta.y < 0))
            {
                mouseDeltaPos = tempDelta;
            }
            else
            {
                mouseDeltaPos = tempDelta;
                mouseDeltaPos.y = lastDeltaY;
            }
            //mouseDeltaPos = mouseCurrentPos - mouseLastPos;
            mouseLastPos = Input.mousePosition;
            lastDeltaY = tempDelta.y;

            if (isCameraMove)
            {
                targetPosition = new Vector3(transform.position.x - (mouseDeltaPos.x * touchSpeed), transform.position.y, transform.position.z - (mouseDeltaPos.y * touchSpeed));
                if (lockHorizontalMove) targetPosition.x = startHorPosition;
                transform.position = Vector3.LerpUnclamped(transform.position, targetPosition, Time.deltaTime);
            }
            else
            {
                if (targetPosition != Vector3.zero)
                {
                    transform.position = Vector3.LerpUnclamped(transform.position, targetPosition, Time.deltaTime);
                    if (Vector3.Distance(transform.position, targetPosition) < 0.2f) targetPosition = Vector3.zero;
                }
            }
            IsMove = isCameraMove;
        }

        internal void CenterCamOnPosition(Vector3 vector3)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, vector3.z);
        }

        public bool IsPointerOverGameObject()
        {
            // check Mouse 
            if (EventSystem.current.IsPointerOverGameObject())
                return true;

            // chek Touch 
            if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
                    return true;
            }

            return false;
        }
    }

    [System.Serializable]
    public class MoveCamBorderLevels
    {
        public float frontBorder;
        public float backBorder;
        public float leftBorder;
        public float rightBorder;
    }
}
