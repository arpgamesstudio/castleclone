﻿using System.Collections;
using System.Collections.Generic;
using TG.Audio;
using UnityEngine;
using UnityEngine.UI;

namespace TG.Examples
{
    public class PlayMusic : MonoBehaviour
    {
        public float fadeDuration = 2f;
        public float mixDuration = 5f;
        public AudioClip[] musics;
        private AudioSource lastClip;
        private int curIndex;

        public void PlayNextMusic()
        {
            var musicClip = GetNextClip();
            lastClip = SoundManager.Instance.PlayMusicClip(musicClip, fadeDuration, false);
        }

        public void PlayNextMusicWithMix()
        {
            var musicClip = GetNextClip();
            lastClip = SoundManager.Instance.PlayMusicClipWithMix(musicClip, mixDuration, false);
        }

        public void StopPlayLastMusic()
        {
            SoundManager.Instance.StopPlayMusic(fadeDuration);
        }

        public void PauseResumeLastMusic()
        {
            if (lastClip.isPlaying)
            {
                SoundManager.Instance.PausePlayingClip(lastClip);
            }
            else
            {
                SoundManager.Instance.ResumeClipIfInPause(lastClip);
            }
        }

        public void PlayNextMusicAfterCurrent()
        {
            var musicClip = GetNextClip();
            SoundManager.Instance.PlayMusicClipAfterCurrentWithMix(musicClip, mixDuration);
        }

        private AudioClip GetNextClip()
        {
            curIndex++;
            if (curIndex == musics.Length)
            {
                curIndex = 0;
            }

            return musics[curIndex];
        }
    }
}
