﻿using System.Collections;
using System.Collections.Generic;
using TG.Audio;
using UnityEngine;

namespace TG.Examples
{
    public class PlayRandomSound : MonoBehaviour
    {
        public AudioClip[] clips;

        public void PlayRandomClip()
        {
            if (clips.Length == 0)
            {
                return;
            }
            var clip = clips[Random.Range(0, clips.Length)];
            SoundManager.Instance.PlayAudioClip(clip);
        }

    }
}
