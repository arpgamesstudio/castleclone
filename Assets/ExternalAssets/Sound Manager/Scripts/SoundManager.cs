﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;

namespace TG.Audio
{
    public class SoundManager : Singleton<SoundManager>
    {
        private const float FADE_MUSIC_DURATION = 0.3f;
        private const float FADE_MUSIC_VALUE = 3f;
        private const float WAIT_FINISHED_CHECK_INTERVAL = 1f;

        [SerializeField]
        private bool dontDestroyOnLoad;

        private readonly SavableValue<bool> soundEnabled = new SavableValue<bool>("SoundManager.soundEnabled", true);
        private readonly SavableValue<bool> musicEnabled = new SavableValue<bool>("SoundManager.musicEnabled", true);
        private readonly SavableValue<float> soundVolume = new SavableValue<float>("SoundManager.soundVolume", 1f);
        private readonly SavableValue<float> musicVolume = new SavableValue<float>("SoundManager.musicVolume", 1f);
        private readonly List<AudioSourceData> sourceMedia = new List<AudioSourceData>();
        private Coroutine nextMusicCoroutine;

        #region Props

        public event Action OnMusicPlayCompleted;

        public bool SoundEnabled
        {
            get => soundEnabled.Value;
            set
            {
                
                if (soundEnabled.Value == value)
                {
                    return;
                }

                foreach (var sourceData in sourceMedia.Where(sourceData => !sourceData.IsMusic))
                {
                    sourceData.Source.volume = value ? sourceData.RequestedVolume * soundVolume.Value : 0;
                }

                soundEnabled.Value = value;
            }
        }

        public bool MusicEnabled
        {
            get => musicEnabled.Value;
            set
            {
                if (musicEnabled.Value == value)
                {
                    return;
                }

                foreach (var sourceData in sourceMedia.Where(sourceData => sourceData.IsMusic))
                {
                    sourceData.Source.volume = value ? sourceData.RequestedVolume * musicVolume.Value : 0;
                }

                musicEnabled.Value = value;
            }
        }

        public float SoundVolume
        {
            get => soundVolume.Value;
            set
            {
                if (value > 1)
                {
                    value = 1;
                }
                else if (value < 0)
                {
                    value = 0;
                }

                soundVolume.Value = value;

                if (!soundEnabled.Value)
                {
                    return;
                }

                foreach (var sourceData in sourceMedia.Where(sourceData => !sourceData.IsMusic))
                {
                    sourceData.Source.volume = sourceData.RequestedVolume * soundVolume.Value;
                }
            }
        }

        public float MusicVolume
        {
            get => musicVolume.Value;
            set
            {
                if (value > 1)
                {
                    value = 1;
                }
                else if (value < 0)
                {
                    value = 0;
                }

                musicVolume.Value = value;

                if (!musicEnabled.Value)
                {
                    return;
                }

                foreach (var sourceData in sourceMedia.Where(sourceData => sourceData.IsMusic))
                {
                    sourceData.Source.volume = sourceData.RequestedVolume * musicVolume.Value;
                }
            }
        }

        internal void PlayAudioClip(object p)
        {
            throw new NotImplementedException();
        }

        #endregion Props

        private AudioPresenter presenter;
        private readonly List<AudioLibrary> audioLibraries = new List<AudioLibrary>();

        private void Awake()
        {
            if (dontDestroyOnLoad)
            {
                DontDestroyOnLoad(this);
            }

            Initialization();
        }

        private void Initialization()
        {
            presenter = gameObject.AddComponent<AudioPresenter>();
            var listener = new GameObject("LISTENER").AddComponent<AudioListener>();
            listener.transform.SetParent(presenter.transform);
            presenter.AudioListener = listener;
        }

        public void AddAudioLibrary(AudioLibrary library)
        {
            if (library == null)
            {
                return;
            }
            audioLibraries.Add(library);
        }

        public void RemoveAudioLibrary(AudioLibrary library)
        {
            if (library == null)
            {
                return;
            }
            audioLibraries.Remove(library);
        }

        #region Music

        public AudioSource PlayMusicClipWithMix(AudioClip clip, float mixDuration = 0, bool loop = true, float volumeProportion = 1)
        {
            if (mixDuration < 0)
            {
                mixDuration = 0;
            }

            if (volumeProportion > 1)
            {
                volumeProportion = 1;
            }
            else if (volumeProportion < 0)
            {
                volumeProportion = 0;
            }

            StopAllCoroutines();
            StopPlayMusic(mixDuration);
            ScanForEndedSources();

            var source = presenter.gameObject.AddComponent<AudioSource>();
            source.clip = clip;
            source.loop = loop;
            source.priority = 0;
            source.spatialBlend = 0;
            source.minDistance = 0.06f;

            var data = new AudioSourceData
            {
                IsMusic = true,
                OnPause = false,
                RequestedVolume = volumeProportion,
                Source = source
            };

            float volume = musicEnabled.Value ? volumeProportion * musicVolume.Value : 0;
            sourceMedia.Add(data);
            source.volume = mixDuration == 0 ? volume : 0;
            source.Play();

            if (mixDuration > 0)
            {
                StartCoroutine(DoFade(source, mixDuration, volume));
            }

            return source;
        }

        public AudioSource PlayMusicClip(AudioClip clip, float fadeDuration = 0,  bool loop = true, float volumeProportion = 1)
        {
            if (fadeDuration < 0)
            {
                fadeDuration = 0;
            }

            if (volumeProportion > 1)
            {
                volumeProportion = 1;
            }
            else if (volumeProportion < 0)
            {
                volumeProportion = 0;
            }

            StopAllCoroutines();
            bool musicPlaying = StopPlayMusic(fadeDuration);
            ScanForEndedSources();

            var source = presenter.gameObject.AddComponent<AudioSource>();
            source.clip = clip;
            source.loop = loop;
            source.priority = 0;
            source.spatialBlend = 0;
            source.minDistance = 0.06f;

            var data = new AudioSourceData
            {
                IsMusic = true,
                OnPause = false,
                RequestedVolume = volumeProportion,
                Source = source
            };

            float volume = musicEnabled.Value ? volumeProportion * musicVolume.Value : 0;
            sourceMedia.Add(data);

            if (fadeDuration == 0)
            {
                source.volume = volume;
                source.Play();
                StartCoroutine(WaitForPlayFinished(source));
            }
            else
            {
                source.volume = 0;
                float delay = musicPlaying ? fadeDuration : 0;
                StartCoroutine(DoFadeAfterDelay(source, fadeDuration, volume, delay));
            }

            return source;
        }

        public void PlayMusicClipAfterCurrentWithMix(AudioClip clip, float mixDuration = 0, bool loop = false, float volumeProportion = 1)
        {
            var currentMusic = GetPrevMusic();

            void PlayNextMusic()
            {
                PlayMusicClipWithMix(clip, mixDuration, loop, volumeProportion);
            }

            if (currentMusic == null)
            {
                PlayNextMusic();
                return;
            }

            if (nextMusicCoroutine != null)
            {
                StopCoroutine(nextMusicCoroutine);
            }
            nextMusicCoroutine = StartCoroutine(WaitForPlayFinished(currentMusic.Source, mixDuration, PlayNextMusic));
        }

        public void PlayMusicClipAfterCurrent(AudioClip clip, float fadeDuration = 0, bool loop = false, float volumeProportion = 1)
        {
            var currentMusic = GetPrevMusic();

            void PlayNextMusic()
            {
                PlayMusicClip(clip, fadeDuration, loop, volumeProportion);
            }

            if (currentMusic == null)
            {
                PlayNextMusic();
                return;
            }
            if (nextMusicCoroutine != null)
            {
                StopCoroutine(nextMusicCoroutine);
            }

            nextMusicCoroutine = StartCoroutine(WaitForPlayFinished(currentMusic.Source, fadeDuration, PlayNextMusic));
        }

        private IEnumerator WaitForPlayFinished(AudioSource source, float tillEnd = 0, Action callback = null)
        {
            while (source != null && (source.clip.length - source.time) > tillEnd) //source.isPlaying
            {
                yield return new WaitForSeconds(WAIT_FINISHED_CHECK_INTERVAL);
            }

            Debug.Log($"WaitForPlayFinished");
            OnMusicPlayCompleted?.Invoke();
            callback?.Invoke();
        }

        public bool StopPlayMusic(float fadeDuration)
        {
            var prevMusic = GetPrevMusic();

            if (prevMusic == null)
            {
                return false;
            }

            void StopMusic()
            {
                sourceMedia.Remove(prevMusic);
                SmartDestroy(prevMusic.Source);
            }

            if (fadeDuration == 0)
            {
                StopMusic();
            }
            else
            {
                StartCoroutine(DoFade(prevMusic.Source, fadeDuration, 0, StopMusic));
            }

            return true;
        }

        public bool IsMusicClipPlaying(AudioClip audioClip)
        {
            bool audioClipIsPlaying = sourceMedia.Any(s => s.IsMusic && s.Source.clip == audioClip && s.Source.isPlaying);
            return audioClipIsPlaying;
        }

        #endregion Music

        #region Sound

        public AudioSource PlayAudioClip(string clip, bool looped = false, float volumeProportion = 1, bool fadeMusic = false)
        {
            var audioClip = GetSoundAudioClip(clip);
            if (fadeMusic)
            {
                StartCoroutine(FadeMusicOnSeconds(audioClip.length));
            }
            return PlayAudioClip(audioClip, looped, volumeProportion);
        }

        public AudioSource PlayAudioClip(AudioClip clip, bool looped = false, float volumeProportion = 1)
        {
            if (volumeProportion > 1)
            {
                volumeProportion = 1;
            }
            else if (volumeProportion < 0)
            {
                volumeProportion = 0;
            }

            ScanForEndedSources();

            var source = presenter.gameObject.AddComponent<AudioSource>();
            source.clip = clip;
            source.loop = looped;
            source.spatialBlend = 0;
            source.minDistance = 0.06f;
            var data = new AudioSourceData
            {
                IsMusic = false,
                OnPause = false,
                RequestedVolume = volumeProportion,
                Source = source
            };

            source.volume = soundEnabled.Value ? volumeProportion * soundVolume.Value : 0;
            source.Play();
            sourceMedia.Add(data);
            return source;
        }

        public void PausePlayingClip(AudioSource audioSource)
        {
            var s = GetSourceData(audioSource);
            if (s == null)
            {
                return;
            }
            s.Source.Pause();
            s.OnPause = true;
        }

        public void ResumeClipIfInPause(AudioSource audioSource)
        {
            var s = GetSourceData(audioSource);
            if (s == null)
            {
                return;
            }
            s.Source.UnPause();
            s.OnPause = false;
        }

        public void StopPlayingClip(AudioSource audioSource, float fadeDuration = 0)
        {
            var s = GetSourceData(audioSource);
            if (s == null)
            {
                return;
            }
            sourceMedia.Remove(s);

            void StopMusic()
            {
                s.Source.Stop();
                SmartDestroy(s.Source);
            }

            if (fadeDuration == 0)
            {
                StopMusic();
            }
            else
            {
                StartCoroutine(DoFade(audioSource, fadeDuration, 0, StopMusic));
            }
        }

        public void StopPlayingClip(AudioClip clip, float fadeDuration = 0)
        {
            var sources = sourceMedia.Where(m => m.Source.clip == clip).ToArray();
            if (!sources.Any())
            {
                return;
            }

            void StopMusic(AudioSourceData data)
            {
                data.Source.Stop();
                SmartDestroy(data.Source);
            }

            foreach (var source in sources)
            {
                sourceMedia.Remove(source);
                if (fadeDuration == 0)
                {
                    StopMusic(source);
                }
                else
                {
                    StartCoroutine(DoFade(source.Source, fadeDuration, 0, () => StopMusic(source)));
                }
            }
        }

        public bool IsAudioClipPlaying(AudioSource audioSource)
        {
            var s = GetSourceData(audioSource);
            return s != null && s.Source.isPlaying;
        }

        public bool IsAudioClipPlaying(AudioClip clip)
        {
            var sources = sourceMedia.Where(m => m.Source.clip == clip).ToArray();
            return sources.Length != 0 && sources.Any(s => s.Source.isPlaying);
        }

        #endregion Sound

        #region Utils

        private AudioSourceData GetPrevMusic()
        {
            var prevMusics = sourceMedia.FirstOrDefault(s => s.IsMusic);
            return prevMusics;
        }

        private IEnumerator DoFadeAfterDelay(AudioSource audioSource, float duration, float targetVolume, float delay)
        {
            yield return new WaitForSeconds(delay);
            if (audioSource == null)
            {
                yield break;
            }

            audioSource.Play();
            StartCoroutine(WaitForPlayFinished(audioSource));
            StartCoroutine(DoFade(audioSource, duration, targetVolume));
        }

        private IEnumerator DoFade(AudioSource audioSource, float duration, float targetVolume, Action callback = null)
        {
            float currentTime = 0;
            float start = audioSource.volume;

            while (currentTime < duration)
            {
                currentTime += Time.deltaTime;
                if (audioSource == null)
                {
                    callback?.Invoke();
                    yield break;
                }

                audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                yield return null;
            }

            callback?.Invoke();
        }

        private void ScanForEndedSources()
        {
            var model = sourceMedia.Where(s => !s.OnPause && !s.Source.isPlaying).ToList();

            foreach (var s in model)
            {
                sourceMedia.Remove(s);

                SmartDestroy(s.Source);
            }
        }

        private IEnumerator FadeMusicOnSeconds(float duration)
        {
            const float fadeDuration = FADE_MUSIC_DURATION;
            float storedVolume = MusicVolume;
            float startVolume = MusicVolume;
            float targetVolume = MusicVolume / FADE_MUSIC_VALUE;
            float currentTime = 0;
            while (currentTime < fadeDuration)
            {
                currentTime += Time.deltaTime;
                MusicVolume = Mathf.Lerp(startVolume, targetVolume, currentTime / fadeDuration);
                yield return null;
            }

            yield return new WaitForSeconds(duration);

            startVolume = MusicVolume;
            targetVolume = storedVolume;
            currentTime = 0;
            while (currentTime < fadeDuration)
            {
                currentTime += Time.deltaTime;
                MusicVolume = Mathf.Lerp(startVolume, targetVolume, currentTime / fadeDuration);
                yield return null;
            }
        }

        private AudioClip GetMusicAudioClip(string clipName)
        {
            if (audioLibraries.Count != 0)
            {
                return audioLibraries.Select(audioLibrary => audioLibrary.GetMusic(clipName)).FirstOrDefault(clip => clip != null);
            }

            Debug.LogWarning("[SoundManager: PlayAudioClip] Audio library not assigned");
            return null;
        }

        private AudioClip GetSoundAudioClip(string clipName)
        {
            if (audioLibraries.Count != 0)
            {
                return audioLibraries.Select(audioLibrary => audioLibrary.GetSound(clipName)).FirstOrDefault(clip => clip != null);
            }

            Debug.LogWarning("[SoundManager: PlayAudioClip] Audio library not assigned");
            return null;
        }

        private AudioSourceData GetSourceData(AudioSource audioSource)
        {
            return sourceMedia.FirstOrDefault(s => s.Source == audioSource);
        }

        private void SmartDestroy(Object obj)
        {
#if UNITY_EDITOR
            Destroy(obj);
#else
            Object.Destroy(obj);
#endif
        }

        #endregion Utils

        #region Classes

        //Source - https://github.com/hexgrimm/Audio/blob/master/AudioController.cs
        internal class AudioSourceData
        {
            public AudioSource Source;
            public bool IsMusic;
            public float RequestedVolume;
            public bool OnPause;
        }

        internal class AudioPresenter : MonoBehaviour
        {
            public AudioListener AudioListener;
        }

        #endregion Classes
    }
}