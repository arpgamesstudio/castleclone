
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace TG.Audio
{
    [CreateAssetMenu(fileName ="AudioLibrary", menuName ="Settings/Audio Library")]
    public class AudioLibrary : ScriptableObject
    {
	    public List<SoundClip> sounds;
        public List<SoundClip> music;

        public AudioClip GetSound(string soundName)
        {
            string soundNameLoc = soundName;
            var clipLoc = sounds.Find(p => p.name == soundNameLoc)?.clip;
            if (clipLoc != null) return clipLoc;

            IEnumerable<SoundClip> clips = sounds.Where(s => string.Equals(s.name, soundName, StringComparison.CurrentCultureIgnoreCase));
            foreach (var clip in clips)
            {
                return clip.clip;
            }
            return null;
        }

        public AudioClip GetMusic(string musicName)
        {
	        var musics = music.Where(s => s.name == musicName);
            foreach (var clip in musics)
            {
                return clip.clip;
            }
            return null;
        }
    }

    [System.Serializable]
    public class SoundClip
    {
        public string name;
        public AudioClip clip;
    }
}